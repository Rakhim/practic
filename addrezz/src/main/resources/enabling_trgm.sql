CREATE EXTENSION pg_trgm;
CREATE EXTENSION btree_gin;

create index fa_tags_gin_idx on full_addresses using gin (tags gin_trgm_ops);
create index fa_tags_gist_idx on full_addresses using gist (tags gist_trgm_ops);

create index idx_ao_gin_offname on addressobjects using gin (offname gin_trgm_ops);
create index idx_ao_gin_formalname on addressobjects using gin (formalname gin_trgm_ops);
create table full_addresses
(
    offname_normal_form text,
    aoguids varchar(300),
    tags text
);

alter table full_addresses owner to normalizer;

create index fa_tags_gist_idx
	on full_addresses using gist (tags gist_trgm_ops);
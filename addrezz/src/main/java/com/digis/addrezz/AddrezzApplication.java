package com.digis.addrezz;

import com.digis.addrezz.controllers.LuceneHelper;
import com.digis.addrezz.controllers.StoredAddressDescription;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableScheduling
public class AddrezzApplication {

	public static void main(String[] args) throws IOException, ParseException {
		SpringApplication.run(AddrezzApplication.class, args);
	}

	private static void searchExample() throws IOException, ParseException {
		final String line = "20000009;129110, Москва, Москва, Москва, пер Орлово-Давыдовский, 2/5, 2,  комн 4 А, 4 Б"
				.replaceAll("[;,]", "")
				.replaceAll("[0-9]{6}", "")
				;
		System.out.println(line);

		final Analyzer analyzer = LuceneHelper.buildAnalyzer();
		final Optional<StoredAddressDescription> description = LuceneHelper
				.search(analyzer, line);

		System.out.println(description.map(StoredAddressDescription::getOffnameNormalForm).orElse("<empty>"));
	}

	public static void indexAddresses() throws IOException {
		final String sourceFileName = "full_addresses.csv";
		System.out.println("start.");
		final File sourceFile = new File(sourceFileName);
		final FileInputStream inputStream = new FileInputStream(sourceFile);
		final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		final String indexPath = "lucene-index-folder";
		removeFolderIfExist(indexPath);

		final StandardAnalyzer analyzer = new StandardAnalyzer();
		final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		final Directory indexDirectory = FSDirectory.open(Paths.get(indexPath));
		final IndexWriter indexWriter = new IndexWriter(indexDirectory, indexWriterConfig);

		final long before = System.currentTimeMillis();
		int lines = 0;
		String readString;
		while ((readString = bufferedReader.readLine()) != null)
		{
			final String[] columns = readString.split(";");

			final Document document = new Document();
			final String address = columns[0];
			final String guids = columns[1];
			document.add(new TextField("address", address, Field.Store.YES));
			document.add(new TextField("guids", guids, Field.Store.YES));
			indexWriter.addDocument(document);

			if (lines % 500_000 == 0) {
				System.out.println(lines);
				indexWriter.flush();
			}

			lines++;
		}

		indexWriter.close();

		System.out.printf("lines: '%d'; in '%s' %n", lines, System.currentTimeMillis() - before);
		System.out.println("end.");
	}

	private static void removeFolderIfExist(String path) {
		final File file = new File(path);
		if (file.exists()) {
			final String[] fileNames = file.list();
			if (fileNames != null) {
				for (String fileName : fileNames) {
					final File currentFile = new File(file.getPath(), fileName);
					currentFile.delete();
				}
			}

			file.delete();
		}
	}

	private static void luceneExample() throws IOException, ParseException {
		final String indexPath = "lucene-index-folder";
		removeFolderIfExist(indexPath);

		final StandardAnalyzer analyzer = new StandardAnalyzer();

		addToIndex(analyzer, indexPath, "first case", "one two three four five both");
		addToIndex(analyzer, indexPath, "second case", "bir eki ush tort bes alty jeti segiz both");

		searchFiles(analyzer, indexPath, "value", "ne");
		searchFiles(analyzer, indexPath, "value", "oe");
		searchFiles(analyzer, indexPath, "value", "on");
		searchFiles(analyzer, indexPath, "value", "one");
		searchFiles(analyzer, indexPath, "value", "br");
		searchFiles(analyzer, indexPath, "value", "bth");
		searchFiles(analyzer, indexPath, "value", "bt");
		searchFiles(analyzer, indexPath, "value", "both");
	}

	private static void show(List<Document> documents, String queryString) {
		if (documents.isEmpty()) {
			System.out.println(String.format("<no results for '%s'>", queryString));
			return;
		}

		System.out.print(queryString + ": ");
		for (Document document : documents) {
			System.out.print(String.format("{\"%s\": \"%s\"} ", document.get("name"), document.get("value")));
		}

		System.out.println();
	}

	public static void addToIndex(Analyzer analyzer, String indexPath, String name, String value) throws IOException {

		final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		final Directory indexDirectory = FSDirectory.open(Paths.get(indexPath));
		final IndexWriter indexWriter = new IndexWriter(indexDirectory, indexWriterConfig);
		final Document document = new Document();

		//final Path path = Paths.get(filepath);
		//final File file = path.toFile();
		//final FileReader fileReader = new FileReader(file);
		//document.add(new TextField("contents", fileReader));
		document.add(new StringField("name", name, Field.Store.YES));
		document.add(new TextField("value", value, Field.Store.YES));

		indexWriter.addDocument(document);
		indexWriter.close();
	}

	public static void addToIndex(Analyzer analyzer, String indexPath, Document document) throws IOException {

		final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		final Directory indexDirectory = FSDirectory.open(Paths.get(indexPath));
		final IndexWriter indexWriter = new IndexWriter(indexDirectory, indexWriterConfig);
		indexWriter.addDocument(document);
		indexWriter.close();
	}

	public static void searchFiles(
			Analyzer analyzer,
			String indexPath,
			String inField,
			String queryString
	) throws IOException, ParseException {

		final Query query = new FuzzyQuery(
			new Term(inField, queryString),
			1
		);

		// exact query:
		// final Query query = new QueryParser(inField, analyzer)
		// 		.parse(queryString);

		final Directory indexDirectory = FSDirectory
				.open(Paths.get(indexPath));

		final IndexReader indexReader = DirectoryReader
				.open(indexDirectory);

		final IndexSearcher searcher = new IndexSearcher(indexReader);
		final TopDocs topDocs = searcher.search(query, 10);
		final List<Document> documents = Arrays.stream(topDocs.scoreDocs)
				.map(scoreDoc -> {
					try {
						return searcher.doc(scoreDoc.doc);
					} catch (IOException e) {
						throw new IllegalStateException(e);
					}
				})
				.collect(Collectors.toList());

		show(documents, queryString);
	}
}

package com.digis.addrezz.attachment.controllers;

import com.digis.addrezz.attachment.dto.AttachmentRs;
import com.digis.addrezz.attachment.model.Attachment;
import com.digis.addrezz.attachment.services.StoreFileService;
import com.digis.addrezz.controllers.AbstractController;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class AttachmentController extends AbstractController {
	private final StoreFileService storeFileService;

	public AttachmentController(StoreFileService storeFileService) {
		this.storeFileService = storeFileService;
	}

	@GetMapping(value = "attachment", produces = "application/json")
	public List<AttachmentRs> getAttachments() {
		return storeFileService.getAttachments().stream()
				.map(attachment -> {
					final AttachmentRs attachmentRs = new AttachmentRs();
					attachmentRs.setFileName(attachment.getOriginalFileName());
					attachmentRs.setId(attachment.getId());
					return attachmentRs;
				})
				.collect(Collectors.toList());
	}

	@PostMapping(value = "attachment")
	public AttachmentRs addFile(@RequestParam("file") MultipartFile file) throws IOException {
		final Attachment savedAttachment = storeFileService.storeFile(file);
		final AttachmentRs attachmentInfo = new AttachmentRs();
		attachmentInfo.setId(savedAttachment.getId());
		attachmentInfo.setFileName(savedAttachment.getOriginalFileName());
		return attachmentInfo;
	}

	@GetMapping(value = "attachment/{attachmentId}", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Resource> downloadAttachmentById(@PathVariable Long attachmentId) throws Exception {
		final Attachment attachment = storeFileService.findById(attachmentId);
		final Resource resource = storeFileService.loadAttachmentAsResource(attachment);
		return ResponseEntity.ok()
				.contentLength(resource.contentLength())
				.header(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=".concat(attachment.getOriginalFileName()))
				.body(resource);
	}
}

package com.digis.addrezz.attachment.services;

import com.digis.addrezz.attachment.model.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class StoreFileService {
	private static final String newline = String.format("%n");

	private final AttachmentService attachmentService;
	private final Path fileStorageLocation;

	@Autowired
	public StoreFileService(AttachmentService attachmentService) {
		this.fileStorageLocation = Paths
				.get("./store/files")
				.toAbsolutePath()
				.normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		this.attachmentService = attachmentService;
	}

	public void deleteOldFiles() throws IOException {
		final Instant now = Instant.now();
		Files.list(fileStorageLocation)
				.forEach(path -> {
					try {
						final FileTime lastModifiedTime = Files.getLastModifiedTime(path);
						final long hourBetween = ChronoUnit.HOURS.between(lastModifiedTime.toInstant(), now);
						if (hourBetween > 1) {
							Files.deleteIfExists(path);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
	}

	private String getNewFileName() {
		return String.valueOf(Calendar.getInstance().getTimeInMillis()) + Thread.currentThread().getId() + ".csv";
	}

	public Attachment storeFile(MultipartFile file) throws IOException {
		final String fileName = getNewFileName();
		final Path targetLocation = this.fileStorageLocation.resolve(fileName);
		Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

		final Attachment attachment = new Attachment();
		attachment.setOriginalFileName(file.getOriginalFilename());
		attachment.setInternalFileName(fileName);
		return attachmentService.addAttachment(attachment);
	}

	public Path getPath(String fileName) {
		return this.fileStorageLocation.resolve(fileName).normalize();
	}

	public Resource loadFileAsResource(String fileName) throws Exception {
		return loadFileAsResource(getPath(fileName));
	}

	public Resource loadAttachmentAsResource(Attachment attachment) throws Exception {
		return loadFileAsResource(getPath(attachment.getInternalFileName()));
	}

	public Resource loadFileAsResource(Path filePath) throws Exception {
		try {
			final Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new Exception("FILE NOT FOUND");
			}

		} catch (Exception e) {
			throw new Exception("FILE NOT FOUND", e);
		}
	}

	public List<Attachment> getAttachments() {
		return attachmentService.getAttachments();
	}

	public Attachment findById(Long attachmentId) {
		return attachmentService.findById(attachmentId);
	}

	public Resource processFile(Attachment attachment) throws Exception {
		final Path filePath = this.fileStorageLocation.resolve(attachment.getInternalFileName()).normalize();
		final Path resultFilePath = this.fileStorageLocation.resolve("res".concat(attachment.getInternalFileName())).normalize();

		final File file = new File(filePath.toUri());
		final File resultFile = new File(resultFilePath.toUri());
		try (
				final FileReader fileReader = new FileReader(file);
				final BufferedReader bufferedReader = new BufferedReader(fileReader);
				final FileWriter resultFileWriter = new FileWriter(resultFile);
				final BufferedWriter resultBufferedWriter = new BufferedWriter(resultFileWriter);
		) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {

				Arrays.stream(line.toLowerCase().split("[^а-я0-9]+"))
						.map(s -> grammize(s, 3))
						.flatMap(Collection::stream)
						.forEach(s -> {
							try {
								resultBufferedWriter.write(s);
								resultBufferedWriter.write(" ");

							} catch (IOException e) {
								System.out.printf("spaceless: '%s'", e.getMessage());
							}
						});

				resultBufferedWriter.write(newline);
			}
		}

		return loadFileAsResource(resultFilePath);
	}

	private List<String> grammize(String string, int size) {
		if (string.length() <= size) {
			return Collections.singletonList(string);
		}

		final List<String> ngrams = new ArrayList<>();
		final int length = string.length();
		final int endIndex = length -  size + 1;
		for (int i = 0; i < endIndex; i++) {
			ngrams.add(string.substring(i, i + size));
		}

		return ngrams;
	}
}

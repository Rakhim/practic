package com.digis.addrezz.controllers;

import com.digis.addrezz.attachment.model.Attachment;
import com.digis.addrezz.attachment.services.StoreFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class NormalizerController extends AbstractController {
	final Logger log = LoggerFactory.getLogger(NormalizerController.class);
	private static final String newline = String.format("%n");

	private final StoreFileService storeFileService;
	private final AddressService addressService;

	private final AtomicLong counter = new AtomicLong();
	private int i = 0;

	public NormalizerController(StoreFileService storeFileService, AddressService addressService) {
		this.storeFileService = storeFileService;
		this.addressService = addressService;
	}

	@GetMapping("hello")
	public HelloRs hello() {
		i++;
		return new HelloRs(i, String.format("Hello, %s - %d!", counter.incrementAndGet(), i));
	}

	private Path waitForExecution(Attachment attachment, Path leftPart) throws Exception {
		log.info("waitForExecution. Запуск НС");

		final String fileName = attachment.getInternalFileName();
		final String resultFileName = "numdom." + fileName;
		final int exitCode = Runtime
				.getRuntime()
				.exec(String.format(
						"../normapi/venv/bin/onmt_translate -batch_size 10 -beam_size 20 -model models/%s -src %s -output %s -min_length 0 -stepwise_penalty -length_penalty wu -alpha 0.9 -block_ngram_repeat 0",
						"model_ngram_numdom_step_300000.pt",
						storeFileService.getPath(fileName).toUri().getPath(),
						storeFileService.getPath(resultFileName).toUri().getPath()
				))
				.waitFor();

		//final Resource resource = storeFileService.loadFileAsResource(resultFileName);
		final File file = new File(storeFileService.getPath(resultFileName).toUri().getPath());
		final File leftFile = new File(leftPart.toUri().getPath());

		final Path purePath = storeFileService.getPath(resultFileName + ".csv");
		final File resultFile = new File(purePath.toUri().getPath());
		try (
				final FileReader fileReader = new FileReader(file);
				final BufferedReader bufferedReader = new BufferedReader(fileReader);

				final FileReader leftFileReader = new FileReader(leftFile);
				final BufferedReader leftBufferedReader = new BufferedReader(leftFileReader);

				final FileWriter resultFileWriter = new FileWriter(resultFile);
				final BufferedWriter resultBufferedWriter = new BufferedWriter(resultFileWriter);
		) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				resultBufferedWriter.write(leftBufferedReader.readLine());

				final String houseNum = line.replaceAll("[<t>/]", "").replaceAll("nan", "").trim();
				if (houseNum.isEmpty()) {
					resultBufferedWriter.write(", д ");
					resultBufferedWriter.write(houseNum);
				}

				resultBufferedWriter.write(newline);
			}
		}

		return purePath;
	}

	@PostMapping("normalize/file")
	public Resource normalizeFile(@RequestParam("file") MultipartFile file) {

		try {
			final Attachment attachment = storeFileService.storeFile(file);

			final Path leftPart = addressService.search(attachment);
			try {
				final Path fullAddress = waitForExecution(attachment, leftPart);
				return storeFileService.loadFileAsResource(fullAddress);
			} catch (Exception e) {
				e.printStackTrace();

				return storeFileService.loadFileAsResource(leftPart);
			}

		} catch (Throwable e) {
			log.info("normalizeFile.", e);
			throw new IllegalStateException();
		}
	}

	@PostMapping("normalize/address/file")
	public Resource normalizeFileAddressOnly(@RequestParam("file") MultipartFile file) {

		try {
			final Attachment attachment = storeFileService.storeFile(file);
			final Path leftPart = addressService.search(attachment);
			return storeFileService.loadFileAsResource(leftPart);

		} catch (Throwable e) {
			log.info("normalizeFile.", e);
			throw new IllegalStateException();
		}
	}

	@PostMapping(value = "normalize/file/set", produces = MediaType.TEXT_PLAIN_VALUE)
	public String normalizeFileSet(@RequestParam("file") MultipartFile file) {

		try {
			final Attachment attachment = storeFileService.storeFile(file);
			final Path leftPart = addressService.search(attachment);
			try {
				final Path fullAddress = waitForExecution(attachment, leftPart);
				//return storeFileService.loadFileAsResource(fullAddress);

				return fullAddress.getFileName().toString();
			} catch (Exception e) {
				log.info("normalizeFile.", e);

				//return storeFileService.loadFileAsResource(leftPart);

				return leftPart.getFileName().toString();
			}

		} catch (Throwable e) {
			System.out.println(e.getMessage());

			e.printStackTrace();

			throw new IllegalStateException();
		}
	}

	@GetMapping(value = "normalize/file/get/{filename}", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Resource> downloadAttachmentById(@PathVariable("filename") String filename) throws Exception {
		final Path path = storeFileService.getPath(filename);
		final Resource resource = storeFileService.loadFileAsResource(path);
		return ResponseEntity.ok()
				.contentLength(resource.contentLength())
				.header(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=".concat(filename))
				.body(resource);
	}

	@GetMapping(value = "normalize", produces = MediaType.APPLICATION_JSON_VALUE)
	public AddressDescription normalizeString(@RequestParam("string") String string) throws Exception {
		return addressService.search(string);
	}
}

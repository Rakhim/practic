package com.digis.addrezz.controllers;

import java.util.List;
import java.util.stream.Collectors;

public class AddressDescription {
	private final String fullAddress;
	private final List<AddressObject> addressObjects;

	public AddressDescription(List<AddressObject> addressObjects) {
		this.addressObjects = addressObjects;
		fullAddress = addressObjects.stream()
				.map(o -> o.getSHORTNAME() + " " + o.getOFFNAME())
				.collect(Collectors.joining(","));
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public List<AddressObject> getAddressObjects() {
		return addressObjects;
	}
}

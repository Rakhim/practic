package com.digis.addrezz.controllers;

import com.digis.addrezz.attachment.model.Attachment;
import com.digis.addrezz.attachment.services.StoreFileService;
import org.apache.lucene.analysis.Analyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AddressService {
	final Logger log = LoggerFactory.getLogger(NormalizerController.class);
	private static final String newline = String.format("%n");

	@Resource
	private AddressRepository addressRepository;

	@Autowired
	private StoreFileService storeFileService;

	@Autowired
	private AddressObjectService addressObjectService;

	public Path search(Attachment attachment) throws Exception {
		log.info("search. Поиск в БД");

		final Path filePath = storeFileService.getPath(attachment.getInternalFileName());
		final Path resultFilePath = storeFileService.getPath("res".concat(attachment.getInternalFileName()));

		final Analyzer analyzer = LuceneHelper.buildAnalyzer();
		final File file = new File(filePath.toUri());
		final File resultFile = new File(resultFilePath.toUri());
		try (
				final FileReader fileReader = new FileReader(file);
				final BufferedReader bufferedReader = new BufferedReader(fileReader);
				final FileWriter resultFileWriter = new FileWriter(resultFile);
				final BufferedWriter resultBufferedWriter = new BufferedWriter(resultFileWriter);
		) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {

				final Optional<StoredAddressDescription> f = LuceneHelper.search(analyzer, line);
				if (f.isPresent()) {
					final StoredAddressDescription addressDescription = f.get();
					final String unitAoguid = addressDescription.getAoguids().get(0);
					final Optional<AddressObject> unitAddressObject = addressObjectService.findByAoguid(unitAoguid);

					//resultBufferedWriter.write(line);
					//resultBufferedWriter.write("   ");

					if (unitAddressObject.isPresent()) {
						resultBufferedWriter.write(unitAddressObject.map(AddressObject::getPOSTALCODE).orElse("") + ",  " + f.get().getOffnameNormalForm());
					} else {
						resultBufferedWriter.write(f.get().getOffnameNormalForm());
					}
				} else {
					resultBufferedWriter.write("NA:");
					resultBufferedWriter.write(line);
					resultBufferedWriter.flush();
				}

				resultBufferedWriter.write(newline);
			}
		}

		return resultFilePath;
	}

	public AddressDescription search(String string) throws Exception {
		final Optional<StoredAddressDescription> storedAddressDescriptionOptional = addressRepository
				.searchByOffnameNormalForm(string);

		final StoredAddressDescription storedAddressDescription = storedAddressDescriptionOptional.get();
		final List<AddressObject> addressObjects = storedAddressDescription
				.getAoguids()
				.stream()
				.map(aouidString -> addressObjectService.findByAoguid(aouidString))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toList());

		return new AddressDescription(addressObjects);
	}
}

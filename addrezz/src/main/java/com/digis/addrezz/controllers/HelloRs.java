package com.digis.addrezz.controllers;

public class HelloRs {
	private final int id;
	private final String content;

	public HelloRs(int id, String content) {
		this.id = id;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
}

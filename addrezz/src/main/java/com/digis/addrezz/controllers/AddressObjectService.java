package com.digis.addrezz.controllers;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;
import java.util.UUID;

@Service
public class AddressObjectService {

	@Resource
	private AddressObjectRepository addressObjectRepository;

	public Optional<AddressObject> findByAoguid(String aoguid) {
		return addressObjectRepository.findByAoguid(UUID.fromString(aoguid));
	}
}

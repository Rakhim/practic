package com.digis.addrezz.controllers;

import java.util.List;

public class StoredAddressDescription {

	private final String offnameNormalForm;
	private final List<String> aoguids;

	public StoredAddressDescription(
			String offnameNormalForm,
			List<String> aoguids
	) {
		this.offnameNormalForm = offnameNormalForm;
		this.aoguids = aoguids;
	}

	public String getOffnameNormalForm() {
		return offnameNormalForm;
	}

	public List<String> getAoguids() {
		return aoguids;
	}
}

package com.digis.addrezz.controllers;

import com.digis.addrezz.attachment.services.StoreFileService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ClearOldFilesJob {

	private final StoreFileService storeFileService;

	public ClearOldFilesJob(StoreFileService storeFileService) {
		this.storeFileService = storeFileService;
	}

	// 1 hour
	@Scheduled(fixedRate = 60 * 60_000)
	public void clear() throws IOException {
		// storeFileService.deleteOldFiles(); temporal disable
	}
}

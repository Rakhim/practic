package com.digis.addrezz.controllers;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AddressObjectRepository extends CrudRepository<AddressObject, Long> {

	@Query("from AddressObject ao where ao.AOGUID = :aoguid and ao.ACTSTATUS = '1'")
	Optional<AddressObject> findByAoguid(@Param("aoguid") UUID aoguid);
}

package com.digis.addrezz.controllers;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.ngram.NGramTokenizerFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LuceneHelper {
	private static final String INDEX_FOLDER = "store/lucene-index-folder";
	private static final String SEARCH_FIELD = "address";
	private static final String GUIDS_FIELD = "guids";

	public static void indexAddresses(Analyzer analyzer) throws IOException {
		final String sourceFileName = "full_addresses.csv";
		System.out.println("start.");
		final File sourceFile = new File(sourceFileName);
		final FileInputStream inputStream = new FileInputStream(sourceFile);
		final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		removeFolderIfExist(INDEX_FOLDER);

		final IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		final Directory indexDirectory = FSDirectory.open(Paths.get(INDEX_FOLDER));
		final IndexWriter indexWriter = new IndexWriter(indexDirectory, indexWriterConfig);

		final long before = System.currentTimeMillis();
		int lines = 0;
		String readString;
		while ((readString = bufferedReader.readLine()) != null) {
			final String[] columns = readString.split(";");

			final Document document = new Document();
			final String address = columns[0];
			final String guids = columns[1];
			document.add(new TextField(SEARCH_FIELD, address, Field.Store.YES));
			document.add(new TextField(GUIDS_FIELD, guids, Field.Store.YES));
			indexWriter.addDocument(document);

			if (lines % 500_000 == 0) {
				System.out.println(lines);
				indexWriter.flush();
			}

			lines++;
		}

		indexWriter.close();

		System.out.printf("lines: '%d'; in '%s' %n", lines, System.currentTimeMillis() - before);
		System.out.println("end.");
	}

	public static void removeFolderIfExist(String path) {
		final File file = new File(path);
		if (file.exists()) {
			final String[] fileNames = file.list();
			if (fileNames != null) {
				for (String fileName : fileNames) {
					final File currentFile = new File(file.getPath(), fileName);
					currentFile.delete();
				}
			}

			file.delete();
		}
	}

	public static Optional<StoredAddressDescription> search(
			Analyzer analyzer,
			String queryString
	) throws IOException, ParseException {

		final String searchString = queryString
				.replaceAll("[/;,]", "")
				.replaceAll("[0-9]{6}", "");

		final Query query;
		try {
			query = new QueryParser(SEARCH_FIELD, analyzer)
					.parse(searchString);
		} catch (ParseException e) {
			throw e;
		}

		final Directory indexDirectory = FSDirectory
				.open(Paths.get(INDEX_FOLDER));

		final IndexReader indexReader = DirectoryReader
				.open(indexDirectory);

		final IndexSearcher searcher = new IndexSearcher(indexReader);
		final TopDocs topDocs = searcher.search(query, 1);

		final ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		for (ScoreDoc scoreDoc : scoreDocs) {
			System.out.println(scoreDoc.score);
		}
		System.out.println();

		final Optional<Document> result = Arrays.stream(topDocs.scoreDocs)
				.map(scoreDoc -> {
					try {
						return searcher.doc(scoreDoc.doc);
					} catch (IOException e) {
						throw new IllegalStateException(e);
					}
				})
				.findAny();

		if (result.isPresent()) {
			final Document resultDocument = result.get();
			final String addressString = resultDocument.get(SEARCH_FIELD);
			final String aoguidsString = resultDocument.get(GUIDS_FIELD);

			final String[] aoguidsArray = aoguidsString.split("#");
			final List<String> aoguids = Arrays.asList(aoguidsArray);
			return Optional.of(new StoredAddressDescription(addressString, aoguids));
		}

		return Optional.empty();
	}

	public static Analyzer buildAnalyzer() throws IOException {
		return CustomAnalyzer.builder()
				.withTokenizer(NGramTokenizerFactory.class, "minGramSize", "1", "maxGramSize", "3")
				.addTokenFilter("lowercase")
				.build();
	}
}

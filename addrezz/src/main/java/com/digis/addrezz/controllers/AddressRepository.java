package com.digis.addrezz.controllers;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class AddressRepository {

	private static final int MAX_RESULTS = 1;

	@PersistenceContext
	private EntityManager entityManager;

	public Optional<StoredAddressDescription> searchByOffnameNormalForm(String text) {
		final Query query = entityManager.createNativeQuery("select offname_normal_form, aoguids from full_addresses order by similarity(:text, lower(offname_normal_form)) desc");

		query.setMaxResults(MAX_RESULTS);
		query.setParameter("text", text.toLowerCase());

		final List resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return Optional.empty();
		}

		final Object[] row = (Object[]) resultList.get(0);
		final String aoguidsString = (String) row[1];
		final String[] aoguidsArray = aoguidsString.split("#");
		final List<String> aoguids = Arrays.asList(aoguidsArray);
		return Optional.of(new StoredAddressDescription((String) row[0], aoguids));
	}

	public Optional<StoredAddressDescription> searchByOffnames(String text) {
		final Query query = entityManager.createNativeQuery("select offname_normal_form, aoguids from full_addresses order by similarity(:text, offnames) desc");

		query.setMaxResults(MAX_RESULTS);
		query.setParameter("text", text.toLowerCase());

		final List resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return Optional.empty();
		}

		final Object[] row = (Object[]) resultList.get(0);
		final String aoguidsString = (String) row[1];
		final String[] aoguidsArray = aoguidsString.split("#");
		final List<String> aoguids = Arrays.asList(aoguidsArray);

		return Optional.of(new StoredAddressDescription((String) row[0], aoguids));
	}

	public Optional<StoredAddressDescription> searchByNormalForm(String text) {
		final Query query = entityManager.createNativeQuery("select offname_normal_form, aoguids, similarity(tags, :text) as sml from full_addresses where full_addresses.tags % :text order by sml desc");

		query.setMaxResults(MAX_RESULTS);
		query.setParameter("text", text.toLowerCase());

		final List resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return Optional.empty();
		}

		final Object[] row = (Object[]) resultList.get(0);
		final String aoguidsString = (String) row[1];
		final String[] aoguidsArray = aoguidsString.split("#");
		final List<String> aoguids = Arrays.asList(aoguidsArray);

		return Optional.of(new StoredAddressDescription((String) row[0], aoguids));
	}
}

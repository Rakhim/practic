package org.example.hiberntae.usage;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * java -cp kafka-usage-all-1.0-SNAPSHOT.jar org.example.hiberntae.usage.HibernateUtil
 */
public class HibernateUtil
{
	private HibernateUtil() {}

	public static void main(String[] args) throws ClassNotFoundException
	{
		Class.forName("oracle.jdbc.OracleDriver");
		final Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");

		final SessionFactory sessionFactory = configuration.buildSessionFactory();
		final Session session = sessionFactory.openSession();
		final Transaction transaction = session.beginTransaction();

		final Query query = session.getNamedQuery("org.example.hiberntae.usage.mapping.Example.list");
		query.setParameter("login", 10001L);

		final List list1 = query.list();
		final List list2 = query.list();
		final List list3 = query.list();
		System.out.println("count 1: " + list1.size());
		System.out.println("count 2: " + list2.size());
		System.out.println("count 3: " + list3.size());

		transaction.commit();
		session.close();
	}

	private static void f()
	{

	}
}

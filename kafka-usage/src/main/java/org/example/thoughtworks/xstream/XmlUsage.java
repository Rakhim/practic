package org.example.thoughtworks.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.InputStream;

public class XmlUsage {

    private static MyData getInvocationData() {
        final MyData data = new MyData();
        data.setOne("bir");
        data.setTwo("eki");
        data.setThree("ush");
        data.setFour("tort");
        data.setFive("bes");
        return data;
    }

    private static void show(Object object) {
        final String xml = toXML(object);
        System.out.println(xml);
        System.out.println();
    }

    public static void main(String[] args) {
        show(getInvocationData());

        final InputStream input = Thread
                .currentThread()
                .getContextClassLoader()
                .getResourceAsStream("data.xml");

        final MyData data = fromXML(input);
        System.out.println(data);
    }

    private static String toXML(Object object) {
        return getXStream().toXML(object);
    }

    private static <T> T fromXML(String xml) {
        return (T) getXStream().fromXML(xml);
    }

    private static <T> T fromXML(InputStream xml) {
        return (T) getXStream().fromXML(xml);
    }

    private static XStream getXStream() {
        final XStream xStream = new XStream(new DomDriver());
        xStream.allowTypeHierarchy(MyData.class);
        return xStream;
    }
}

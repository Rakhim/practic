package com.moc.controllers;

import javax.servlet.GenericServlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.moc.controllers.ResponseHelper.writeResponse;

public class GenericServletImpl extends GenericServlet {

    @Override
    public void service(ServletRequest request, ServletResponse response) {
        writeResponse(
            (HttpServletRequest) request,
            (HttpServletResponse) response,
            "GenericServletImpl"
        );
    }
}
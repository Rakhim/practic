package com.moc.controllers;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.moc.controllers.ResponseHelper.writeResponse;

public class ServletImpl implements Servlet {
    private ServletConfig config = null;

    @Override
    public void init(ServletConfig config) {
        System.out.println("ServletImpl.init");
        this.config = config;
    }

    @Override
    public ServletConfig getServletConfig() {
        System.out.println("ServletImpl.getServletConfig");
        return config;
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) {
        writeResponse(
            (HttpServletRequest) request,
            (HttpServletResponse) response,
            "ServletImpl"
        );
    }

    @Override
    public String getServletInfo() {
        System.out.println("ServletImpl.getServletInfo");
        return "information about ush!!";
    }

    @Override
    public void destroy() {
        System.out.println("ServletImpl.destroy");
    }
}

package com.moc.controllers.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class HelloController
{
    @RequestMapping(value = "*", produces = "application/json; charset=UTF-8")
    public String redirectToGeneric()
    {
        return "/generic/servlet/impl";
    }

    @RequestMapping(value = "hello", produces = "application/json; charset=UTF-8")
    public @ResponseBody String hello()
    {
        return "Hello, Spring World!";
    }

    @RequestMapping(value = "greeting", produces = "application/json; charset=UTF-8")
    public @ResponseBody Greeting greeting()
    {
        return new Greeting("Hello, Greeting World!");
    }
}

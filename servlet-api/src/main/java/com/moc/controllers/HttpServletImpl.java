package com.moc.controllers;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.moc.controllers.ResponseHelper.writeResponse;

public class HttpServletImpl extends HttpServlet {
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
        System.out.println("HttpServletImpl.service");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
        System.out.println("HttpServletImpl.service");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        writeResponse(
            request,
            response,
            "HttpServletImpl"
        );
    }

}

package com.moc.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ResponseHelper {
    private ResponseHelper() {
    }

    public static void writeResponse(HttpServletRequest request, HttpServletResponse response, String title) {
        writeStatus(response);
        writeResponseData(request, response, title);
    }

    private static void writeStatus(HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
    }

    private static void writeResponseData(HttpServletRequest request, HttpServletResponse response, String title) {
        try (PrintWriter out = response.getWriter()) {
            out.println(getPage(request, title));
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            e.printStackTrace();
        }
    }

    private static String getPage(HttpServletRequest request, String title) {
        return String.format(
                getParameterizedPage(),
                title,
                request.getRequestURI(),
                request.getProtocol(),
                request.getPathInfo(),
                request.getRemoteAddr(),
                Math.random()
        );
    }

    private static String getParameterizedPage() {
        return "" +
                "<!DOCTYPE html>" +
                "<html>" +
                "<head>" +
                "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>" +
                "<title>Hello, World</title>" +
                "</head>" +
                "<body>" +
                "<h1>Hello, %s World!</h1>" +
                "<p>Request URI: %s</p>" +
                "<p>Protocol: %s</p>" +
                "<p>PathInfo: %s</p>" +
                "<p>Remote Address: %s</p>" +
                "<p>A Random Number: <strong> %s</strong></p>" +
                "</body>" +
                "</html>";
    }

}

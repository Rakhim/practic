package moc.com.readerswritersproblem;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raim
 */
public class Main {
  public static void main(String[] args ) {
    ExecutorService writerService = Executors.newSingleThreadExecutor();
    ExecutorService readerService = Executors.newFixedThreadPool(50);
    Writer writer = new Writer();
    
    for (int i = 0; i < 10;i++) {
      try {
        List<Reader> readers = new LinkedList<>();
        for (int j = 0; j < (int)(Math.random() * 10);j++) {
          readers.add(new Reader());
        }
        Object result = writerService.submit(writer).get();
        readerService.invokeAll(readers);
        
      } catch (InterruptedException | ExecutionException ex) {
        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
      }
      
    }    
  }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.readerswritersproblem;

import java.util.concurrent.Callable;

/**
 *
 * @author Raim
 */
public class Reader implements Callable<Object> {
  private final char name;
  
  public Reader() {
    final String characters = "QWERTYUIOPASDFGHJKLZXCVBNM";
    this.name = characters.charAt((int)(Math.random() * characters.length()));
  }
  
  @Override
  public Object call() {
    for (int i = 0; i < 20;i++) {
      System.out.print(name);
    }
    System.out.println();
    return null;
  }
  
}

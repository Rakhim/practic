package com.moc.iscp;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moc.iscp.serialization.IntervalDeserializer;
import com.moc.iscp.serialization.IntervalSerializer;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Администратор
 */
@JsonSerialize(using = IntervalSerializer.class)
@JsonDeserialize(using = IntervalDeserializer.class)
public class Interval {
  private Double left;
  private Double right;

  public Interval() {
    this(null, null);
  }
  
  public Interval(Double left, Double right) {
    this.left = left;
    this.right = right;
  }

  Interval(Interval value) {
    left = value.left;
    right = value.right;
  }
  
  public Double getLeft() {
    return left;
  }

  public void setLeft(Double left) {
    this.left = left;
  }

  public Double getRight() {
    return right;
  }

  public void setRight(Double right) {
    this.right = right;
  }

  void addAndAssign(Interval interval) {
    this.left += interval.left;
    this.right += interval.right;
  }

  public Interval add(Interval interval) {
    return new Interval(
      getLeft() + interval.getLeft(),
      getRight() + interval.getRight()
    );
  }
  
  public Interval sub(Interval interval) {
    return new Interval(
      getLeft() - interval.getRight(),
      getRight() - interval.getLeft()
    );
  }
  
  public Interval mul(Interval interval) {
    double[] list = new double[]{
      getLeft() * interval.getLeft(),
      getLeft() * interval.getRight(),
      getRight() * interval.getLeft(),
      getRight() * interval.getRight()
    };
    
    Arrays.sort(list);
    
    return new Interval(
      list[0],
      list[list.length - 1]
    );
  }

  /**
   * 
   * @param interval without 0
   * @return
   */
  public Interval div(Interval interval) {
    
    return mul(new Interval(
      1. / interval.getRight(),
      1. / interval.getLeft()
    ));
  }

  public boolean isPoint() {
    return Objects.equals(getLeft(), getRight());
  }
  
  public Interval abs() {
    return new Interval(
      Math.abs(getLeft()),
      Math.abs(getRight())
    );
  }
  
  public Interval copy() {
    return new Interval(left, right);
  }
  
  public Interval dual() {
    return new Interval(
      getRight(),
      getLeft()
    );
  }
  
  public Interval pro() {
    return correct() ? copy() : dual();
  }

  public double min() {
    return correct() ? getLeft() : getRight();
  }

  public double max() {
    return correct() ? getRight() : getLeft();
  }
  
  public boolean correct() {
    return getLeft() <= getRight();
  }
  
  public boolean in(double x) {
    final Interval pro = pro();    
    return (pro.getLeft() <= x) && (pro.getRight() >= x);
  }
  
  public boolean in(Interval x) {
    return in(x.getLeft()) && in(x.getRight());
  }
  
  public Interval intersect(Interval x) {
    if (in(x)) {
      return x.copy();
    }
    
    if (x.in(this)) {
      return copy();
    }
    
    return new Interval(
      x.in(getLeft()) ? getLeft() : x.in(getRight()) ? getRight() : 0,
      in(x.getLeft()) ? x.getLeft() : in(x.getRight()) ? x.getRight() : 0
    ).pro();
  }
  
  @Override
  public String toString() {
    return "[" + left + ", " + right + ']';
  }
}
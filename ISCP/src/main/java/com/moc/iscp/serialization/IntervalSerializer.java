package com.moc.iscp.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.moc.iscp.Interval;
import java.io.IOException;

/**
 *
 * @author Raim
 */
public class IntervalSerializer extends StdSerializer<Interval> {

  public IntervalSerializer() {
    this(null);
  }

  public IntervalSerializer(Class<Interval> clazz) {
    super(clazz);
  }
  
  @Override
  public void serialize(Interval interval, JsonGenerator generator, SerializerProvider sp) throws IOException {
    generator.writeStartArray();
    generator.writeNumber(interval.getLeft());
    generator.writeNumber(interval.getRight());
    generator.writeEndArray();
  }

}

package com.moc.iscp.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.moc.iscp.Interval;
import java.io.IOException;

/**
 *
 * @author Raim
 */
public class IntervalDeserializer extends StdDeserializer<Interval> {

  public IntervalDeserializer() {
    this(null);
  }
  
  public IntervalDeserializer(Class<?> clazz) {
    super(clazz);
  }
  
  @Override
  public Interval deserialize(JsonParser parser, DeserializationContext dc) throws IOException, JsonProcessingException {
    final JsonNode node = parser.getCodec().readTree(parser);
    return new Interval(
      node.path(0).asDouble(),
      node.path(1).asDouble()
    );
  }

}

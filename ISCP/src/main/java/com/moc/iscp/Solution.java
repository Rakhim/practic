package com.moc.iscp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Raim
 */
public class Solution {
  final List<Long> list = new ArrayList<>();      // список номеров удобных для вывода
  final List<Set<Long>> sets = new ArrayList<>(); // покрытие
  public Interval interval;                       // сумма всех весов
  final List<Interval> intervals = new ArrayList<>();      // веса 
  
  void setSum(Interval interval) {
    this.interval = interval;
  }

  @Override
  public String toString() {
    return "{" + 
           "\n номера = " + list + 
           "\n подмножества = " + sets + 
           "\n веса = " + intervals +
           "\n суммарный вес = " + interval +
           "\n}";
  }

}

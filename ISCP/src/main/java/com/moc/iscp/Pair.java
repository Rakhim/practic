package com.moc.iscp;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

/**
 *
 * @author Raim
 */
public class Pair {
  private Set<Long> set;
  private Interval weight;

  public Pair() {
    this(null, null);
  }
  
  public Pair(Set<Long> set, Interval weight) {
    this.set = set;
    this.weight = weight;
  }

  @JsonProperty
  public Set<Long> getSet() {
    return set;
  }

  @JsonProperty
  public Interval getWeight() {
    return weight;
  }

  public void setSet(Set<Long> set) {
    this.set = set;
  }

  public void setWeight(Interval weight) {
    this.weight = weight;
  }

  @Override
  public String toString() {
    return "{" + "set=" + set + ", weight=" + weight + '}';
  }
  
}

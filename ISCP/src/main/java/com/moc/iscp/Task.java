package com.moc.iscp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Task {
  private final SCP scp;
  //private final Set<Long> initialSet = new HashSet<>();
  private final String fileName;
  //private final Map<Long, Set<Long>> initialSubsets = new HashMap<>();
  //private final Map<Long, Interval> initialIntervals = new HashMap<>();
    
  /**
   *
   * @param fileName
   * @throws java.lang.Exception
   */
  public Task(String fileName) throws Exception {
    this.fileName = fileName;
    
    scp = new SCP(read());
  }
  
  private Map<Long, Pair> read() throws Exception {
    Map<Long, Pair> result = new HashMap<>();
    final ObjectMapper mapper = new ObjectMapper();
    final File file = new File(fileName);
    final TypeReference<Map<Long, Pair>> typeReference = new TypeReference<Map<Long, Pair>>(){};
    
    try {
      result = mapper.readValue(file, typeReference);
      
    } catch (Exception e) {
      throw e;
    }

    return result;
  }
    
  /**
  * Поделить границы интервалов на мощности соответствующих множеств
  */
  private Map<Long, Interval> normalize(SCP scp) {
    final Map<Long, Interval> result = new HashMap<>();

    for (Long id: scp.keySet()) {
      // лишь в случае непустоты множества
      if (scp.getSet(id).size() > 0) {
        result.put(
          id,
          new Interval(
            scp.getInterval(id).getLeft() / scp.getSet(id).size(),
            scp.getInterval(id).getRight() / scp.getSet(id).size()
          )
        );
      }
    }
      
    return result;
  }

  /**
  * Найти максимальное значение правых границ интервалов
  * @param subsets
  * @param intervals
  */
  
  private Double maxRight(SCP scp, Map<Long, Interval> intervals) {
    Double max = 0.;
    for (Long id: scp.keySet()) {
      // среди не пустых множеств
      if (scp.getSet(id).size() > 0 && intervals.get(id).getRight() > max) {
        max = intervals.get(id).getRight();
      }
    }
      
    return max;
  }

  /**
  * Найти минимальное значение правых границ интервалов
  * @param subsets
  * @param intervals
  */
  private Double minRight(SCP scp, Map<Long, Interval> intervals) {
    // поиск минимального значения. В начале предпологаем, что оно максимальное.
    Double min = maxRight(scp, intervals);
    for (Long id: scp.keySet()) {
      // среди не пустых множеств
      if (scp.getSet(id).size() > 0 && intervals.get(id).getRight() < min) {
        min = intervals.get(id).getRight();
      }
    }

    return min;
  }
    
  /**
  * Список множеств с минимальным значением соответствующей правой границы
  */
  private List<Long> choose(SCP scp) {
    final Map<Long, Interval> normalizedIntervals = normalize(scp);
    final Double min = minRight(scp,  normalizedIntervals);
    
    // складываем все множества соответствующие минимальному значению
    final List<Long> result = new ArrayList<>();
    for (Long id: scp.keySet()) {
      // Среди не пустых множеств. 
      // Если левая часть не превосходит минимума правых частей
      if (scp.getSet(id).size() > 0 && normalizedIntervals.get(id).getLeft()<= min) {
        result.add(id);
      }
    }
    
    return result;
  }

  /**
  * Вес выбранного множества
  * @param number
  */  
  private Interval weightByID(SCP scp, List<Long> numbers, Long number) {
    
    // минимальный относительный вес
    Double min = Double.MAX_VALUE;
    for (Long index : numbers) {
      if (!index.equals(number)) {
        final Double normalized = 
          scp.getInterval(index).getRight() / scp.getSet(index).size();
        
        if (normalized < min) {
          min = normalized;
        }
      }
    }
    
    final Interval interval = new Interval(scp.getInterval(number));
    final Double normalized = interval.getRight() / scp.getSet(number).size();
    
    if (normalized > min) {
      interval.setRight(
        Double.valueOf(scp.getSet(number).size()) * min
      );
    }
    
    return interval;
  }
  
  private SCP taskModification(SCP scp, Long number) {
    final SCP result = new SCP();
    
    for (Long id: scp.keySet()) {
      if (!(id.equals(number)) && scp.getSet(id).size() > 0) {
        final Set<Long> set = new HashSet<>(scp.getSet(id)); 
        set.removeAll(scp.getSet(number));
        
        if (scp.getInterval(id).getLeft() / scp.getSet(id).size() <
            scp.getInterval(number).getLeft() / scp.getSet(number).size()
        ) {
          result.put(
              id,
              set,
              new Interval(
                scp.getInterval(number).getLeft() * scp.getSet(id).size() / scp.getSet(number).size(),
                scp.getInterval(id).getRight()
              )
          );
        } else {
          result.put( id, set, new Interval(scp.getInterval(id)));
        }
      }  
    }
    
    return result;
  }
  
  /**
  * Вычесть из всех подмножеств данное множество
  * @param subsets
  * @param set
  */
  private Map<Long, Set<Long>> difference(Map<Long, Set<Long>> subsets, Set<Long> set) {

    final Map<Long, Set<Long>> result = new HashMap<>();
    for (Map.Entry<Long, Set<Long>> entry: subsets.entrySet()) {
      final Set<Long> subset = new HashSet<>(entry.getValue());
      subset.removeAll(set);
      result.put(entry.getKey(), subset);
    }

    return result;
  }
  
  public List<Solution> evaluate() {
    final SimpleTree simpleTree = new Root();
    evaluate(scp, simpleTree);

    final List<Solution> result = new ArrayList<>();
    for (SimpleTree leaf: simpleTree.leafs()) {
      final Interval interval = new Interval(0.0, 0.0);
      final Solution solution = new Solution();
      
      for (SimpleTree tree: leaf.path()) {
        solution.list.add(tree.info().number);
        solution.sets.add(scp.getSet(tree.info().number));
        solution.intervals.add(tree.info().choosedWeight);
        
        interval.addAndAssign(tree.info().choosedWeight);
      }
      
      solution.setSum(interval);
      
      result.add(solution);
    }

    return result;
  }
    
  private void evaluate(SCP scp, SimpleTree tree) {
    
    // если все подмножества пусты
    if (scp.emptySets()) {
      // значит путь найден
      return;
    }

    final List<Long> numbers = choose(scp);
    
    for (Long number:numbers) {
      final Info info = new Info();
      info.number = number;
      info.choosedWeight = weightByID(scp, numbers, number);
      
      final SimpleTree simpleTree = new SimpleTreeImpl(info, tree);
      tree.add(simpleTree);

      final SCP modifiedTask = taskModification(scp, number);
      
      evaluate(
        modifiedTask,
        simpleTree
      );
    }
  }

  @Override
  public String toString() {
    return "Task{ fileName=" + fileName  + ", scp=" + scp + '}';
  }
  
  private static interface SimpleTree {
    Info info();
    void add(SimpleTree tree);
    List<SimpleTree> leafs();
    List<SimpleTree> path();
  }
    
  private static class Root implements SimpleTree {
    final List<SimpleTree> descendants;

    public Root() {
      this.descendants = new ArrayList<>();
    }

    @Override
    public void add(SimpleTree tree) {
      descendants.add(tree);
    }

    @Override
    public List<SimpleTree> leafs() {
      final List<SimpleTree> list = new ArrayList<>();
      if (descendants.isEmpty()) {
        list.add(this);
      } else {
        for (SimpleTree tree: descendants) {
          list.addAll(tree.leafs());
        }
      }

      return list;
    }

    @Override
    public List<SimpleTree> path() {
      return new ArrayList<>();
    }

    @Override
    public Info info() {
      throw new UnsupportedOperationException("У корня нет информаций.");
    }
  }
    
  private static class SimpleTreeImpl implements SimpleTree {
    final Info info;
    final SimpleTree root;
    final List<SimpleTree> descendants;

    public SimpleTreeImpl(Info info, SimpleTree root) {
      this.info = info;
      this.root = root;
      this.descendants = new ArrayList<>();
    }

    @Override
    public void add(SimpleTree tree) {
      descendants.add(tree);
    }

    @Override
    public List<SimpleTree> leafs() {
      final List<SimpleTree> list = new ArrayList<>();
      if (descendants.isEmpty()) {
        list.add(this);
      } else {
        for (SimpleTree tree: descendants) {
          list.addAll(tree.leafs());
        }
      }

      return list;
    }

    @Override
    public List<SimpleTree> path() {
      final List<SimpleTree> list = new ArrayList<>();
      list.addAll(root.path());
      list.add(this);
      return list;
    }

    @Override
    public Info info() {
      return info;
    }

    @Override
    public String toString() {
      return "{" + info + '}';
    }
  }  
}

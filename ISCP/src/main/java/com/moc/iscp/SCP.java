package com.moc.iscp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Raim
 */
public final class SCP {
  final Map<Long, Pair> map;
  
  public SCP() {
    this.map = new HashMap<>();
  }
  
  public SCP(Map<Long, Pair> map) {
    this.map = map;
  }
  
  public Set<Long> universum() {
    final Set<Long> result = new HashSet<>();
    for (Pair pair: map.values()) {
      result.addAll(pair.getSet());
    }
    
    return result;
  }
  
  public Set<Long> keySet() {
    return map.keySet();
  }
  
  public Set<Long> getSet(Long index) {
    return map.get(index).getSet();
  }
  
  public void setSet(Long index, Set<Long> set) {
    map.get(index).setSet(set);
  }
  
  public Interval getInterval(Long index) {
    return map.get(index).getWeight();
  }
  
  public void setInterval(Long index, Interval interval) {
    map.get(index).setWeight(interval);
  }

  void put(Long id, Set<Long> set, Interval interval) {
    map.put(id, new Pair(set, interval));
  }
  
  public boolean emptySets() {

    for (Pair pair:map.values()) {
      if (pair.getSet().size() > 0) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String toString() {
    return "SCP{" + "map=" + map + '}';
  }

}

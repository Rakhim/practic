package com.moc.iscp;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
  private static final String FILE_NAME = "input.txt";

  public static void main(String[] args) {
    
    try {
      final Task task = new Task(FILE_NAME);                   
      final List<Solution> solutions = task.evaluate();
      
      System.out.println(task);
      
      for (Solution solution: solutions) {
        System.out.println(solution);
      }
      
      System.out.println("Всего решений: " + solutions.size());
      
    } catch (Exception e) {
      Logger
          .getLogger(Main.class.getName())
          .log(Level.SEVERE, "Ошибка ", e);
    }
    
  }
}

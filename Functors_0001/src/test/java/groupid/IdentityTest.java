package groupid;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IdentityTest {

    private Integer getValue() {
        final Identity<String> left = new Identity<>("left")
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String value) {
                        return value.concat("-");
                    }
                });

        return new Identity<>("right")
                .flatMap(new Function<String, Identity<String>>() {
                    @Override
                    public Identity<String> apply(final String value) {
                        return left.map(new Function<String, String>() {
                            @Override
                            public String apply(String leftValue) {
                                return leftValue.concat(value);
                            }
                        });
                    }
                })
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String value) {
                        return "<p>".concat(value).concat("</p>");
                    }
                })
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String value) {
                        return "<b>".concat(value).concat("</b>");
                    }
                })
                .map(new Function<String, Integer>() {
                    @Override
                    public Integer apply(String value) {
                        System.out.println(value);
                        return value.length();
                    }
                })
                .getValue();
    }

    @Test
    public void shouldAnswerAsExpected() {
        assertEquals(Integer.valueOf(24), getValue());
    }
}

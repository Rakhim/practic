package groupid;

/**
 * Functor - обертка над объектом категорий T.
 *
 * @param <T> -   тип категорий
 * @param <F> -   Functor-образ
 */
public interface Functor<T, F extends Functor<?, ?>> {

    /**
     * Применение отображения {@link Function} к текущему Functor-у
     *
     * @param function -   отображение
     * @param <R>      -   тип образа
     * @return -   Functor образа.
     */
    <R> F map(Function<T, R> function);
}

package groupid;

/**
 * Functor отображение которго сохраняет тип functor-а.
 * @param <T>   -   тип объекта категорий
 */
public final class Identity<T> implements Functor<T, Identity<?>> {

    private final T value;

    Identity(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    @Override
    public <R> Identity<R> map(Function<T, R> function) {
        return new Identity<>(
                function.apply(getValue())
        );
    }

    public <R> Identity<R> flatMap(Function<T, Identity<R>> function) {
        return function.apply(getValue());
    }
}
package groupid.chain;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BlockchainTest {

    @Test
    public void isValid() {
        final Blockchain blockchain = Blockchain
                .init("one")
                .addBlock("two")
                .addBlock("three")
                .addBlock("four")
                .addBlock("five");

        assertTrue(blockchain.isValid());
    }

    @Test
    public void asList() {
        final List<String> expectations = Arrays.asList(
                "one",
                "two",
                "three",
                "four",
                "five"
        );

        final List<Block> blocks = Blockchain
                .init("one")
                .addBlock("two")
                .addBlock("three")
                .addBlock("four")
                .addBlock("five")
                .asList();

        for (int i = 0; i < expectations.size(); i++) {
            assertEquals(expectations.get(i), blocks.get(i).getData());
        }
    }
}
package groupid;

import groupid.chain.Blockchain;

public class Main {
    public static void main(String[] args) {
        final Blockchain blockchain = Blockchain
                .init("one")
                .addBlock("two")
                .addBlock("three")
                .addBlock("four")
                .addBlock("five");

        System.out.println(blockchain);
    }
}

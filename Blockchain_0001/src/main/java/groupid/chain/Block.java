package groupid.chain;

import java.io.Serializable;

public interface Block extends Serializable {
    String getData();
}

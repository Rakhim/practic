package groupid.chain;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class Blockchain {
    private final BlockInternal chain;

    public static Blockchain init(String data) {
        return new Blockchain(data);
    }

    private Blockchain(String data) {
        chain = new GenesisBlock(data, "0");
    }

    private Blockchain(BlockInternal chain) {
        this.chain = chain;
    }

    public Blockchain addBlock(String data) {
        return new Blockchain(chain.addBlock(data));
    }

    public boolean isValid() {
        return chain.isValid();
    }


    public List<Block> asList() {
        final List<Block> list = new ArrayList<>();
        chain.add(list);
        return list;
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(asList());
    }
}

package groupid.chain;

import com.google.gson.GsonBuilder;

import java.util.Date;

abstract class AbstractBlock implements BlockInternal {
    private final String data;
    private final long timestamp;
    private final String previousHash;
    private final String hash;

    AbstractBlock(String data, String previousHash) {
        this.data = data;
        this.previousHash = previousHash;
        this.timestamp = new Date().getTime();
        this.hash = Utils.digest(this);
    }

    final String getPreviousHash() {
        return previousHash;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String getData() {
        return data;
    }

    @Override
    public final String getHash() {
        return hash;
    }

    @Override
    public final BlockInternal addBlock(String data) {
        return new BlockImpl(data, this);
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}

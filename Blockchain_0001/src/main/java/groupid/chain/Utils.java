package groupid.chain;

import groupid.StringUtils;

class Utils {
    private Utils() {
    }

    public static String digest(AbstractBlock block) {
        return StringUtils.digest(block.getData(), Long.toString(block.getTimestamp()), block.getPreviousHash());
    }
}

package groupid.chain;

import java.util.List;

class GenesisBlock extends AbstractBlock {
    GenesisBlock(String data, String previousHash) {
        super(data, previousHash);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void add(List<Block> list) {
        list.add(this);
    }
}

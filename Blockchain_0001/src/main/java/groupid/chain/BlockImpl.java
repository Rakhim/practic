package groupid.chain;

import java.util.List;

class BlockImpl extends AbstractBlock {
    private final transient BlockInternal previous;

    BlockImpl(String data, BlockInternal previous) {
        super(data, previous.getHash());
        this.previous = previous;
    }

    @Override
    public final boolean isValid() {
        return super.getPreviousHash().equals(previous.getHash()) && previous.isValid();
    }

    @Override
    public void add(List<Block> list) {
        previous.add(list);
        list.add(this);
    }
}

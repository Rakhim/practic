package groupid.chain;

import java.util.List;

interface BlockInternal extends Block {

    boolean isValid();

    BlockInternal addBlock(String data);

    long getTimestamp();

    String getHash();

    void add(List<Block> list);
}

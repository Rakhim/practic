package groupid;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class StringUtils {

    private StringUtils() {
    }

    public static String digest(String... strings) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(Arrays.toString(strings).getBytes("UTF-8"));
            final StringBuffer buffer = new StringBuffer();
            for (byte b : hash) {
                final int cut = 0xff & b;
                buffer.append(cut < 0x10 ? "0" + Integer.toHexString(cut) : Integer.toHexString(cut));
            }

            return buffer.toString();

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

package moc.com.simplexmethod;

import moc.com.simplexmethod.matrix.Matrix;

import java.util.List;

/**
 * Ограничения задачи.
 */
public class Constraints {
  private final Matrix lhs; /* левая часть системы ограничений*/
  private final Vector rhs; /* правая часть системы ограниений*/
  private final List<Relation> relations; /* отношения между частями */

  public Constraints(Matrix lhs, Vector rhs, List<Relation> relations) {
    this.lhs = lhs;
    this.rhs = rhs;
    this.relations = relations;
  }

  public Matrix getLhs() {
    return lhs;
  }

  public Vector getRhs() {
    return rhs;
  }

  public List<Relation> getRelations() {
    return relations;
  }

  public Constraints clone() {
    return new Constraints(
      lhs.clone(),
      rhs.clone(),
      relations
    );
  }
}

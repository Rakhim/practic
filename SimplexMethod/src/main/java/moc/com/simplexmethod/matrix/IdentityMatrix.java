package moc.com.simplexmethod.matrix;

import moc.com.simplexmethod.Vector;

import java.io.PrintStream;

/**
 * Единичная  матрица - предпологалось использование при
 * формирований симплекс-таблицы.
 * @author Raim
 */
public class IdentityMatrix implements Matrix {
  private final MatrixImpl matrix;
  
  public IdentityMatrix(int n) {
    matrix = new MatrixImpl(n, n);
    for (int i = 0; i < n;i++) {
      matrix.set(i, i, 1.0);
    }
  }
  
  public double get(int i, int j) {
    return i == j ? matrix.get(i, j) : 0.0;
  }
  
  public int width() {
    return matrix.width();
  }
  
  public int height() {
    return matrix.height();
  }

  public Vector row(int i) {
    return new Vector(matrix.width()).set(i, 1.0);
  }

  public Vector column(int i) {
    return new Vector(matrix.height()).set(i, 1.0);
  }

  public Matrix clone() {
    return matrix.clone();
  }

  public void show(PrintStream out) {
    matrix.show(out);
  }
}

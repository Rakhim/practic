package moc.com.simplexmethod.matrix;

import moc.com.simplexmethod.Vector;

import java.io.PrintStream;

/**
 *
 * @author Raim
 */
public class DiagonalMatrix implements Matrix {
  private MatrixImpl matrix;

  public DiagonalMatrix(int n) {
    this.matrix = new MatrixImpl(n , n);
  }

  public DiagonalMatrix(Vector vector) {
    this(vector.size());
    for (int i = 0; i < vector.size();i++) {
      matrix.set(i, i, vector.get(i));
    }
  }
  
  public DiagonalMatrix(double... elements) {
    this(elements.length);
    for (int i = 0; i < elements.length;i++) {
      matrix.set(i, i, elements[i]);
    }
  }

  public double get(int i, int j) {
    return i == j ? matrix.get(i, j) : 0.0;
  }
  
  public void set(int i, double value) {
    matrix.set(i, i, value);
  }
  
  public int width() {
    return matrix.width();
  }
  
  public int height() {
    return matrix.height();
  }
  
  public Vector row(int i) {
    final Vector result = new Vector(matrix.width()); 
    return result.set(i, matrix.get(i, i));
  }

  public Vector column(int i) {
    final Vector result = new Vector(matrix.height()); 
    return result.set(i, matrix.get(i, i));
  }

  public Matrix clone() {
    return matrix.clone();
  }

  public void show(PrintStream out) {
    matrix.show(out);
  }
}

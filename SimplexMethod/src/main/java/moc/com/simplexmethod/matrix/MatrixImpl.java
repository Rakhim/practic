package moc.com.simplexmethod.matrix;

import moc.com.simplexmethod.Vector;

import java.io.PrintStream;

/**
 *
 * @author Raim
 */
public class MatrixImpl implements Matrix {
  private final double[][] matrix;
  private final int n;
  private final int m;

  public MatrixImpl(int n, int m) {
    matrix = new double[n][m];
    this.n = n;
    this.m = m;
  }
  
  public int width() {
    return m;
  }
  
  public int height() {
    return n;
  }
  
  public void set(int i, int j, double x) {
    matrix[i][j] = x;
  }
  
  public double get(int i, int j) {
    return matrix[i][j];
  }
  
  public Vector row(int i) {
    final Vector result = new Vector(width()); 
    for (int k = 0; k < width();k++) {
      result.set(k, matrix[i][k]);
    }
    return result;
  }

  public Vector column(int i) {
    final Vector result = new Vector(height()); 
    for (int k = 0; k < width();k++) {
      result.set(k, matrix[k][i]);
    }
    return result;
  }
  
  @Override
  public Matrix clone() {
    final MatrixImpl result = new MatrixImpl(height(), width());
    for (int i = 0; i < height();i++) {
      for (int j = 0; j < width();j++) {
        result.set(i, j, get(i, j));
      }
    }
    return result;
  }

  public void show(PrintStream out) {
    for (int i = 0; i < n;i++) {
      if (i == n - 1) {
        out.print("M");
      } else if (i == n - 2) {
        out.print("F");
      } else {
        out.print(" ");
      }

      for (int j = 0; j < m;j++) {
        out.print(String.format("%8s", String.format("%4.2f", matrix[i][j])));
      }
      out.println();
    }
    out.println();
  }
}

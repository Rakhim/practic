/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.simplexmethod.matrix;

import moc.com.simplexmethod.Vector;

import java.io.PrintStream;

/**
 *
 * @author Raim
 */
public interface Matrix {  
  int width();
  int height();
  double get(int i, int j);
  Vector row(int i);
  Vector column(int i);
  Matrix clone();

  void show(PrintStream out);
}

package moc.com.simplexmethod;

import moc.com.simplexmethod.matrix.Matrix;
import moc.com.simplexmethod.matrix.MatrixImpl;

import java.io.PrintStream;

/**
 * Симплекс-таблица
 * @author Raim
 */
public class SimplexTable {
  final MatrixImpl table;
  
  public SimplexTable(CanonicalTask task) {
    this.table = getSimplexTable(task);
  }
  
  /**
   * Результат: свободный член целевой функций
   * @return 
   */
  public double getResult() {
    return table.get(functionRowBy(FunctionType.TARGET), 0);
  }
  
  /**
   * Инициализировать симплекс-таблицу по данной задаче
   * @param task
   * @return 
   */
  private MatrixImpl getSimplexTable(CanonicalTask task) {
    final Function target = task.getTarget();   /* целевая функция */
    final Function penalty = task.getPenalty(); /* функция штрафов*/
    final Matrix lhs = task.getConstraints().getLhs();
    final Vector rhs = task.getConstraints().getRhs();
    final MatrixImpl matrix = new MatrixImpl(lhs.height() + 2, lhs.width() + 1);

    init(matrix, lhs);
    init(matrix, rhs);
    init(matrix, target, matrix.height() - 2);
    init(matrix, penalty, matrix.height() - 1);
    return matrix;
  }

  /**
   * Левая часть ограничений - сдвинута на столбец вправо
   *
   * @param matrix
   * @param lhs 
   */
  private void init(MatrixImpl matrix, Matrix lhs) {
    for (int i = 0 ; i < lhs.height(); i++)
      for (int j = 0; j < lhs.width(); j++)
        matrix.set(i, j + 1, lhs.get(i, j));
  }

  /**
   * Инициализируем первый столбец матрицы правыми частями ограничений
   * @param matrix
   * @param rhs 
   */
  private void init(MatrixImpl matrix, Vector rhs) {
    for (int i = 0; i < rhs.size(); i++)
      matrix.set(i, 0, rhs.get(i));
  }

  /**
   * Инициализируем данную строку row коэффициентами данной функций
   * function
   * 
   * @param matrix
   * @param function
   * @param row 
   */
  private void init(MatrixImpl matrix, Function function, int row) {
    for (int j = 0; j < function.asArray().length;j++) {
      matrix.set(row, j, -function.get(j));
    }
  }
  
  public void show(PrintStream out) {
    table.show(out);
  }
  
  /**
   * Вернуть строку с коэффициэнтами соответствующей функций
   * @param type
   * @return 
   */
  private int functionRowBy(FunctionType type) {
    return FunctionType.TARGET.equals(type)
      ? table.height() - 2
      : table.height() - 1;
  }
  
  /**
   * Проверить коэффициенты функций(штрафов или целевой) указанной в последней строке
   * на отрицательность
   * @param type
   * @return 
   */
  private boolean check(FunctionType type) {
    for (int j = 1; j < table.width(); j++)
      if (table.get(functionRowBy(type), j) < 0)
        return false;

    return true;
  }
  
  /**
   * Поиск ведущей строки среди ограничений
   * @param column
   * @return 
   */
  private int leadRow(int column) {
    int index = 0;
    double value = Double.MAX_VALUE;
    
    for (int i = 0; i < table.height() - 2; i++) {
      final double variable = table.get(i, column);
      final double constant = table.get(i, 0);
      if (variable > 0) {
        final double currentValue = constant / variable;
        if (currentValue < value) {
          value = currentValue;
          index = i;
        }
      }
    }
    
    return index;
  }

  /**
   * Поиск ведущего столбца по последеней строке соответствующей 
   * функций штрафов или целевой функций
   * (Только коэффициенты соответствующие переменным, без константы)
   * @return 
   */  
  private int leadColumn(FunctionType type) {
    int index = 0;
    double value = 0;
    for (int j = 1; j < table.width(); j++) {
      final double currentValue = table.get(functionRowBy(type), j);
      if (currentValue < 0 && value > currentValue) {
        value = currentValue;
        index = j; 
      }
    }
    
    return index;
  }

  /**
   * Вернуть координаты ведущего элемента в симплекс-таблице
   * @param type
   * @return
   */
  private Pair lead(FunctionType type) {
    final int leadColumn = leadColumn(type);
    return new Pair(leadRow(leadColumn), leadColumn);
  }

  /**
   * Поделить ведущую строку на ведущий элемент
   * @param lead
   */
  private void updateLeadRow(Pair lead) {
    final double leadElement = table.get(lead.left, lead.right);    
    Utils.multiplyRow(table, lead.left, 1. / leadElement);
  }

  /**
   * Пересчитать другие строки таблицы согласно ведущему элементу.
   * @param lead
   */
  private void updateOtherRows(Pair lead) {
    for (int i = 0; i < lead.left;i++) {
      Utils.substractRow(table, i, table.row(lead.left).mul(table.get(i, lead.right)));
    }
    
    for (int i = lead.left + 1; i < table.height();i++) {
      Utils.substractRow(table, i, table.row(lead.left).mul(table.get(i, lead.right)));
    }    
  }

  public void update() {
    /* Обновить симплекс-таблицу согласно функций штрафов*/
    while (!check(FunctionType.PENALTY)) {
      update(FunctionType.PENALTY);
    }

    /* Полученную симплекс таблицу в результате модификаций симплекс-методом для функций штрафов
    * решить с целевой функцией
    */
    while (!check(FunctionType.TARGET)) {
      update(FunctionType.TARGET);
    }
  }
  
  private void update(FunctionType type) {
    final Pair lead = lead(type);
    updateLeadRow(lead);
    updateOtherRows(lead);
  }
  
  private static class Pair {
    private final int left;
    private final int right;

    public Pair(int left, int right) {
      this.left = left;
      this.right = right;
    }

  }

  private enum FunctionType {
    PENALTY,
    TARGET
  }
}

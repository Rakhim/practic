package moc.com.simplexmethod;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import moc.com.serializer.TaskDeserializer;
import moc.com.simplexmethod.matrix.Matrix;

import java.util.List;

/**
 * Представление задачи описанной в файле input.txt
 * @author Raim
 */
@JsonDeserialize(using = TaskDeserializer.class)
public class Task {
  private final TaskType type;
  private final Function function;
  private final Constraints constraints;

  public Task(TaskType type, Function function, Constraints constraints) {
    this.type = type;
    this.function = function;
    this.constraints = constraints;
  }

  public TaskType getType() {
    return type;
  }

  public Function getFunction() {
    return function;
  }

  public Matrix getLhs() {
    return constraints.getLhs();
  }

  public Vector getRhs() {
    return constraints.getRhs();
  }

  public List<Relation> getRelations() {
    return constraints.getRelations();
  }

  public Constraints getConstraints() {
    return constraints;
  }
}

package moc.com.simplexmethod;

import moc.com.simplexmethod.matrix.Matrix;
import moc.com.simplexmethod.matrix.MatrixImpl;

import java.util.List;

/**
 * P = 6x + py + 4z
 * 2x +  y +  z + u = 180
 * x  + 3y + 2z + v = 300
 * 2x +  y + 2z + w = 240
 * 
 * @author Raim
 */
public class CanonicalTask {
  private final Function target;
  private final Function penalty;
  private Constraints constraints;

  /**
   * Приведение исходной задачи task к
   * каноническому типу.
   * Канонический тип:
   * Целевая функция - максимум
   * Ограничения - равенства
   * Функция штрафов - сумма равенств в исходной задаче
   * @param task
   */
  public CanonicalTask(Task task) {
    this.constraints = task.getConstraints().clone();
    final Matrix lhs = constraints.getLhs();
    final Vector rhs = constraints.getRhs();
    this.target = task.getFunction();
    /**
     * Изначально функция штрафов - (0. х)
     * т.е. тождественный нуль, т.о. она не будет оказывать влиния
     * на результат в случае отсутствия ограничений-равенств в исходной
     * задаче.
     */
    penalty = new Function(lhs.width());

    final List<Relation> relations = task.getRelations();
    for (int i = 0; i < relations.size();i++) {
      if (Relation.GE.equals(relations.get(i))) {
        /**
         * Если есть неравенства со знаком >=
         * то умножаем его на -1
         */
        Utils.multiplyRow((MatrixImpl)lhs, i, -1.0);
        Utils.multiplyVector(rhs, i, -1.0);
      } else if (Relation.EQ.equals(relations.get(i))) {
        /**
         * Если в исходных ограниениях есть равенства то формируем из их суммы
         * функцию штрафов
         */
        final Vector vector = Utils.join(rhs.get(i), lhs.row(i));
        Utils.addFunction(penalty, vector);
      }
    }

    /**
     * Если исходная задача на минимум то умножаем ее на -1
     */
    if (TaskType.MIN.equals(task.getType())) {
      Utils.multiplyFunction(target, -1.0);
    }
  }

  public Function getTarget() {
    return target;
  }

  public Function getPenalty() {
    return penalty;
  }

  public Constraints getConstraints() {
    return constraints;
  }
}

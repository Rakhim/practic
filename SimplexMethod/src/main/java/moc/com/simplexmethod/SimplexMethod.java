package moc.com.simplexmethod;

import java.io.PrintStream;

/**
 *
 * @author Raim
 */
public class SimplexMethod {
  private final String SHOW_RESULT_FORMAT = "Результат: %5.5f";
  private final SimplexTable table;
  
  public SimplexMethod(CanonicalTask task) {
    table = new SimplexTable(task);
  }
  
  public void solve() {
    table.update();
  }
  
  public void show() {
    show(System.out);
    System.out.println(String.format(SHOW_RESULT_FORMAT, table.getResult()));
  }
  
  public void show(PrintStream out) {
    table.show(out);
  }
}

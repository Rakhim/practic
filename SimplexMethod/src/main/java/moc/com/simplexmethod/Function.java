package moc.com.simplexmethod;

import java.util.List;

/**
 * Представляет собой функцию
 * Вектор -> Действительное число
 * x.get(0) - свободный член
 * x.get(i) - коэффициент при x[i]
 * @author Raim
 */
public class Function {
  private final Vector coefficients;
  
  public Function(int degree) {
    coefficients = new Vector(degree + 1);
  }

  public Function(List<Double> coefficients) {
    this.coefficients = new Vector(coefficients);
  }

  Function(double... coefficients) {
    this.coefficients = new Vector(coefficients);
  }
  
  public double get(int i) {
    return coefficients.get(i);
  }
  
  public void set(int i, double x) {
    coefficients.set(i, x);
  }
  
  public double evaluate(Vector vector) {
    final Vector value = Utils.join(1.0, vector);
    return coefficients.mul(value);
  }

  public double[] asArray() {
    return coefficients.asArray();
  }

  void show(String penalty) {
    System.out.println(penalty);
    for (double x: asArray()) {
      System.out.print(String.format("%4.4f ", x));
    }
    System.out.println();
  }
}

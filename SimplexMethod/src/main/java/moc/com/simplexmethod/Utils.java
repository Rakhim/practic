package moc.com.simplexmethod;

import moc.com.simplexmethod.matrix.Matrix;
import moc.com.simplexmethod.matrix.MatrixImpl;

/**
 *
 * @author Raim
 */
public class Utils {
  
  /**
   * Модифиция матрицы matrix.
   * Умножить строку i на заданное число x
   * @param matrix  - исходная матрица
   * @param i       - индекс строки
   * @param x       - множитель
   */
  public static void multiplyRow(MatrixImpl matrix, int i, double x) {
    for (int j = 0; j < matrix.width();j++) {
      matrix.set(i, j, matrix.get(i, j) * x);
    }
  }

  /**
   * Модифиция вектора vector
   * Умножить ячейку i вектора vector на скаляр x
   * @param vector
   * @param i
   * @param x
   */
  public static void multiplyVector(Vector vector, int i, double x) {
    vector.set(i, vector.get(i) * x);
  }


  /**
   * Модификация функций function.
   * Умножить функцию на число x.
   * @param function
   * @param x
   */
  public static void multiplyFunction(Function function, double x) {
    final double[] coefficients = function.asArray();
    for (int i = 0; i < coefficients.length;i++) {
      coefficients[i] = coefficients[i] * x;
    }
  }

  /**
   * "Конкатенация" таблиц.
   *  Расширяет левую таблицу правой.
   *  Выстоа левой таблицы должна быть не ниже правой.
   * @param left
   * @param right
   * @return left(n, m) ++ right(n, k) = result(n, m + k)
   */
  static Matrix join(Matrix left, Matrix right) {
    final MatrixImpl matrix = new MatrixImpl(left.height(), left.width() + right.width());
    for (int i = 0; i < left.height();i++) {
      for (int j = 0; j < left.width();j++) {
        matrix.set(i, j, left.get(i, j));
      }
    }

    for (int i = 0; i < right.height();i++) {
      for (int j = 0; j < right.width();j++) {
        matrix.set(i, left.width() +  j, right.get(i, j));
      }
    }

    return matrix;
  }

  /**
   * "Конкатенация" скаляра и вектора.
   * Расширение вектора vector скаляром scalar слева.
   * @param scalar
   * @param vector
   * @return
   */
  public static Vector join(double scalar, Vector vector) {
    final Vector newVector = new Vector(vector.size() + 1);
    newVector.set(0, scalar);
    for (int i = 0; i < vector.size();i++) {
      newVector.set(i + 1, vector.get(i));
    }
    return newVector;
  }

  /**
   * Модификация функций.
   * Сложить данную функцию с вектором.
   * @param function
   * @param vector
   */
  public static void addFunction(Function function, Vector vector) {
    for (int i = 0; i < vector.size();i++) {
      function.set(i, function.get(i) + vector.get(i));
    }
  }

  /**
   * Модификация матрицы.
   * Вычесть из данной строки вектор.
   * @param matrix
   * @param i
   * @param vector
   */
  public static void substractRow(MatrixImpl matrix, int i, Vector vector) {
    for (int j = 0; j < matrix.width();j++) {
      matrix.set(i, j, matrix.get(i, j) - vector.get(j));
    }
  }
}

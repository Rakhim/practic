/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.simplexmethod;

/**
 *
 * @author Raim
 */
public enum Relation {
  LE, // less then or equal to      <=
  GE, // greater then or equal to   >=
  EQ  // equal to                   =
}

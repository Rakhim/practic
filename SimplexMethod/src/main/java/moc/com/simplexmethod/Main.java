package moc.com.simplexmethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class Main {
  /**
   * Имя файла описания задачи.
   */
  private static final String FILE_NAME = "input.txt";

  /**
   * Загрузить описание задачи из файла fileName.
   * Задача описана в формате JSON.
   *
   * @param fileName
   * @return
   * @throws IOException
   */
  private static Task loadTask(String fileName) throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    final File file = new File(fileName);
    final TypeReference<Task> typeReference = new TypeReference<Task>(){};
    return mapper.readValue(file, typeReference);
  }
  
  public static void main( String[] args ) {
    try {
      final Task task = loadTask(FILE_NAME);
      final CanonicalTask canonicalTask = new CanonicalTask(task);
      final SimplexMethod method = new SimplexMethod(canonicalTask);
      method.solve();
      method.show();
    } catch (IOException ex) {
      Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}

package moc.com.simplexmethod;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Raim
 */
public class Vector {
  private final double[] vector;

  /**
   * Создать вектор из 0.0 длины n
   * @param n
   */
  public Vector(int n) {
    vector = new double[n];
  }

  public Vector(double... vector) {
    this.vector = Arrays.copyOf(vector, vector.length);
  }

  /**
   * Создание вектора по списку
   * @param list
   */
  public Vector(List<Double> list) {
    vector = new double[list.size()];
    for (int i = 0; i < list.size();i++) {
      vector[i] = list.get(i);
    }
  }
  
  public Vector set(int i, double x) {
    vector[i] = x;
    return this;
  }
  
  public double get(int i) {
    return vector[i];
  }
  
  public double[] asArray() {
    return vector;
  }

  public int size() {
    return vector.length;
  }


  /**
   * Скалярное произведение
   * @param right
   * @return
   */
  public double mul(Vector right) {
    double value = 0.0;
    for (int i = 0; i < vector.length;i++) {
      value += vector[i] * right.asArray()[i];
    }
    return value;
  }


  /**
   * Произведение текущего вектора и скаляра x
   * @param x
   * @return
   */
  public Vector mul(double x) {
    final Vector value = new Vector(vector.length); 
    for (int i = 0; i < vector.length;i++) {
      value.set(i, x * vector[i]);
    }
    return value;
  }


  /**
   * Cложить вектора записав результат в новый вектор
   * @param right
   * @return
   */
  public Vector add(Vector right) {
    final Vector value = new Vector(vector.length); 
    for (int i = 0; i < vector.length;i++) {
      value.set(i, vector[i] + right.get(i));
    }
    return value;
  }


  /**
   * Вычесть из текущего (левого) вектора
   * вектор right (правый) записав результат в новый вектор
   * @param right
   * @return
   */
  public Vector sub(Vector right) {
    final Vector value = new Vector(vector.length); 
    for (int i = 0; i < vector.length;i++) {
      value.set(i, vector[i] - right.get(i));
    }
    return value;
  }

  /**
   * клонировать текущий вектор как объект
   * @return
   */
  public Vector clone() {
    return new Vector(vector);
  }
}

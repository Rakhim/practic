package moc.com.isle;

/**
 *
 * @author Raim
 */
public class Visualizer {
  private final Panel panel;
  
  public Visualizer(Panel panel) {
    this.panel = panel;
  }
  
  public void visualize(Paintable paintable) {
    paintable.paint(panel);
  }
}

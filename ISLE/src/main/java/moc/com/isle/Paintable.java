/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.isle;

/**
 *
 * @author Raim
 */
public interface Paintable {
  
  /**
   *
   * @param panel
   */
  void paint(Panel panel);
}

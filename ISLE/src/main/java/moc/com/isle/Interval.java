package moc.com.isle;

import java.util.Arrays;

/**
 *
 * @author Raim
 */
public class Interval {
  private double left;
  private double right;

  public Interval() {
  }

  public Interval(Interval x) {
    this.left = x.left;
    this.right = x.right;
  }

  public Interval(double left, double right) {
    this.left = left;
    this.right = right;
  }

  public double getLeft() {
    return left;
  }

  public void setLeft(double left) {
    this.left = left;
  }

  public double getRight() {
    return right;
  }

  public void setRight(double right) {
    this.right = right;
  }
  
  public Interval add(Interval interval) {
    return new Interval(
      getLeft() + interval.getLeft(),
      getRight() + interval.getRight()
    );
  }
  
  public Interval sub(Interval interval) {
    return new Interval(
      getLeft() - interval.getRight(),
      getRight() - interval.getLeft()
    );
  }
  
  public Interval mul(Interval interval) {
    double[] list = new double[]{
      getLeft() * interval.getLeft(),
      getLeft() * interval.getRight(),
      getRight() * interval.getLeft(),
      getRight() * interval.getRight()
    };
    
    Arrays.sort(list);
    
    return new Interval(
      list[0],
      list[list.length - 1]
    );
  }

  /**
   * 
   * @param interval without 0
   * @return
   */
  public Interval div(Interval interval) {
    
    return mul(new Interval(
      1. / interval.getRight(),
      1. / interval.getLeft()
    ));
  }

  public boolean isPoint() {
    return getLeft() == getRight();
  }
  
  public Interval abs() {
    return new Interval(
      Math.abs(getLeft()),
      Math.abs(getRight())
    );
  }
  
  public Interval copy() {
    return new Interval(left, right);
  }
  
  public Interval dual() {
    return new Interval(
      getRight(),
      getLeft()
    );
  }
  
  public Interval pro() {
    return correct() ? copy() : dual();
  }

  public double min() {
    return correct() ? getLeft() : getRight();
  }

  public double max() {
    return correct() ? getRight() : getLeft();
  }
  
  public boolean correct() {
    return getLeft() <= getRight();
  }
  
  public boolean in(double x) {
    final Interval pro = pro();    
    return (pro.getLeft() <= x) && (pro.getRight() >= x);
  }
  
  public boolean in(Interval x) {
    return in(x.getLeft()) && in(x.getRight());
  }
  
  public Interval intersect(Interval x) {
    if (in(x)) {
      return x.copy();
    }
    
    if (x.in(this)) {
      return copy();
    }
    
    return new Interval(
      x.in(getLeft()) ? getLeft() : x.in(getRight()) ? getRight() : 0,
      in(x.getLeft()) ? x.getLeft() : in(x.getRight()) ? x.getRight() : 0
    ).pro();
  }
  
  @Override
  public String toString() {
    return "(" + left + ", " + right + ')';
  }
}
package moc.com.isle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

/**
 *
 * @author Raim
 */
public final class Panel {
  public static final int POINT_SIZE = 2;
  private final Graphics graphics;
  private final double scaleX = 0.4;
  private final double scaleY = 0.4;
  private final Dimension dimension;
  
  public Panel(Graphics graphics, Dimension dimension) {
    this.graphics = graphics;
    this.dimension = dimension;
    drawAxes();
  }
  
  private Point origin() {
    return new Point(dimension.width / 2, dimension.height / 2);
  }

  private Dimension getSize() {
    return dimension;
  }
  
  public Point toGlobal(double x, double y) {
    final double xx = origin().x + x * (getSize().height / Math.PI) * scaleX;
    final double yy = origin().y - y * (getSize().height / Math.PI) * scaleY;
    return new Point(xx, yy);
  }

  public Point fromGlobal(double x, double y) {
    final double xx = (x - origin().x) / ((getSize().height / Math.PI) * scaleX);
    final double yy = (origin().y - y) / ((getSize().height / Math.PI) * scaleY);
    return new Point(xx, yy);
  }
  
  private void drawPoint(double x, double y, Color color, int diameter) {
    final Color current = graphics.getColor();
    final Point global = toGlobal(x, y);
    
    graphics.setColor(color);
    graphics.drawOval((int)(global.x) - diameter / 2, (int)(global.y) - diameter / 2, diameter, diameter);
    graphics.setColor(current);
  }

  public void drawLine(double x1, double y1, double x2, double y2, Color color) {
    final Color current = graphics.getColor();
    final Point global1 = toGlobal(x1, y1);
    final Point global2 = toGlobal(x2, y2);

    graphics.setColor(color);
    graphics.drawLine((int)(global1.x), (int)(global1.y), (int)(global2.x), (int)(global2.y));
    graphics.drawLine((int)(global1.x) - 1, (int)(global1.y), (int)(global2.x) - 1, (int)(global2.y));

    graphics.setColor(current); 
  }
  
  private void drawString(double x, double y, String string, Color color) {
    final Color current = graphics.getColor();
    final Point global = toGlobal(x, y);
    
    graphics.setColor(color);
    graphics.drawString(string, (int)global.x, (int)global.y);
    graphics.setColor(current);
  }
  
  private void fillPoint(double x, double y, Color color, int diameter) {
    final Color current = graphics.getColor();
    final Point global = toGlobal(x, y);
    
    graphics.setColor(color);
    graphics.fillOval((int)(global.x) - diameter / 2, (int)(global.y) - diameter / 2, diameter, diameter);
    graphics.setColor(current);
  }
  
  public void drawPoint(double x, double y, Color color) {
    drawPoint(x, y, color, POINT_SIZE);
  }

  public void fillPoint(double x, double y, Color color) {
    fillPoint(x, y, color, 2 * POINT_SIZE);
  }
  
  private void drawLabels() {
    final Dimension size = getSize();
    final int bound = (int)(size.height / Math.PI);

    for (int i = -bound; i < (bound + 1); i++) {
      fillPoint(i * Math.PI / 2, 0, Color.DARK_GRAY, 2 * POINT_SIZE + 1);
      fillPoint(0, i * Math.PI / 2, Color.DARK_GRAY, 2 * POINT_SIZE + 1);
    }
  }

  private void drawAxes() {
    final Dimension size = getSize();
    final Point origin = origin();

    graphics.setColor(Color.DARK_GRAY);
    graphics.drawLine(0, (int)origin.y, size.width, (int)origin.y);
    graphics.drawLine((int)origin.x, 0, (int)origin.x, size.height);
    drawLabels();
  }

};
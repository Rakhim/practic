package moc.com.isle.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import moc.com.isle.ISLE;

/**
 *
 * @author Raim
 */
public class ISLESerializer extends StdSerializer<ISLE> {
  public ISLESerializer() {
    this(null);
  }
  
  public ISLESerializer(Class<ISLE> clazz) {
    super(clazz);
  }

  @Override
  public void serialize(ISLE isle, JsonGenerator generator, SerializerProvider sp) throws IOException {
  }
}

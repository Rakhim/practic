package moc.com.isle.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import java.io.IOException;
import moc.com.isle.ISLE;

/**
 *
 * @author Raim
 */
public class ISLEDeserializer extends StdDeserializer<ISLE> {
  public ISLEDeserializer() {
    this(null);
  }
  
  public ISLEDeserializer(Class<?> clazz) {
    super(clazz);
  }

  @Override
  public ISLE deserialize(JsonParser parser, DeserializationContext dc) throws IOException, JsonProcessingException {
    
    final JsonNode node = parser.getCodec().readTree(parser);
    final int n = ((IntNode)node.get("n")).intValue();
    final int m = ((IntNode)node.get("m")).intValue();
    final JsonNode matrix = node.get("matrix");
    
    final ISLE result = new ISLE(2, 2);
    
    for (int i = 0; i < n;i++) {
      final JsonNode row = matrix.path(i);
      
      for (int j = 0; j < m;j++) {
        final JsonNode interval = row.path(j);
        result.set(
          i, j,
          interval.get(0).asDouble(),
          interval.get(1).asDouble()
        );
      }
    }

    final JsonNode rhs = node.get("rhs");
    
    for (int i = 0; i < n;i++) {
      result.set(
          i,
          rhs.path(i).path(0).asDouble(),
          rhs.path(i).path(1).asDouble()
      );
    }
    
    return result;
  }
}

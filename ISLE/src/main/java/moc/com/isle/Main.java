package moc.com.isle;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 */
public class Main {
  private static final String FILE_NAME = "input.txt";
  private static final String FILE_EXTENSION = "png";
  private static final String DELIMITER = ".";
  
  public static void main(String[] args) {
    try {
      final ObjectMapper mapper = new ObjectMapper();
      final File file = new File(FILE_NAME);
      final TypeReference<ISLE> typeReference = new TypeReference<ISLE>(){};
      
      final ISLE intervalSLE = mapper.readValue(file, typeReference);

      mapper.writeValue(new File("result.txt"), intervalSLE);
      
      final ISLEVisualizer isleVisualizer = new ISLEVisualizer(intervalSLE, 350_000);
      final GaussSeidelVisualizer gaussSeidelVisualizer = new GaussSeidelVisualizer(intervalSLE);
      
      save(new Paintable[]{isleVisualizer, gaussSeidelVisualizer}, "file", new Dimension(1800, 1800));
    } catch (Exception e) {
      Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
    }
  }
  
  private static void save(Paintable[] paintables,String fileName, Dimension dimension) {
    try {
      
      final BufferedImage image = new BufferedImage(
          dimension.width,
          dimension.height,
          BufferedImage.TYPE_INT_RGB
      );

      final Graphics2D graphics = image.createGraphics();
      graphics.setColor(Color.GREEN);

      final Panel panel = new Panel(graphics,  dimension);
      final Visualizer visualizer = new Visualizer(panel);
      
      for (Paintable paintable: paintables) {
        visualizer.visualize(paintable);
      }
      
      ImageIO.write(image, FILE_EXTENSION, new File(fileName + DELIMITER + FILE_EXTENSION));

    } catch (IOException ex) {
      Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }    
  }
}

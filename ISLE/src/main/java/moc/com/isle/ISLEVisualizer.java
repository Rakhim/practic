package moc.com.isle;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Raim
 */
public class ISLEVisualizer implements Paintable {
  private final ISLE isle;
  private final int n;
  private final List<Point> unitedSolution;
  private final List<Point> tolerableSolution;
  private final List<Point> controlledSolution;
  
  private final List<SLE> sles;
  
  public ISLEVisualizer(ISLE isle, int n) {
    this.isle = isle;
    this.n = n;
    sles = new ArrayList<>(n);
    unitedSolution = new ArrayList<>(n);
    tolerableSolution = new ArrayList<>(n);
    controlledSolution = new ArrayList<>();
    process();
  }
  
  private boolean controlled(Point point) {
    return isle.controlled(point);
  }
  
  private boolean tolerable(Point point) {
   
    for (int i = 0; i < 100;i++) {
      final Matrix x = MatrixUtils.transpose(
          MatrixUtils.toMatrix(new double[]{point.x, point.y})
      );
      double[] column = MatrixUtils.multiply(sles.get(i).getLhs(), x).column(0);
      if (!isle.belongs(column)) {
        return false;
      }
    }
    return true;
  }
  
  private void process() {
    sles.addAll(isle.generate(n));
    
    for (int i = 0; i < n;i++) {
      final SLE sle = sles.get(i);      
      final double[] values = MatrixUtils.solutionJGSLE(sle);
      unitedSolution.add(new Point(values[0], values[1]));
    }
    
    for (Point point: unitedSolution) {
      if (tolerable(point)) {
        tolerableSolution.add(point);
      }
      
      if (controlled(point)) {
        controlledSolution.add(point);
      }
    }
  }
  
  @Override
  public void paint(Panel panel) {
    paint(panel, unitedSolution, Color.GREEN);
    paint(panel, tolerableSolution, Color.BLUE);
    paint(panel, controlledSolution, Color.YELLOW);
  }

  private void paint(Panel panel, List<Point> points, Color color) {
    final double size = 0.1;
    
    final int offset = 700;
    final RealMatrix frequncies = new RealMatrix(2 * offset, 2 * offset);

    for (Point point: points) {
      final int x = offset + (int)(point.x / size);
      final int y = offset + (int)(point.y / size);
      
      frequncies.set(x, y, frequncies.get(x, y) + 1);
    }
    
    final double max = frequncies.max();
    for (Point point: points) {
      final int x = offset + (int)(point.x / size);
      final int y = offset + (int)(point.y / size);

      final double frequency = frequncies.get(x, y) / max;
      final double level = frequncies.get(x, y) / max;
      final int halfR = color.getRed();
      final int halfG = color.getGreen();
      final int halfB = color.getBlue();
      
      final Color hue = frequency < 0.1 ? Color.GRAY : new Color(
          (int)(level * halfR),
          (int)(level * halfG),
          (int)(level * halfB)
      );
            
      panel.fillPoint(point.x, point.y, hue);
    }
  }
}

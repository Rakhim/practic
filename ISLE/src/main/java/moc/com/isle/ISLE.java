package moc.com.isle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import moc.com.isle.serializer.ISLEDeserializer;

/**
 *
 * @author Raim
 */
@JsonDeserialize(using = ISLEDeserializer.class)
public class ISLE {
  private final int n;
  private final int m;
  
  private final Interval[][] lhs;
  private final Interval[] rhs;
  private final List<SLE> sles;
  
  public ISLE(int n, int m) {
    this.n = n;
    this.m = m;
    
    lhs = new Interval[n][m];
    for (Interval[] rows: lhs) {
      for (int i = 0; i < rows.length;i++) {
        rows[i] = new Interval();
      }
    }
    rhs = new Interval[n];
    for (int i = 0; i < rhs.length;i++) {
      rhs[i] = new Interval();
    }
    
    sles = new ArrayList<>();
  }
  
  public List<SLE> generate(int n) {
    for (int i = 0; i < n;i++) {
      sles.add(getSLE());
    }
    
    return sles;
  }

  private boolean controlled(Point point, double[] column) {
    final Interval a_00 = lhs[0][0];
    final Interval a_10 = lhs[1][0];
    final RealMatrix matrix = new RealMatrix(2, 2);
    
    for (double x = a_00.getLeft(); x <= a_00.getRight(); x+= 0.5) {
      for (double y = a_10.getLeft(); y <= a_10.getRight(); y += 0.5) {
        matrix.set(0, 0, x);
        matrix.set(0, 1, (column[0] - x * point.x) / point.y);
        matrix.set(1, 0, y);
        matrix.set(1, 1, (column[1] - y * point.x) / point.y);
        
        if (belongs(matrix)) {
          return true;
        }
      }
    }
    
    return false;
  }
  
  public boolean controlled(Point point) {
   
    for (int i = 0; i < 100;i++) {
      final Matrix x = MatrixUtils.transpose(
          MatrixUtils.toMatrix(new double[]{point.x, point.y})
      );
      
      double[] column = sles.get(i).getRhs();
      
      if (controlled(point, column)) {
        return false;
      }
    }
    return true;
  }
    
  public boolean belongs(double[] vector) {
    if (rhs.length == vector.length) {
      for (int i = 0; i < rhs.length; i++) {
        final double value = vector[i];
        if (value < rhs[i].getLeft() || rhs[i].getRight() < value) {
          return false;
        }
      }
      return true;
    }
    
    return false;
  }

  public boolean belongs(Matrix matrix) {
    if (lhs.length == matrix.height()) {
      
      for (int i = 0; i < matrix.height();i++) {
        for (int j = 0; j < matrix.width();j++) {
          if (!lhs[i][j].in(matrix.get(i, j))) {
            return false;
          }
        }
      }
      
      return true;
    }
    
    return false;
  }
  
  /**
   *
   * @param i
   * @param j
   * @return
   */
  @JsonIgnore
  public Interval get(int i, int j) {
    return lhs[i][j];
  }
  
  /**
   *
   * @param i
   * @return
   */
  @JsonIgnore
  public Interval get(int i) {
    return rhs[i];
  }
  
  /**
   * set lhs values (left hand side)
   * @param i
   * @param j
   * @param left
   * @param right
   * @return this
   */
  public ISLE set(int i, int j, double left, double right) {
    this.lhs[i][j].setLeft(left);
    this.lhs[i][j].setRight(right);
    return this;
  }
  
  /**
   * add rhs values (right hand side)
   * @param i
   * @param left
   * @param right
   * @return this
   */
  @JsonIgnore
  public ISLE set(int i, double left, double right) {
    rhs[i].setLeft(left);
    rhs[i].setRight(right);
    return this;
  }
  
  /**
   *
   * @return
   */
  public int width() {
    return m;
  }
  
  /**
   *
   * @return
   */
  public int height() {
    return n;
  }
  
  private double random() {
    final Random random = new Random();
    final int N = 12;
    double sum = 0;
    for (int i = 0; i < N;i++) {
      sum += random.nextDouble();
    }
    return random.nextDouble();
  }
  
  private double random(double a, double b) {    
    return a + random() * Math.abs(b - a);
  }
  
  private double random(int i, int j) {
    return random(
        lhs[i][j].getLeft(),
        lhs[i][j].getRight()
    );
  }
  
  @JsonIgnore
  public SLE getSLE() {
    Matrix realMatrix;
    int it = 0;
    do {
      realMatrix = new RealMatrix(n, m);
      for (int i = 0; i < n;i++) {
        for (int j = 0; j < m;j++) {
          realMatrix.set(i, j, random(i, j));
        }
      }
      it++;
    } while(!MatrixUtils.isDiagonalDominant(realMatrix) && it < 50);
   
    final double values[] = new double[realMatrix.height()];
    for (int i = 0; i < realMatrix.height();i++) {
      values[i] = random(rhs[i].getLeft(), rhs[i].getRight());
    }
    return new SLE(realMatrix, values);
  }

  @Override
  public String toString() {
    return "{" + "n=" + n + ",\n"
        + "m=" + m + ",\nmatrix=" + lhs + ",\nrhs=" + Arrays.toString(rhs) + '}';
  }
  
}

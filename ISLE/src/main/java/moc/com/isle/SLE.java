package moc.com.isle;

import java.util.Arrays;

/**
 *
 * @author Raim
 */
public class SLE {
  private final Matrix lhs;
  private final double[] rhs;

  public SLE(Matrix lhs, double[] rhs) {
    this.lhs = lhs;
    this.rhs = rhs;
  }

  public Matrix getLhs() {
    return lhs;
  }

  public double[] getRhs() {
    return rhs;
  }

  @Override
  public String toString() {
    return lhs + "\n, " + Arrays.toString(rhs);
  }
  
}

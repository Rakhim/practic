package moc.com.isle;

/**
 *
 * @author Raim
 */
public class DoubleScalar implements Scalar<Double> {
  private final double x;
  
  public DoubleScalar(double x) {
    this.x = x;
  }

  @Override
  public Scalar<Double> add(Double x) {
    return new DoubleScalar(this.x + x);
  }

  @Override
  public Scalar<Double> sub(Double x) {
    return new DoubleScalar(this.x - x);
  }

  @Override
  public Scalar<Double> mul(Double x) {
    return new DoubleScalar(this.x * x);
  }

  @Override
  public Scalar<Double> div(Double x) {
    return new DoubleScalar(this.x / x);
  }

  @Override
  public Double value() {
    return x;
  }
  
}

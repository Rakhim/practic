package moc.com.isle;

/**
 *
 * @author Raim
 */
public interface Scalar<T> {
  Scalar<T> add(T x);
  Scalar<T> sub(T x);
  Scalar<T> mul(T x);
  Scalar<T> div(T x);
  
  T value();
}

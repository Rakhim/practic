package moc.com.isle;

import java.util.Arrays;

/**
 */
public class MatrixUtils {

  public static boolean isDiagonalDominant(Matrix matrix) {
    boolean value = true;
    for (int i = 0; i < matrix.height() && value;i++) {
      double sum = 0.0;
      for (int j = 0; j < i && j < matrix.width(); j++) {
        sum += Math.abs(matrix.get(i, j));
      }
      
      for (int j = i + 1; j < matrix.width();j++) {
        sum += Math.abs(matrix.get(i, j));
      }
      value = Math.abs(matrix.get(i, i)) >= sum;
    }
    return value;
  }

  public static Matrix transpose(Matrix matrix) {
    Matrix result = new RealMatrix(matrix.width(), matrix.height());
    for (int i = 0; i < matrix.height();i++) {
      for (int j = 0; j < matrix.width();j++) {
        result.set(j, i, matrix.get(i, j));
      }
    }
    return result;
  }
  
  public static Matrix toMatrix(double[] values) {
    final Matrix matrix = new RealMatrix(1, values.length);
    for (int i = 0; i < values.length;i++) {
      matrix.set(0, i, values[i]);
    }
    return matrix;
  }
  
  public static Matrix multiply(Matrix left, Matrix right) {
    Matrix matrix = new RealMatrix(left.height(), right.width());

    for (int i = 0; i < left.height(); i++) {
      for (int j = 0; j < right.width(); j++) {
        matrix.set(i, j, multiply(left.row(i), right.column(j)));
      }
    }
    return matrix;
  }

  public static double multiply(double[] left, double[] right) {
    double summ = 0;
    for (int i = 0; i < left.length; i++) {
      summ += left[i] * right[i];
    }
    return summ;
  }

  public static double[] join(double[] left, double[] right) {
    double[] result = new double[left.length + right.length];
    int k = 0;
    for (double x: left) {
      result[k++] = x;
    }
    
    for (double x: right) {
      result[k++] = x;
    }
    
    return result;
  }
  
  public static Matrix add(Matrix left, Matrix right) {
    final Matrix matrix = new RealMatrix(left.height(), left.width());
    for (int i = 0; i < left.height();i++) {
      for (int j =0;j < left.width();j++) {
        matrix.set(i, j, left.get(i, j) + right.get(i, j));
      }
    }
    return matrix;
  }
  
  // matrix - исходная матрица
  // b - столбец правых частей
  // для СЛАУ Jordann-Gauss System of Linear Equations
  public static double[] solutionJGSLE(SLE sle) {
    final Matrix matrix = sle.getLhs();
    final double[] b = sle.getRhs();
    
    final double[] result = Arrays.copyOf(b, b.length);

    RealMatrix source = new RealMatrix(matrix);

    final int n = source.width();
    int[] ct = new int[n];

    for (int i = 0; i < n; i++) {
      double max = source.get(i, i);
      int imax = i;
      int jmax = i;

      for (int j = i; j < n; j++) {
        if (source.get(i, j) > max) {
          jmax = j;
          max = source.get(i, j);
        }
      }
      ct[i] = jmax;

      source.swapString(imax, i);
      source.swapColumn(jmax, i);

      final double t1 = result[imax];
      result[imax] = result[i];
      result[i] = t1;

      if (source.get(i, i) != 0) {
        final double t = 1.0 / source.get(i, i);
        for (int j = i; j < n; j++) {
          source.set(i, j, source.get(i, j) * t);
        }

        result[i] *= t;

        for (int k = 0; k < i; k++) {
          final double s = source.get(k, i);
          for (int l = i; l < n; l++) {
            source.set(k, l, source.get(k, l) - source.get(i, l) * s);
          }

          result[k] -= result[i] * s;
        }
      }
      
      for (int k = i + 1; k < n; k++) {
        final double t2 = source.get(k, i);
        for (int l = i; l < n; l++) {
          source.set(k, l, source.get(k, l) - source.get(i, l) * t2);
        }

        result[k] -= result[i] * t2;
      }
    }

    for (int i = n - 1; i >= 0; i--) {
      final double t = result[ct[i]];
      result[ct[i]] = result[i];
      result[i] = t;
    }
    
    return result;
  }
}

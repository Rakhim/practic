package moc.com.isle;

import java.awt.Color;
import java.util.Arrays;

/**
 *
 * @author Raim
 */
public class GaussSeidelVisualizer implements Paintable  {
  private final ISLE isle;
  
  public GaussSeidelVisualizer(ISLE isle) {
    this.isle = isle;
  }
  
  public double[] solution(SLE sle, double[] x) {
    final double[] result = Arrays.copyOf(x, x.length);
    
    final Matrix matrix = sle.getLhs();
    final double[] b = sle.getRhs();
    
    for (int it = 0; it < 500;it++) {
      for (int i = 0; i < matrix.width();i++) {
        double y = 0.0;
        
        for (int j = 0; j < matrix.height();j++) {
          if (i != j) {
            y += matrix.get(i, j) * result[j];
          }
        }
        
        result[i] = 1. / matrix.get(i, i) * (b[i] - y);
      }
    }
    
//    final double[] solutionJG = MatrixUtils.solutionJGSLE(sle);
//    final double[] solutionGS = result;
//    System.out.println("JG " + Arrays.toString(solutionJG));    
//    System.out.println("GS " + Arrays.toString(solutionGS));
    
    return result;
  }
  
  private Interval[] solution(ISLE isle, Interval[] x, Panel panel) {
    final Interval[] result = Arrays.copyOf(x, x.length);
    
    for (int it = 0; it < 200;it++) {
      paint(result, panel);
      
      for (int i = 0; i < isle.width();i++) {
        Interval sum = new Interval(0, 0);
        
        for (int j = 0; j < isle.height();j++) {
          if (i != j) {
            sum = sum.add(isle.get(i, j).mul(result[j]));
          }
        }
        
        final Interval value = (isle.get(i).sub(sum).div(isle.get(i, i)));        
        result[i] = value.intersect(result[i]);
      }
    }
        
    return result;
  }

  private void paint(Interval[] x, Panel panel) {
    System.out.println("GSV::paint:: " + Arrays.toString(x));
    if (x[0].isPoint() || x[1].isPoint()) {
      return;
    }

    panel.drawLine(x[0].getLeft(), x[1].getRight(),  x[0].getRight(), x[1].getRight(), Color.PINK);
    panel.drawLine(x[0].getLeft(), x[1].getLeft(),  x[0].getRight(), x[1].getLeft(), Color.PINK);
    
    panel.drawLine(x[0].getLeft(), x[1].getLeft(),  x[0].getLeft(), x[1].getRight(), Color.PINK);
    panel.drawLine(x[0].getRight(), x[1].getLeft(),  x[0].getRight(), x[1].getRight(), Color.PINK);
  }
  
  @Override
  public void paint(Panel panel) {
    final Interval[] x = new Interval[2];
    x[0] = new Interval(-55, 55);
    x[1] = new Interval(-55, 55);
    
    solution(isle, x, panel);
  }
  
}

package moc.com.isle;

import java.util.Arrays;

/**
 * Created by Raim on 09.12.2016.
 */
public class RealMatrix implements Matrix {

  private final double elements[][];
  private final int n;
  private final int m;

  public RealMatrix(int n, int m) {
    this.n = n;
    this.m = m;
    elements = new double[n][];
    
    for (int i = 0; i < n; i++) {
      elements[i] = new double[m];
    }
    zero();
  }

  public RealMatrix(Matrix matrix) {
    m = matrix.width();
    n = matrix.height();
    elements = new double[n][];
    
    for (int i = 0; i < n; i++) {
      elements[i] = matrix.row(i);
    }
  }

  private void zero() {
    for (double[] vector : elements) {
      for (int i = 0; i < vector.length; i++) {
        vector[i] = 0;
      }
    }
  }

  public double max() {
    double value = 0;
    for (double[] vector : elements) {
      for (double element: vector) {
        if (value < element) {
          value = element;
        }
      }
    }
    
    return value;
  }
  
  
  @Override
  public double get(int i, int j) {
    if (i < 0 || j < 0 || i >= elements.length || j >= elements[0].length) {
      return 0.0;
    }
    return elements[i][j];
  }

  @Override
  public Matrix set(int i, int j, double value) {
    if (i < 0 || j < 0 || i >= elements.length || j >= elements[0].length) {
      return this;
    }
    elements[i][j] = value;
    return this;
  }

  @Override
  public double[] row(int i) {
    return Arrays.copyOf(elements[i], m);
  }

  @Override
  public double[] column(int j) {
    final double[] vector = new double[n];
    
    for (int i = 0; i < n; i++) {
      vector[i] = elements[i][j];
    }
    return vector;
  }

  @Override
  public int width() {
    return m;
  }

  @Override
  public int height() {
    return n;
  }

  public void swapString(int i, int k) {
    if (i == k) {
      return;
    }
    
    final double[] p = elements[i];
    elements[i] = elements[k];
    elements[k] = p;
  }

  public void swapColumn(int k, int l) {
    if (k == l) {
      return;
    }

    for (int i = 0; i < n; i++) {
      final double t = elements[i][k];
      elements[i][k] = elements[i][l];
      elements[i][l] = t;
    }
  }


  @Override
  public void print() {
    System.out.println(this);
  }
  
  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    
    for (double[] vector : elements) {
      for (int i = 0; i < vector.length; i++) {
        builder.append(String.format("%4.3f   ", vector[i]));
      }
      builder.append('\n');
    }
    return builder.toString();
  }
}


package groupid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="text">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{}Base">
 *                   &lt;sequence>
 *                     &lt;element ref="{}one"/>
 *                   &lt;/sequence>
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="textarea">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{}Base">
 *                   &lt;sequence>
 *                     &lt;element ref="{}second"/>
 *                   &lt;/sequence>
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "text",
    "textarea"
})
@XmlRootElement(name = "field")
public class Field {

    protected Field.Text text;
    protected Field.Textarea textarea;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link Field.Text }
     *     
     */
    public Field.Text getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link Field.Text }
     *     
     */
    public void setText(Field.Text value) {
        this.text = value;
    }

    /**
     * Gets the value of the textarea property.
     * 
     * @return
     *     possible object is
     *     {@link Field.Textarea }
     *     
     */
    public Field.Textarea getTextarea() {
        return textarea;
    }

    /**
     * Sets the value of the textarea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Field.Textarea }
     *     
     */
    public void setTextarea(Field.Textarea value) {
        this.textarea = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{}Base">
     *       &lt;sequence>
     *         &lt;element ref="{}one"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "one"
    })
    public static class Text
        extends Base
    {

        @XmlElement(required = true)
        protected String one;

        /**
         * Gets the value of the one property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOne() {
            return one;
        }

        /**
         * Sets the value of the one property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOne(String value) {
            this.one = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{}Base">
     *       &lt;sequence>
     *         &lt;element ref="{}second"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "second"
    })
    public static class Textarea
        extends Base
    {

        @XmlElement(required = true)
        protected String second;

        /**
         * Gets the value of the second property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSecond() {
            return second;
        }

        /**
         * Sets the value of the second property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSecond(String value) {
            this.second = value;
        }

    }

}


package groupid;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the groupid package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _One_QNAME = new QName("", "one");
    private final static QName _Second_QNAME = new QName("", "second");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: groupid
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link Root }
     * 
     */
    public Root createRoot() {
        return new Root();
    }

    /**
     * Create an instance of {@link Field.Text }
     * 
     */
    public Field.Text createFieldText() {
        return new Field.Text();
    }

    /**
     * Create an instance of {@link Field.Textarea }
     * 
     */
    public Field.Textarea createFieldTextarea() {
        return new Field.Textarea();
    }

    /**
     * Create an instance of {@link Base }
     * 
     */
    public Base createBase() {
        return new Base();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "one")
    public JAXBElement<String> createOne(String value) {
        return new JAXBElement<String>(_One_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "second")
    public JAXBElement<String> createSecond(String value) {
        return new JAXBElement<String>(_Second_QNAME, String.class, null, value);
    }

}

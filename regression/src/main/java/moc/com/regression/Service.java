package moc.com.regression;

import moc.com.regression.nn.Neuron;
import moc.com.regression.nn.activation.Activations;
import moc.com.regression.nn.layers.InputLayer;
import moc.com.regression.nn.layers.Layer;
import moc.com.regression.nn.layers.LayerImpl;
import moc.com.regression.ols.OrdinaryLeastSquares;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * @author Raim
 */
public final class Service {
    private static final String DEFAULT_FUNCTION = "y = cos(2 * PI * x)";

    private String currentFunctionLabel = DEFAULT_FUNCTION;
    private List<Point> trainingSet;
    private final OrdinaryLeastSquares polynomial = new OrdinaryLeastSquares();

    private Point origin;
    private int trainingSetSize = 10;
    private double errorVariance = 0;
    private int polynomialPower = 3;
    private double trainingSetLeftBorder = 0;
    private double trainingSetRightBorder = 1;
    private double scale = 1;
    private boolean showOriginalFunction = true;
    private boolean showOLSApproximationFunction = true;
    private boolean showNNApproximationFunction = true;
    private boolean showTrainingSet = true;
    private double regularizationFactor = 0;
    private double domainLeftBorder = -1.5;
    private double domainRightBorder = 1.5;
    private int lineThickness = 1;
    private double lineGranularity = 0.002;
    private int subSetsCount = 0;
    private Neuron neuron;
    private int iterationCount = 10000;
    private int hiddenLayerSize = 5;
    private int hiddenLayerCount = 1;
    private double learningRate = 0.05;

    final Map<String, Function<Double, Double>> functions = new HashMap<>();

    {
        functions.put(DEFAULT_FUNCTION, (x) -> Math.cos(2 * Math.PI * x));
        functions.put("y = 5 * x^3 + x^2 + 5", (x) -> 5 * x * x * x + x * x + 5);
        functions.put("y = x * sin(2 * PI * x)", (x) -> 0.9 * x * Math.sin(2 * Math.PI * x));
        functions.put("y = (x * sin(2 * PI * x))^2", (x) -> x * Math.sin(2 * Math.PI * x) * x);
        functions.put("y = 0.7 * x^6 + 0.9 * x^4 + 0.2 * x", x -> 0.7 * Math.pow(x, 6) + 0.9 * Math.pow(x, 4) + 0.2 * x);
        functions.put("y = sin(x) - sin(x) * cos(x)", (x) -> Math.sin(x) - Math.sin(x) * Math.cos(x));
        functions.put("y = sin(x)", Math::sin);
        functions.put("y = cos(x)", Math::cos);
        functions.put("y = tg(x)", Math::tan);
        functions.put("y = th(x)", Math::tanh);
        functions.put("y = cth(x)", (x) -> 1 / Math.tanh(x));
        functions.put("y = ch(x)", Math::cosh);
        functions.put("y = sh(x)", Math::sinh);
        functions.put("Гауссиан(0,1)", (x) -> (1 / Math.sqrt(2 * Math.PI)) * Math.exp(-x * x / 2));
        functions.put("Логистическая", (x) -> 1 / (1 + Math.exp(-x)));
        functions.put("sinc(x)", (x) -> Math.sin(x) / x);
    }

    public Service() {
        update();
        rebuildNN();
    }

    private void rebuildNN() {
        Layer layer = new InputLayer(1);
        for (int i = 0; i < hiddenLayerCount; i++) {
            layer = new LayerImpl(layer, Activations.HTAN, hiddenLayerSize, learningRate);
        }

        neuron = new LayerImpl(layer, Activations.IDENTITY, 1, learningRate).getNeurons().get(0);
    }

    private double random(double a, double b) {
        final Random random = new Random();
        return a + random.nextDouble() * Math.abs(b - a);
    }

    private List<Point> generateRandoms(int count, double errorVariance) {
        final List<Point> points = new ArrayList<>(count);
        final Function<Double, Double> currentFunction = functions.get(currentFunctionLabel);
        double value;
        double error;
        for (int i = 0; i < count; i++) {
            value = random(trainingSetLeftBorder, trainingSetRightBorder);
            error = currentFunction.evaluate(value) + random(-errorVariance / 2, errorVariance / 2);
            points.add(new Point(value, error));
        }
        return points;
    }

    public List<Point> getTrainingSet() {
        return trainingSet;
    }

    public void setTrainingSet(List<Point> trainingSet) {
        this.trainingSet = trainingSet;
    }

    public void regenerate() {
        setTrainingSet(generateRandoms(trainingSetSize, errorVariance));
    }

    public List<Point> join(List<Point> left, List<Point> right) {
        final List<Point> result = new ArrayList<>(left.size() + right.size());
        result.addAll(left);
        result.addAll(right);
        return result;
    }

    private List<Point> subList(List<Point> list, int left, int right) {
        final List<Point> result = new ArrayList<>(list.size() - right + left);
        for (int i = left; i < list.size() && i < right; i++) {
            result.add(list.get(i));
        }

        return result;
    }

    private List<Point> exclude(List<Point> list, int left, int right) {
        final List<Point> result = new ArrayList<>(list.size() - right + left);
        for (int i = 0; i < list.size() && i < left; i++) {
            result.add(list.get(i));
        }

        for (int i = right; i < list.size(); i++) {
            result.add(list.get(i));
        }

        return result;
    }

    public void recalculate() {
        if (subSetsCount != 0) {
            final int length = trainingSet.size() / subSetsCount;
            try {
                setSplitFactor(trainingSet.size() / length +
                        (trainingSet.size() % length == 0 ? 0 : 1));
            } catch (Exception e) {
            }

            double error = Double.MAX_VALUE;
            double currentError;
            for (int i = 2; i < 35; i++) {
                currentError = 0;
                for (int j = 0; j < subSetsCount; j++) {
                    int left = length * j;
                    int right = Math.min(left + length, trainingSet.size());

                    List<Point> result = exclude(trainingSet, left, right);

                    polynomial.train(result, i);
                    currentError += error(subList(trainingSet, left, right));
                }

                if (error > currentError) {
                    setPolynomialPower(i);
                    error = currentError;
                }
            }
        }

        polynomial.train(trainingSet, polynomialPower);
    }

    public void setCurrentFunction(String functionLabel) {
        this.currentFunctionLabel = functionLabel;
    }

    public String[] getFunctionLabels() {
        final Set<String> labels = functions.keySet();
        return labels.toArray(new String[0]);
    }

    public Function<Double, Double> getCurrentFunction() {
        return functions.get(currentFunctionLabel);
    }

    public Function<Double, Double> getOLSApproximation() {
        return polynomial;
    }

    public int getTrainingSetSize() {
        return trainingSetSize;
    }

    public void setTrainingSetSize(int trainingSetSize) {
        this.trainingSetSize = trainingSetSize;
    }

    public double getErrorVariance() {
        return errorVariance;
    }

    public void setErrorVariance(double errorVariance) {
        this.errorVariance = errorVariance;
    }

    public int getPolynomialPower() {
        return polynomialPower;
    }

    public void setPolynomialPower(int polynomialPower) {
        this.polynomialPower = polynomialPower;
    }

    public double getTrainingSetLeftBorder() {
        return trainingSetLeftBorder;
    }

    public double getTrainingSetRightBorder() {
        return trainingSetRightBorder;
    }

    public void setTrainingSetBounds(double trainingSetLeftBorder, double trainingSetRightBorder) {
        this.trainingSetLeftBorder = trainingSetLeftBorder;
        this.trainingSetRightBorder = trainingSetRightBorder;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public String getCurrentFunctionLabel() {
        return currentFunctionLabel;
    }

    public void setOrigin(int x, int y) {
        this.origin = new Point(x, y);
    }

    public Point getOrigin() {
        return origin;
    }

    public boolean isShowOriginalFunction() {
        return showOriginalFunction;
    }

    public boolean isShowOLSApproximationFunction() {
        return showOLSApproximationFunction;
    }

    public void setShowOriginalFunction(boolean showOriginalFunction) {
        this.showOriginalFunction = showOriginalFunction;
    }

    public void setShowOLSApproximationFunction(boolean showOLSApproximationFunction) {
        this.showOLSApproximationFunction = showOLSApproximationFunction;
    }

    public void setShowTrainingSet(boolean showTrainingSet) {
        this.showTrainingSet = showTrainingSet;
    }

    public boolean isShowTrainingSet() {
        return showTrainingSet;
    }

    public void update() {
        regenerate();
        recalculate();
    }

    public double error() {
        return error(trainingSet);
    }

    public double error(List<Point> points) {
        final Function<Double, Double> currentFunction = getCurrentFunction();
        double error = 0;
        for (Point point : points) {
            error += Math.abs(polynomial.evaluate(point.x) - currentFunction.evaluate(point.x));
        }
        return error;
    }

    public void setRegularizationFactor(double regularizationFactor) {
        this.regularizationFactor = regularizationFactor;
    }

    public double getDomainLeftBorder() {
        return domainLeftBorder;
    }

    public double getDomainRightBorder() {
        return domainRightBorder;
    }

    public void setDomainBounds(double domainLeftBorder, double domainRightBorder) {
        this.domainLeftBorder = domainLeftBorder;
        this.domainRightBorder = domainRightBorder;
    }

    public void setLineGranularity(double lineGranularity) {
        this.lineGranularity = lineGranularity;
    }

    public double getLineGranularity() {
        return lineGranularity;
    }

    public int getLineThickness() {
        return lineThickness;
    }

    public void setLineThickness(int lineThickness) {
        this.lineThickness = lineThickness;
    }

    public double getRegularizationFactor() {
        return regularizationFactor;
    }

    public int getSplitFactor() {
        return subSetsCount;
    }

    public void setSplitFactor(int splitFactor) throws Exception {
        this.subSetsCount = splitFactor < 0 || splitFactor > trainingSetSize ? trainingSetSize : splitFactor;
    }

    public boolean isShowNNApproximationFunction() {
        return showNNApproximationFunction;
    }

    public void setShowNNApproximationFunction(boolean show) {
        this.showNNApproximationFunction = show;
    }

    public Function<Double, Double> getNNApproximation() {
        return neuron;
    }

    public void train() {
        rebuildNN();

        for (int i = 0; i < iterationCount; i++) {
            for (Point point : trainingSet) {
                double localError = point.y - neuron.evaluate(point.x);
                neuron.train(point.x, localError);
            }
        }
    }

    public void reset() {
        neuron.reset();
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getLearningRate() {
        return neuron.getLearningRate();
    }

    public int getIterationCount() {
        return iterationCount;
    }

    public void setIterationCount(int iterationCount) {
        this.iterationCount = iterationCount;
    }

    public int getHiddenLayerSize() {
        return hiddenLayerSize;
    }

    public void setHiddenLayerSize(int hiddenLayerSize) {
        this.hiddenLayerSize = hiddenLayerSize;
    }

    public void putToTrainingSet(Point point) {
        trainingSet.add(point);
    }

    public int getHiddenLayerCount() {
        return hiddenLayerCount;
    }

    public void setHiddenLayerCount(int hiddenLayerCount) {
        this.hiddenLayerCount = hiddenLayerCount;
    }
}

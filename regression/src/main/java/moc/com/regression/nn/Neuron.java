/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.regression.nn;

import moc.com.regression.Function;

/**
 * @author Raim
 */
public interface Neuron extends Function<Double, Double> {
    void train(double x, double error);

    double bias();

    double getLearningRate();

    public void reset();
}

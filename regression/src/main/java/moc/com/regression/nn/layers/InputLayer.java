package moc.com.regression.nn.layers;

import moc.com.regression.nn.Identity;
import moc.com.regression.nn.Neuron;

import java.util.ArrayList;
import java.util.List;

public class InputLayer implements Layer {
    private final List<Neuron> neurons = new ArrayList<>();

    public InputLayer(int size) {
        for (int i = 0; i < size; i++) {
            neurons.add(new Identity());
        }
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }
}

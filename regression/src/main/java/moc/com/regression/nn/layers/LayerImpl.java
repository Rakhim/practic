package moc.com.regression.nn.layers;

import moc.com.regression.nn.Neuron;
import moc.com.regression.nn.NeuronImpl;
import moc.com.regression.nn.activation.Activation;

import java.util.ArrayList;
import java.util.List;

public class LayerImpl implements Layer {
    private final List<Neuron> neurons = new ArrayList<>();

    public LayerImpl(Layer previousLayer, Activation activation, int size, double learningRate) {
        for (int i = 0; i < size; i++) {
            neurons.add(new NeuronImpl(previousLayer.getNeurons(), activation, learningRate));
        }
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }
}

package moc.com.regression.nn.layers;

import moc.com.regression.nn.Neuron;

import java.util.List;

public interface Layer {
    List<Neuron> getNeurons();
}

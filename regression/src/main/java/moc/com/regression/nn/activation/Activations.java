package moc.com.regression.nn.activation;

public enum Activations implements Activation {
    HTAN() {
        @Override
        public Double derivate(Double x) {
            return 1.0 - Math.tanh(x) * Math.tanh(x);
        }

        @Override
        public Double evaluate(Double x) {
            return Math.tanh(x);
        }
    },

    IDENTITY() {
        @Override
        public Double derivate(Double x) {
            return 1.0;
        }

        @Override
        public Double evaluate(Double x) {
            return x;
        }
    },

    RELU() {
        @Override
        public Double evaluate(Double x) {
            return x > 0. ? x : 0.;
        }

        @Override
        public Double derivate(Double x) {
            return x > 0. ? 1 : 0.;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.regression.nn;

/**
 * @author Raim
 */
public class Identity implements Neuron {

    public Identity() {
    }

    @Override
    public Double evaluate(Double x) {
        return x;
    }

    @Override
    public void train(double x, double error) {
    }

    @Override
    public double bias() {
        return 0;
    }

    @Override
    public double getLearningRate() {
        return 0;
    }

    @Override
    public void reset() {
    }
}

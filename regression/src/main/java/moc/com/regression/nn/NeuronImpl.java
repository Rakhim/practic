package moc.com.regression.nn;

import moc.com.regression.nn.activation.Activation;

import java.util.List;

/**
 * @author Raim
 */
public final class NeuronImpl implements Neuron {
    private final Activation activation;
    private final List<Neuron> connections;
    private final double[] weights;
    private double bias;
    private final double learningRate;

    public NeuronImpl(List<Neuron> connections, Activation activation, double learningRate) {
        this.connections = connections;
        this.activation = activation;
        this.learningRate = learningRate;
        weights = new double[connections.size()];
        reset();
    }

    @Override
    public double bias() {
        return bias;
    }

    @Override
    public void train(double x, double deviation) {
        final double coeff = learningRate
                * deviation
                * activation.derivate(combination(x));

        for (int i = 0; i < weights.length; i++) {
            weights[i] += coeff * connections.get(i).evaluate(x);
        }

        bias += coeff;

        for (int i = 0; i < weights.length; i++) {
            connections.get(i).train(x, deviation);
        }
    }

    private double combination(double x) {
        double combination = 0.0;
        for (int i = 0; i < connections.size(); i++) {
            combination += connections.get(i).evaluate(x) * weights[i];
        }
        return combination + bias;
    }

    @Override
    public Double evaluate(Double x) {
        return activation.evaluate(combination(x));
    }

    @Override
    public double getLearningRate() {
        return learningRate;
    }

    @Override
    public void reset() {
        for (int i = 0; i < weights.length; i++) {
            weights[i] = Math.random() - 0.5;
        }
        bias = Math.random() - 0.5;

        for (Neuron neuron : connections) {
            neuron.reset();
        }
    }
}

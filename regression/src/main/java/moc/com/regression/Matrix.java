/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.regression;

/**
 * @author Raim
 */
public interface Matrix {

    double[] column(int j);

    double get(int i, int j);

    int height();

    void print();

    double[] row(int i);

    Matrix set(int i, int j, double value);

    int width();

}

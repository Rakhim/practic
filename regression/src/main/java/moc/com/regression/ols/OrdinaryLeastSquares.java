/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.regression.ols;

import java.util.Arrays;
import java.util.List;

import moc.com.regression.DiagonalMatrix;
import moc.com.regression.Function;
import moc.com.regression.Matrix;
import moc.com.regression.MatrixUtils;
import moc.com.regression.Point;
import moc.com.regression.RealMatrix;

/**
 * @author Raim
 */
public class OrdinaryLeastSquares implements Function<Double, Double> {
    private double[] coefficients;
    private double regularizationFactor;

    public double getRegularizationFactor() {
        return regularizationFactor;
    }

    public void setRegularizationFactor(double regularizationFactor) {
        this.regularizationFactor = regularizationFactor;
    }

    private double getIJ(List<Point> points, int i, int j) {
        double value = 0;
        for (Point point : points) {
            value += Math.pow(point.x, i + j);
        }
        return value;
    }

    private Matrix matrixForSelection(List<Point> points, int power) {
        final Matrix matrix = new RealMatrix(power + 1, power + 1);
        for (int i = 0; i < power + 1; i++) {
            for (int j = 0; j < power + 1; j++) {
                matrix.set(i, j, getIJ(points, i, j));
            }
        }
        return matrix;
    }

    private double getI(List<Point> points, int i) {
        double value = 0;
        for (Point point : points) {
            value += point.y * Math.pow(point.x, i);
        }
        return value;
    }

    private double[] rightSideOfEquation(List<Point> points, int power) {
        double[] vector = new double[power + 1];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = getI(points, i);
        }
        return vector;
    }

    public void train(List<Point> points, int power) {
        final Matrix matrix = matrixForSelection(points, power);
        final double[] regularizationMatrix = new double[matrix.width()];
        Arrays.fill(regularizationMatrix, regularizationFactor);

        coefficients = MatrixUtils.solutionJGSLE(
                MatrixUtils.add(matrix, new DiagonalMatrix(regularizationMatrix)),
                rightSideOfEquation(points, power)
        );
    }

    @Override
    public Double evaluate(Double x) {
        double value = 0;
        for (int i = 0; i < coefficients.length; i++) {
            value += Math.pow(x, i) * coefficients[i];
        }
        return value;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(coefficients[0]);

        for (int i = 1; i < coefficients.length - 1; i++) {
            stringBuilder
                    .append(coefficients[i] > 0 ? " + " : "")
                    .append(coefficients[i])
                    .append(" * x^")
                    .append(i);
        }

        stringBuilder
                .append(coefficients[coefficients.length - 1] > 0 ? " + " : "")
                .append(coefficients[coefficients.length - 1])
                .append(" * x^")
                .append(coefficients.length - 1);

        return stringBuilder.toString();
    }
}

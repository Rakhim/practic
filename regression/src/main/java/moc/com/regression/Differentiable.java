package moc.com.regression;

/**
 * @author Raim
 */
public interface Differentiable extends Function<Double, Double> {
    Double derivate(Double x);
}

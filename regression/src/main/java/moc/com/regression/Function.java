/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.regression;

/**
 * @author Raim
 */
public interface Function<A, B> {
    B evaluate(A a);
}

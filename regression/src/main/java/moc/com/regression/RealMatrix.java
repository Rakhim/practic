package moc.com.regression;

import java.util.Arrays;

/**
 * Created by Raim on 09.12.2016.
 */
public class RealMatrix implements Matrix {

    private final double elements[][];
    private final int n;
    private final int m;

    public RealMatrix(int n, int m) {
        this.n = n;
        this.m = m;

        elements = new double[n][];
        for (int i = 0; i < n; i++) {
            elements[i] = new double[m];
        }
        zero();
    }

    public RealMatrix(Matrix matrix) {
        m = matrix.width();
        n = matrix.height();
        elements = new double[n][];
        for (int i = 0; i < n; i++) {
            elements[i] = matrix.row(i);
        }
    }

    private void zero() {
        for (double[] vector : elements) {
            for (int i = 0; i < vector.length; i++) {
                vector[i] = 0;
            }
        }
    }

    @Override
    public double get(int i, int j) {
        return elements[i][j];
    }

    @Override
    public Matrix set(int i, int j, double value) {
        elements[i][j] = value;
        return this;
    }

    @Override
    public double[] row(int i) {
        return Arrays.copyOf(elements[i], m);
    }

    @Override
    public double[] column(int j) {
        double[] vector = new double[n];
        for (int i = 0; i < n; i++) {
            vector[i] = elements[i][j];
        }
        return vector;
    }

    @Override
    public int width() {
        return m;
    }

    @Override
    public int height() {
        return n;
    }

    public void swapString(int i, int k) {
        if (i == k) {
            return;
        }
        double[] p = elements[i];
        elements[i] = elements[k];
        elements[k] = p;
    }

    public void swapColumn(int k, int l) {
        if (k == l) {
            return;
        }

        for (int i = 0; i < n; i++) {
            double t = elements[i][k];
            elements[i][k] = elements[i][l];
            elements[i][l] = t;
        }
    }

    @Override
    public void print() {
        for (double[] vector : elements) {
            for (int i = 0; i < vector.length; i++) {
                System.out.print(String.format("%4.3f   ", vector[i]));
            }
            System.out.println();
        }
    }
}

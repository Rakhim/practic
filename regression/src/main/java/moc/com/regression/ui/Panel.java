package moc.com.regression.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

import moc.com.regression.Function;
import moc.com.regression.Point;
import moc.com.regression.Service;

/**
 * @author Raim
 */
public final class Panel extends JPanel {
    private static final int POINT_SIZE = 2;
    private Graphics graphics;
    private final Service service;
    private final Frame frame;

    public Panel(Frame frame, Service service) {
        super(new BorderLayout());
        this.service = service;
        this.frame = frame;
    }

    @Override
    public void paintComponent(Graphics g) {
        graphics = g;
        final Point globalLeft = toGlobal(service.getTrainingSetLeftBorder(), 0);
        final Point globalRight = toGlobal(service.getTrainingSetRightBorder(), 0);
        final Color oldColor = graphics.getColor();
        graphics.setColor(Color.WHITE);
        graphics.fillRect((int) globalLeft.x, 0, (int) (Math.abs(globalLeft.x - globalRight.x)), getSize().height);
        graphics.setColor(oldColor);
        drawAxes();
        paint();
    }

    private Point origin() {
        return service.getOrigin();
    }

    public Point toGlobal(double x, double y) {
        final double xx = origin().x + x * (getSize().height / Math.PI) * service.getScale();
        final double yy = origin().y - y * (getSize().height / Math.PI) * service.getScale();
        return new Point(xx, yy);
    }

    public Point fromGlobal(double x, double y) {
        final double xx = (x - origin().x) / ((getSize().height / Math.PI) * service.getScale());
        final double yy = (origin().y - y) / ((getSize().height / Math.PI) * service.getScale());
        return new Point(xx, yy);
    }

    private void drawPoint(double x, double y, Color color, int diameter) {
        final Color current = graphics.getColor();
        graphics.setColor(color);
        final Point global = toGlobal(x, y);
        graphics.drawOval((int) (global.x) - diameter / 2, (int) (global.y) - diameter / 2, diameter, diameter);
        graphics.setColor(current);
    }

    private void fillPoint(double x, double y, Color color, int diameter) {
        final Color current = graphics.getColor();
        graphics.setColor(color);
        final Point global = toGlobal(x, y);
        graphics.fillOval((int) (global.x) - diameter / 2, (int) (global.y) - diameter / 2, diameter, diameter);
        graphics.setColor(current);
    }

    public void drawPoint(double x, double y, Color color) {
        drawPoint(x, y, color, service.getLineThickness());
    }

    public void fillPoint(double x, double y, Color color) {
        fillPoint(x, y, color, service.getLineThickness());
    }

    private void drawLabels() {
        final Dimension size = getSize();
        final int bound = (int) (size.height / Math.PI);

        for (int i = -bound; i < (bound + 1); i++) {
            fillPoint(i * Math.PI / 2, 0, Color.DARK_GRAY, 2 * POINT_SIZE + 1);
            fillPoint(0, i * Math.PI / 2, Color.DARK_GRAY, 2 * POINT_SIZE + 1);
        }
    }

    private void drawAxes() {
        final Dimension size = getSize();
        final Point origin = origin();

        graphics.setColor(Color.DARK_GRAY);
        graphics.drawLine(0, (int) origin.y, size.width, (int) origin.y);
        graphics.drawLine((int) origin.x, 0, (int) origin.x, size.height);
        drawLabels();
    }

    private void paint() {
        if (service.getSplitFactor() > 0) {
            frame.update();
        }

        final Function<Double, Double> currentFunction = service.getCurrentFunction();
        final Function<Double, Double> approximationOLS = service.getOLSApproximation();
        final Function<Double, Double> approximationNN = service.getNNApproximation();
        for (double x = service.getDomainLeftBorder(); x < service.getDomainRightBorder(); x += service.getLineGranularity()) {
            if (service.isShowOriginalFunction()) {
                final double value = currentFunction.evaluate(x);
                drawPoint(x, value, frame.getOriginalFunctionColor());
            }

            if (service.isShowOLSApproximationFunction()) {
                final double value = approximationOLS.evaluate(x);
                drawPoint(x, value, frame.getApproximationOLSFunctionColor());
            }

            if (service.isShowNNApproximationFunction()) {
                final double value = approximationNN.evaluate(x);
                drawPoint(x, value, frame.getApproximationNNFunctionColor());
            }
        }

        if (service.isShowTrainingSet()) {
            for (Point point : service.getTrainingSet()) {
                fillPoint(point.x, point.y, frame.getTrainingSetColor(), 2 * POINT_SIZE + 1);
                drawPoint(point.x, point.y, frame.getTrainingSetColor(), 2 * POINT_SIZE + 1);
            }
        }
    }
};
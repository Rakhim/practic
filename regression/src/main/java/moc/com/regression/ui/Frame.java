package moc.com.regression.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import moc.com.regression.Service;

/**
 * @author Raim
 */
public final class Frame extends JFrame {
    private static final int SCREEN_DEFAULT_WIDTH = 1000;
    private static final int SCREEN_DEFAULT_HEIGHT = 500;
    private final Service service;
    private final StatusBar statusBar;
    private final Settings settings;
    private Color trainingSetColor = Color.MAGENTA;
    private Color approximationOLSFunctionColor = Color.BLUE;
    private Color approximationNNFunctionColor = Color.GREEN;
    private Color originalFunctionColor = Color.ORANGE;

    public Frame(Service service) {
        this.service = service;
        statusBar = new StatusBar(service);
        settings = new Settings(this, service);

        final Panel panel = new Panel(this, service);
        panel.add(settings, BorderLayout.LINE_END);
        panel.add(statusBar, BorderLayout.PAGE_END);
        setContentPane(panel);
        pack();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(SCREEN_DEFAULT_WIDTH, SCREEN_DEFAULT_HEIGHT);

        service.setOrigin(SCREEN_DEFAULT_WIDTH / 6, SCREEN_DEFAULT_HEIGHT / 2);

        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.isAltDown()) {
                    service.setOrigin(e.getX(), e.getY());
                } else {
                    service.putToTrainingSet(panel.fromGlobal(e.getX(), e.getY()));
                }
                repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

    }

    public void update() {
        statusBar.setStatusBarText(String.valueOf(service.error()), service.getOLSApproximation().toString());
        settings.updateFormData();
    }

    public Color getTrainingSetColor() {
        return trainingSetColor;
    }

    public void setTrainingSetColor(Color trainingSetColor) {
        this.trainingSetColor = trainingSetColor;
    }

    Color getApproximationOLSFunctionColor() {
        return approximationOLSFunctionColor;
    }

    public void getApproximationOLSFunctionColor(Color approximationOLSFunctionColor) {
        this.approximationOLSFunctionColor = approximationOLSFunctionColor;
    }

    Color getOriginalFunctionColor() {
        return originalFunctionColor;
    }

    public void setOriginalFunctionColor(Color originalFunctionColor) {
        this.originalFunctionColor = originalFunctionColor;
    }

    public Color getApproximationNNFunctionColor() {
        return approximationNNFunctionColor;
    }

    public void setApproximationNNFunctionColor(Color approximationNNFunctionColor) {
        this.approximationNNFunctionColor = approximationNNFunctionColor;
    }
}
package moc.com.regression;

import moc.com.regression.ui.Frame;

/**
 * @author Raim
 */
public class Main {
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(() -> {
            new Frame(new Service()).setVisible(true);
        });
    }
}
 
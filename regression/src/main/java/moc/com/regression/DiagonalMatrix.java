/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.regression;

import java.util.Arrays;

/**
 * @author Raim
 */
public class DiagonalMatrix implements Matrix {
    private double[] elements;

    public DiagonalMatrix(double[] elements) {
        this.elements = Arrays.copyOf(elements, elements.length);
    }

    @Override
    public double[] column(int j) {
        return row(j);
    }

    @Override
    public double get(int i, int j) {
        return i == j ? elements[i] : 0;
    }

    @Override
    public int height() {
        return elements.length;
    }

    @Override
    public void print() {
        System.out.println("[");
        for (double element : elements) {
            System.out.print(" " + element);
        }
        System.out.println("]");
    }

    @Override
    public double[] row(int i) {
        double[] vector = new double[elements.length];
        Arrays.fill(vector, 0);
        vector[i] = elements[i];
        return vector;
    }

    @Override
    public Matrix set(int i, int j, double value) {
        elements[i] = i == j ? value : 0;
        return this;
    }

    @Override
    public int width() {
        return height();
    }

}

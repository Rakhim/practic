package groupid;

import junit.framework.TestCase;

import static groupid.HillCipher.decrypt;
import static groupid.HillCipher.encrypt;

public class HillCipherTest extends TestCase {

    public void testEncrypt() {
        final String plain = "Это зашифрованный текст.";
        final String encrypted = encrypt("ЙЦУКЕНГШЩ", plain);
        final String decrypted = decrypt("йниьйшумн", encrypted);
        assertEquals(plain.toLowerCase(), decrypted);
    }
}
package groupid;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Raim
 */
public class Vector {
    private final int[] elements;

    /**
     * Создать вектор из 0.0 длины n
     * @param n
     */
    public Vector(int n) {
        elements = new int[n];
    }

    public Vector(int... vector) {
        this.elements = Arrays.copyOf(vector, vector.length);
    }

    /**
     * Создание вектора по списку
     * @param list
     */
    public Vector(List<Integer> list) {
        elements = new int[list.size()];
        for (int i = 0; i < list.size();i++) {
            elements[i] = list.get(i);
        }
    }

    public Vector set(int i, int x) {
        elements[i] = x;
        return this;
    }

    public int get(int i) {
        return elements[i];
    }

    public int[] asArray() {
        return elements;
    }

    public int size() {
        return elements.length;
    }


    /**
     * Скалярное произведение
     * @param right
     * @return
     */
    public double mul(Vector right) {
        double value = 0.0;
        for (int i = 0; i < elements.length;i++) {
            value += elements[i] * right.asArray()[i];
        }
        return value;
    }


    /**
     * Произведение текущего вектора и скаляра x
     * @param x
     * @return
     */
    public Vector mul(int x) {
        final Vector value = new Vector(elements.length);
        for (int i = 0; i < elements.length;i++) {
            value.set(i, x * elements[i]);
        }
        return value;
    }


    /**
     * Cложить вектора записав результат в новый вектор
     * @param right
     * @return
     */
    public Vector add(Vector right) {
        final Vector value = new Vector(elements.length);
        for (int i = 0; i < elements.length;i++) {
            value.set(i, elements[i] + right.get(i));
        }
        return value;
    }


    /**
     * Вычесть из текущего (левого) вектора
     * вектор right (правый) записав результат в новый вектор
     * @param right
     * @return
     */
    public Vector sub(Vector right) {
        final Vector value = new Vector(elements.length);
        for (int i = 0; i < elements.length;i++) {
            value.set(i, elements[i] - right.get(i));
        }
        return value;
    }

    public Vector mod(int m) {
        final Vector newVector = new Vector(size());
        for (int i = 0; i < size();i++) {
            newVector.set(i, get(i) % m);
        }
        return newVector;
    }
}
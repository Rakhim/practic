package groupid;

public class Matrix {
    private final int[][] elements;
    private final int n;
    private final int m;

    public Matrix(int n, int m) {
        elements = new int[n][m];
        this.n = n;
        this.m = m;
    }

    public Matrix(Vector vector) {
        this(vector.size(), 1);
        for (int i = 0; i < vector.asArray().length;i++) {
            set(i, 0, vector.get(i));
        }
    }


    public int width() {
        return m;
    }

    public int height() {
        return n;
    }

    public void set(int i, int j, int x) {
        elements[i][j] = x;
    }

    public int get(int i, int j) {
        return elements[i][j];
    }

    public Vector row(int i) {
        final Vector result = new Vector(width());
        for (int k = 0; k < width();k++) {
            result.set(k, elements[i][k]);
        }
        return result;
    }

    public Vector column(int i) {
        final Vector result = new Vector(height());
        for (int k = 0; k < height();k++) {
            result.set(k, elements[k][i]);
        }
        return result;
    }
}
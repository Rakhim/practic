package groupid;

import java.util.*;

/**
 * Hello world!
 *
 */
public class HillCipher {
    private static final List<Character> alphabet = Arrays.asList(
        'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
        'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ъ', 'ы', 'э', 'ю', 'я',
        ' ', '.'
    );

    private static Map<Character, Integer> alphabet2numbers = new HashMap<>();
    private static Map<Integer, Character> numbers2alphabet = new HashMap<>();
    static {
        for (int i = 0; i < alphabet.size(); i++) {
            alphabet2numbers.put(alphabet.get(i), i);
            numbers2alphabet.put(i, alphabet.get(i));
        }
    }

    private HillCipher() {}

    private static Vector toVector(String string) {
        return new Vector(encode(string));
    }

    private static Matrix toMatrix(String string) {
        final int size = (int)(Math.sqrt(string.length()));
        final Matrix matrix = new Matrix(size, size);
        final List<Integer> encoded = encode(string.toLowerCase());
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix.set(i, j, encoded.get(i * size + j));
            }
        }

        return matrix;
    }

    /**
     * Зашифровать текст по ключу
     * @param keyword   -   ключ
     * @param plain     -   текст
     * @return          -   зашифрованный текст
     */
    public static String encrypt(String keyword, String plain) {
        final int size = (int)(Math.sqrt(keyword.length()));
        final Matrix matrix = toMatrix(keyword.toLowerCase());
        final String lower = plain.toLowerCase();
        final List<Integer> list = new ArrayList<>();
        for (int i = 0; i < plain.length(); i+= size) {
            final Vector block = toVector(lower.substring(i, i + size));
            final Vector vector = MatrixUtils.multiply(matrix, block).mod(alphabet.size());
            list.addAll(toList(vector.asArray()));
        }
        return decode(list);
    }

    /**
     * Дещифровать шифр-текст по ключу
     * @param keyword   -   ключ
     * @param plain     -   шифр-текст
     * @return          -   открытый текст
     */
    public static String decrypt(String keyword, String plain) {
        return encrypt(keyword, plain);
    }

    private static List<Integer> toList(int[] array) {
        final List<Integer> list = new ArrayList<>();
        for (int x: array) {
            list.add(x);
        }

        return list;
    }

    private static List<Integer> encode(String string) {
        final List<Integer> list= new ArrayList<>();
        for (Character character: string.toCharArray()) {
            list.add(alphabet2numbers.get(character));
        }
        return list;
    }

    private static String decode(List<Integer> list) {
        final StringBuilder builder = new StringBuilder();
        for (Integer x: list) {
            builder.append(numbers2alphabet.get(x));
        }
        return builder.toString();
    }
}

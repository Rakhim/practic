package groupid;

public class MatrixUtils {

    private MatrixUtils() {}

    private static Matrix multiply(Matrix left, Matrix right) {
        final Matrix matrix = new Matrix(left.height(), right.width());
        for (int i = 0; i < left.height(); i++) {
            for (int j = 0; j < right.width(); j++) {
                matrix.set(i, j, multiply(left.row(i), right.column(j)));
            }
        }
        return matrix;
    }

    private static int multiply(Vector left, Vector right) {
        return multiply(left.asArray(), right.asArray());
    }

    public static Vector multiply(Matrix left, Vector right) {
        return multiply(left, new Matrix(right)).column(0);
    }

    private static int multiply(int[] left, int[] right) {
        int summ = 0;
        for (int i = 0; i < left.length; i++) {
            summ += left[i] * right[i];
        }
        return summ;
    }

    public static Matrix add(Matrix left, Matrix right) {
        final Matrix matrix = new Matrix(left.height(), left.width());
        for (int i = 0; i < left.height();i++) {
            for (int j =0;j < left.width();j++) {
                matrix.set(i, j, left.get(i, j) + right.get(i, j));
            }
        }
        return matrix;
    }
}
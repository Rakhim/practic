/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.model.types;

/**
 *
 * @author Raim
 */
public interface Response {
  
  boolean isCacheHit();
  
  String getData();
}

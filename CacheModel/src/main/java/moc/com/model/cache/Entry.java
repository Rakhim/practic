package moc.com.model.cache;

/**
 *
 * @author Raim
 */
public final class Entry {
  private int address;
  private boolean referenced;
  private boolean modified;
  private boolean present;
  
  int getAddress() {
    return this.address;
  }

  boolean isReferenced() {
    return this.referenced;
  }

  boolean isModified() {
    return this.modified;
  }

  public void setAddress(int address) {
    this.address = address;
  }

  public void setReferenced(boolean referenced) {
    this.referenced = referenced;
  }

  public void setModified(boolean modified) {
    this.modified = modified;
  }

  public boolean isPresent() {
    return present;
  }

  public void setPresent(boolean present) {
    this.present = present;
  }  
  
  public int getCategory() {
    return referenced && modified  ? 3 :
           referenced && !modified ? 2 :
           !referenced && modified ? 1 : 
                                     0;
  }

}

package moc.com.model.cache;

import moc.com.model.Main;
import moc.com.model.addressgenerator.ResponseImpl;
import moc.com.model.types.Request;
import moc.com.model.types.Response;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raim
 */
public final class Cache {
  private final List<Entry> entries;
  private final int groupCount;
  private final int groupSize;
  private int timeout;
  private int requests;

  public Cache(int groupCount, int groupSize) {
    this.entries = new ArrayList<>(groupSize * groupCount);
    this.groupSize = groupSize;
    this.groupCount = groupCount;
    this.requests = 0;

    for (int i = 0; i < groupSize * groupCount;i++) {
      entries.add(new Entry());
    }
    
    Logger.getLogger(Main.class.getName()).log(Level.INFO,
      "Cache object has been created. size = {0}, groups = {1}, groupSize = {2}",
      new Object[]{entries.size(), groupCount, groupSize});

  }
  
  public void clearReferencedFlag() {
    for (Entry entry: entries) {
      entry.setReferenced(false);
    }
  }
  
  private int groupNumber(int address) {
    return address & (groupCount - 1);
  }
    
  public boolean process(Request request) {
    requests++;
    /**
     * TimeOut - грубо: количество запросов, спустя которое необходимо занулять
     * флаг чтения - reference flag
     * 
     * Таким образом мы будем знать, что записи у которых флаг чтения true
     * использовались недавно т.е. такие записи Recently Used
     * А все остальные - Not recently Used
     */
    if (timeout != 0 && requests % timeout == 0) {
      clearReferencedFlag();
      requests = 0;
    }

    /**
     * Найти запись (cache hit) или найти первого кандидата на замену
     * (cache miss)
     */
    final Result hitResult = find(request.getAddress());

    // is cache hit
    if (hitResult.status()) {
      if (request.isWrite()) {
        /**
         * Запись нового значения - модификация
         */        
        hitResult.getEntry().setModified(true);
      } else {
        /**
         * Скидывать ли тут флаг модификаций? 
         */
      }

      hitResult.getEntry().setReferenced(true);
    } else {
      /** 
       * 1. сбрасываем основную запись на диск
       * 2. Далее: заменяем найденную подходящую наиболее подходящую запись
       * Так как запись свежая, не имеет значение чтение ли это или запись
       */
      final Entry entry = hitResult.getEntry();
      if (request.isWrite()) {
        entry.setModified(false);
        entry.setReferenced(true);
      } else {
        entry.setModified(false);
        entry.setReferenced(true);
      }
      
      entry.setAddress(request.getAddress());
    }
    
    return hitResult.status();
  }

  private SortedMap<Integer, List<Entry>> categoryMap() {
    final SortedMap<Integer, List<Entry>> categories = new TreeMap<>();
    categories.put(0, new ArrayList<>());
    categories.put(1, new ArrayList<>());
    categories.put(2, new ArrayList<>());
    categories.put(3, new ArrayList<>());
    return categories;
  }
  
  /** Найти элемент или первого кандидата на замену.
   * @return 
   */
  private Result find(int address) {
    final SortedMap<Integer, List<Entry>> categories = categoryMap();
    final int groupBaseAddress = groupNumber(address) * groupSize;
    boolean isFound = false;
    
    Entry entry = null;
    for (int i = 0; i < groupSize && !isFound; i++) {
      entry = entries.get(groupBaseAddress + i);
      final int tag = entry.getAddress();
      if (tag == address) {
        return new Result(true, entry);
      }
      /*
        Добавить запись кэша в соответствующую категорию:
        0 - не модифицированы, не считаны
        1 - модифицированы, не считаны
        2 - не считаны, модифицироварны
        3 - считаны, модифицированы
      */
      categories.get(entry.getCategory()).add(entry);
    }
    
    for (List<Entry> category: categories.values()) {
      if (category.size() > 0) {
        /**
         * Возвращаем из наименьшей возможной категорий первую попавшуюся запись
         */
        return new Result(false, category.get(0));
      }
    }
    
    throw new RuntimeException("Не найдено ни одного элмента никакой категорий. (Невозможная ситуция)");
  }
  
  public Entry get(int index) {
    return entries.get(index);
  }
  
  public void set(int index, Entry entry) {
    entries.set(index, entry);
  }

  @Override
  public String toString() {
    return "Cache{" + "groupCount=" + groupCount + ", groupSize=" + groupSize + '}';
  }

  public void setTimeout(int timeout) {
      this.timeout = timeout;
  }

  private static class Result {
    private final boolean status;
    private final Entry entry;
    
    public Result(boolean status, Entry entry) {
      this.status = status;
      this.entry = entry;
    }

    public boolean status() {
      return status;
    }

    public Entry getEntry() {
      return entry;
    }
  }
}

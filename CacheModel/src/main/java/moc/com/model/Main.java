package moc.com.model;

import moc.com.model.addressgenerator.Generator;
import moc.com.model.cache.Cache;
import moc.com.model.types.Request;
import moc.com.model.ui.Panel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import moc.com.model.addressgenerator.RequestImpl;

/**
 *
 * @author Raim
 */
public class Main {
  private static String ADDRESSES_FILE_NAME = "adresses.txt";
  private static String FILE_EXTENSION = "png";
  private static String FILE_NAME_FORMAT = "result/file_locality%d_timeout%d." + FILE_EXTENSION;
  
  private static Request createReadRequest(int address) {
    return new RequestImpl(address, " for spatial locality", false, 0);
  }

  /**
   * Вернуть количество cache hit
   * @param cache           - кэш
   * @param requests        - список запросов
   * @param spatialLocality - пространственная локальность
   * @return количество cache hit
   * @throws Exception 
   */
  private static int hits(Cache cache, List<Request> requests, int spatialLocality) throws Exception {
    int hits = 0;
    for (Request request: requests) {
      hits += (cache.process(request) ? 1 : 0);
      final int startAddress = request.getAddress();
      for (int j = 1; j < spatialLocality; j++) {
        cache.process(createReadRequest(startAddress + j));
      }
    }
    return hits;
  }
  
  public static void main(String[] args) throws Exception {
    final Generator generator = new Generator(1, 300, 0.2, 0.5);
    generator.generate();
    generator.save(ADDRESSES_FILE_NAME);

    final List<Point> points = new ArrayList();
    final List<Request> requests = generator.getRequests();
    for (int timeout = 0; timeout < 5; timeout += 2) {
      for (int spatialLocality = 0; spatialLocality < 5; spatialLocality += 2) {
        points.clear();

        for (int index = 1; index < 10; index ++) {
          final Cache cache = new Cache(2, (int) Math.pow(2, index));
          cache.setTimeout(timeout);

          final int hits = hits(cache, requests, spatialLocality);
          points.add(new Point(index, hits / (double) requests.size()));
        }

        plot(String.format(FILE_NAME_FORMAT, spatialLocality, timeout), points);
      }
    }
  }
  
  public static void plot(String name, List<Point> points) {
    try {
      final BufferedImage image = new BufferedImage(1000, 500, BufferedImage.TYPE_INT_RGB);
      final Graphics2D graphics = image.createGraphics();
      graphics.setColor(Color.GREEN);
      final Panel panel = new Panel(graphics);
      panel.paint(points);
      ImageIO.write(image, FILE_EXTENSION, new File(name));
    } catch (IOException ex) {
      Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}

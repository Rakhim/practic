package moc.com.model.addressgenerator;

public final class Fractions {
    private final double cyclic;
    private final double linear;
    private final double functional;


    public Fractions(double cyclic, double linear) {
        this.cyclic = cyclic;
        this.linear = linear;
        this.functional = 1. - linear - cyclic;
    }

    public double getCyclic() {
        return cyclic;
    }

    public double getLinear() {
        return linear;
    }

    public double getFunctional() {
        return functional;
    }
}

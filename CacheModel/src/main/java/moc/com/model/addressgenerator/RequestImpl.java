package moc.com.model.addressgenerator;

import moc.com.model.types.Request;

/**
 *
 * @author Raim
 */
public class RequestImpl implements Request {
  private final int address;
  private final String label;
  private final boolean write;
  private final int level;
  
  /**
   *
   * @param address - physical address
   * @param label   - label
   * @param write   - is write accesss
   * @param level   - stack level
   */
  public RequestImpl(int address, String label, boolean write, int level) {
    this.address = address;
    this.label = label;
    this.write = write;
    this.level = level;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < level;i++) {
      builder.append("---");
    }
    return String.format("%7d --%s   %s, write: %b", getAddress(), builder.toString(), getLabel(), isWrite());
  }

  @Override
  public int getAddress() {
    return address;
  }

  public String getLabel() {
    return label;
  }

  @Override
  public boolean isWrite() {
    return write;
  }

  public int getLevel() {
    return level;
  }
  
}

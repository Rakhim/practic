package moc.com.model.addressgenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import moc.com.model.types.Request;

/**
 *
 * @author Raim
 */
public class Generator {
  private static final String START_FUNCTION_LABEL_FORMAT = "start function length: %d";
  private static final String END_FUNCTION_LABEL_FORMAT   = "end   function length: %d";
  private static final String CYCLE_BLOCK_LABEL_FORMAT    = "|chunkSize: %d, length: %d (cycle)";
  private static final String LINEAR_BLOCK_LABEL_FORMAT   = "|               length: %d (linear)";
  private static final double CYCLE_BODY_SIZE = 0.3;

  private final List<Request> requests;
  private final int size;
  private final Fractions baseFractions;
  private final int baseAddress;

  private int currentFunctionEntryPoint = 100000;
  /**
   *
   * @param baseAddress - base address for the generated requests
   * @param size - the size of the generated code
   * @param linear - percentage of linear type of code
   * @param cyclic - percentage of cyclic type of code
   * percentage of functional type of code is (1 - linear - cyclic)
   * because of full group of hypothesis 
   */
  public Generator(int baseAddress, int size, double linear, double cyclic) {
    this.size = size;
    this.baseAddress = baseAddress;
    requests = new ArrayList<>(size);
    baseFractions = new Fractions(cyclic, linear);
  }

  private static List<Request> linearChunk(int start, int length, int level) {
    return linearChunk(
        start,
        length,
        String.format(LINEAR_BLOCK_LABEL_FORMAT, length),
        level
    );
  }
  
  /**
   * @length - length of linear type of code
   * terminal state.
   */
  private static List<Request> linearChunk(int start, int length, String label, int level) {
    final List<Request> chunk = new ArrayList<>(length);
    for (int i = 1; i < length + 1; i++) {
      chunk.add(new RequestImpl(start + i, label, false, level));
    }
    return chunk;
  }

  private int randomLength(int length) {
    // if length less then 20% of total length ..
    final boolean condition = length < (int) (0.2 * size) || length == 1;
    return (int) (condition ? length : Math.random() * length);
  }

  private static List<Request> cyclicChunk(int start, int length, int level) {
    final int chunkSize = (int)(length *  CYCLE_BODY_SIZE) < 2 ? 1 : (int)(length *  CYCLE_BODY_SIZE);
    final int cycleLength = length / chunkSize;
    final List<Request> chunk = linearChunk(
        start,
        chunkSize,
        String.format(CYCLE_BLOCK_LABEL_FORMAT, chunkSize, chunkSize * cycleLength),
        level
    );

    final List<Request> list = new ArrayList<>();
    for (int i = 0; i < cycleLength;i++) {
      list.addAll(chunk);
    }

    final int rest = length % chunkSize;
    for (int i = 0; i < rest;i++) {
      list.add(chunk.get(i));
    }
    return list;
  }
  
  private static State randomState() {
    final State[] states = new State[] {
      State.CYCLIC, State.LINEAR, State.FUNCTIONAL
    };
    return states[(int)(Math.random() * states.length)];
  }

  private List<Request> functionalChunk(Fractions fractions , int start, int totalLength, int level) {
    final int length = totalLength - 2;
    int linearLength = length < 3 ? length : (int)(fractions.getLinear() * length);
    int functionalLength = length < 3 ? 0 : (int)(fractions.getFunctional() * length);
    int cyclicLength = length < 3 ? 0 : length - linearLength - functionalLength;

    final List<Request> list = new ArrayList<>();
    list.add(new RequestImpl(
        start,
        String.format(START_FUNCTION_LABEL_FORMAT, totalLength),
        false,
        level
    ));
    int lastAddress = list.get(list.size() - 1).getAddress();
    while(linearLength + cyclicLength + functionalLength > 0) {
      switch (randomState()) {
        case LINEAR:
          if (linearLength > 0) {
            final int generate = randomLength(linearLength);
            linearLength -= generate;
            list.addAll(linearChunk(lastAddress, generate, level));
            lastAddress = list.get(list.size() - 1).getAddress();
          }
          break;
        case CYCLIC:
          if (cyclicLength > 0) {
            final int generate = randomLength(cyclicLength);
            cyclicLength -= generate;
            list.addAll(cyclicChunk(lastAddress, generate, level));
            lastAddress = list.get(list.size() - 1).getAddress();
          }
          break;
        case FUNCTIONAL:
          if (functionalLength > 0) {
            final int generate = randomLength(functionalLength);
            functionalLength -= generate;
            final Fractions newFractions = new Fractions(0.5, 0.5);
            final List<Request> generatedChunk = functionalChunk(newFractions, currentFunctionEntryPoint, generate, level + 1);
            list.addAll(generatedChunk);
            currentFunctionEntryPoint = generatedChunk.get(generatedChunk.size() - 1).getAddress() + 1;
          }
          break;
      }
    }

    list.add(new RequestImpl(
      lastAddress + 1,
      String.format(END_FUNCTION_LABEL_FORMAT, totalLength),
      false,
      level
    ));
    return list;
  }
  
  public void generate() {
    requests.addAll(functionalChunk(baseFractions, baseAddress, size, 0));
  }

  public List<Request> getRequests() {
    return requests;
  }
  
  public void save(String fileName) {
    try (OutputStream out = new FileOutputStream(new File(fileName))) {
      for (Request request: requests) {
        out.write(request.toString().getBytes());
        out.write('\n');
      }
      out.flush();
    } catch (Exception e) {
    }
  }
  
  private enum State {
    LINEAR,
    CYCLIC,
    FUNCTIONAL
  }
}

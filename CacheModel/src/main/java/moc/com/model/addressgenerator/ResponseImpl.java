package moc.com.model.addressgenerator;

import moc.com.model.types.Response;

/**
 *
 * @author Raim
 */
public class ResponseImpl implements Response {
  private final boolean chacheHit;
  
  public ResponseImpl(boolean chacheHit) {
    this.chacheHit = chacheHit;
  }
  
  @Override
  public boolean isCacheHit() {
    return chacheHit;
  }

  @Override
  public String getData() {
    return "stub";
  }
  
}

package moc.com.model.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;
import moc.com.model.Point;

/**
 *
 * @author Raim
 */
public final class Panel {
  private static final int POINT_SIZE = 2;
  private final Graphics graphics;
  private final double scaleX = 0.3;
  private final double scaleY = 2.0;
  
  public Panel(Graphics graphics) {
    this.graphics = graphics;
    drawAxes();
  }
  
  private Point origin() {
    return new Point(50, 450);
  }

  private Dimension getSize() {
    return new Dimension(1000, 500);
  }
  
  public Point toGlobal(double x, double y) {
    final double xx = origin().x + x * (getSize().height / Math.PI) * scaleX;
    final double yy = origin().y - y * (getSize().height / Math.PI) * scaleY;
    return new Point(xx, yy);
  }

  public Point fromGlobal(double x, double y) {
    final double xx = (x - origin().x) / ((getSize().height / Math.PI) * scaleX);
    final double yy = (origin().y - y) / ((getSize().height / Math.PI) * scaleY);
    return new Point(xx, yy);
  }
  
  private void drawPoint(double x, double y, Color color, int diameter) {
    final Color current = graphics.getColor();
    graphics.setColor(color);
    final Point global = toGlobal(x, y);
    graphics.drawOval((int)(global.x) - diameter / 2, (int)(global.y) - diameter / 2, diameter, diameter);
    graphics.setColor(current);
  }

  private void drawLine(double x1, double y1, double x2, double y2, Color color) {
    final Color current = graphics.getColor();
    graphics.setColor(color);
    final Point global1 = toGlobal(x1, y1);
    final Point global2 = toGlobal(x2, y2);

    graphics.drawLine((int)(global1.x), (int)(global1.y), (int)(global2.x), (int)(global2.y));
    graphics.setColor(current);
  }
  
  private void drawString(double x, double y, String string, Color color) {
    final Color current = graphics.getColor();
    graphics.setColor(color);
    Point global = toGlobal(x, y);
    graphics.drawString(string, (int)global.x, (int)global.y);
    graphics.setColor(current);
  }
  
  private void fillPoint(double x, double y, Color color, int diameter) {
    final Color current = graphics.getColor();
    graphics.setColor(color);
    final Point global = toGlobal(x, y);
    graphics.fillOval((int)(global.x) - diameter / 2, (int)(global.y) - diameter / 2, diameter, diameter);
    graphics.setColor(current);
  }
  
  public void drawPoint(double x, double y, Color color) {
    drawPoint(x, y, color, 1);
  }

  public void fillPoint(double x, double y, Color color) {
    fillPoint(x, y, color, 1);
  }
  
  private void drawLabels() {
    final Dimension size = getSize();
    final int bound = (int)(size.height / Math.PI);

    for (int i = -bound; i < (bound + 1); i++) {
      fillPoint(i * Math.PI / 2, 0, Color.DARK_GRAY, 2 * POINT_SIZE + 1);
      fillPoint(0, i * Math.PI / 2, Color.DARK_GRAY, 2 * POINT_SIZE + 1);
    }
  }

  private void drawAxes() {
    final Dimension size = getSize();
    final Point origin = origin();

    graphics.setColor(Color.DARK_GRAY);
    graphics.drawLine(0, (int)origin.y, size.width, (int)origin.y);
    graphics.drawLine((int)origin.x, 0, (int)origin.x, size.height);
    drawLabels();
  }

  public void paint(List<Point> points) {
    Point startPoint = points.get(0);
    for (Point point: points) {
      fillPoint(point.x, point.y, Color.BLUE, 2 * POINT_SIZE + 1);
      drawPoint(point.x, point.y, Color.BLUE, 2 * POINT_SIZE + 1);      
      drawLine(startPoint.x, startPoint.y, point.x, point.y, Color.DARK_GRAY);
      drawLine(point.x, 0, point.x, point.y, Color.DARK_GRAY);
      drawString(point.x, point.y, "" + point.y, Color.GREEN);
      drawString(point.x, 0, "" + (int)point.x, Color.GREEN);
      startPoint = point;
    }
  }
};
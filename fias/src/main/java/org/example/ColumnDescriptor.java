package org.example;

public interface ColumnDescriptor
{
	String getName();
}

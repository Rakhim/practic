package org.example;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.tensorflow.DataType;
import org.tensorflow.Graph;
import org.tensorflow.Operation;
import org.tensorflow.Tensor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Hello world!
 */
public class Main
{
	private static final String chars = "йцукенгшщзхъфывапролджээячсмитьбю";
	private static final int charsCount = chars.length();
	private static final String houseLabel = "дом";
	private static final String buildnumLabel = "корпус";
	private static final String flatLabel = "помещение";
	private static final String roomLabel = "комната";
	private static final String postalcodeLabel = "индекс";
	private static final List<String> shortnameList = Arrays.asList( "Респ", "АО","Аобл", "край", "обл", "г", "р-н", "ул",
	                                                                 houseLabel, buildnumLabel, flatLabel, roomLabel, postalcodeLabel);

	private static List<String> rooms = Arrays.asList("106-107",
	                                                  "106/2",
	                                                  "106а",
	                                                  "106б",
	                                                  "107",
	                                                  "107а",
	                                                  "108",
	                                                  "108-1",
	                                                  "108/1",
	                                                  "108/2",
	                                                  "10/8/9",
	                                                  "108а",
	                                                  "108А",
	                                                  "108Б",
	                                                  "108в",
	                                                  "108и",
	                                                  "108К",
	                                                  "109",
	                                                  "109-110",
	                                                  "109/1/2",
	                                                  "1098",
	                                                  "1099",
	                                                  "109Б",
	                                                  "10а",
	                                                  "10А",
	                                                  "10б",
	                                                  "10Б",
	                                                  "10в",
	                                                  "10В",
	                                                  "10ж",
	                                                  "10н",
	                                                  "10П",
	                                                  "10с",
	                                                  "10, цоколь",
	                                                  "10этаж1",
	                                                  "10этаж1литВ",
	                                                  "10этаж1литД",
	                                                  "11",
	                                                  "1-1",
	                                                  "1/1",
	                                                  "110",
	                                                  "1-10",
	                                                  "1/10",
	                                                  "1100",
	                                                  "1101",
	                                                  "110этаж1литА",
	                                                  "111",
	                                                  "1-11",
	                                                  "11-1",
	                                                  "11/1",
	                                                  "111-114",
	                                                  "11/11а",
	                                                  "11/11А/12",
	                                                  "11-12",
	                                                  "11,12",
	                                                  "114",
	                                                  "1-14",
	                                                  "1/14",
	                                                  "114а",
	                                                  "114А",
	                                                  "115",
	                                                  "115-116",
	                                                  "115/116",
	                                                  "116",
	                                                  "116-117",
	                                                  "116а",
	                                                  "116А",
	                                                  "117",
	                                                  "1-17",
	                                                  "117/А",
	                                                  "117а/1",
	                                                  "117/Б",
	                                                  "118",
	                                                  "1-18",
	                                                  "118/119",
	                                                  "118а",
	                                                  "118А",
	                                                  "118 А",
	                                                  "118этаж1",
	                                                  "119",
	                                                  "1-19",
	                                                  "119а",
	                                                  "11а",
	                                                  "1/1а",
	                                                  "11А",
	                                                  "11б",
	                                                  "1,1б",
	                                                  "11Б",
	                                                  "11б/11в",
	                                                  "11в",
	                                                  "11г",
	                                                  "11г/11д",
	                                                  "11д");

	private static final String newline = String.format("%n");

	private static String randomRoom()
	{
		return rooms.get(new Random().nextInt(rooms.size()) % rooms.size());
	}

	public static void makeNewlines(String sourceFileName) throws IOException
	{
		System.out.println("start.");

		final String targetFileName = sourceFileName.concat(".log");
		final File sourceFile = new File(sourceFileName);
		final FileInputStream inputStream = new FileInputStream(sourceFile);
		final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		final File file = new File(targetFileName);
		final FileWriter fileWriter = new FileWriter(file);

		int read;
		String readString;
		final char[] buffer = new char[500];

		int lines = 0;
		final long before = System.currentTimeMillis();
		while ((read = bufferedReader.read(buffer)) != -1)
		{
			readString = String.valueOf(buffer, 0, read);
			fileWriter.write(readString.replaceAll(">", ">\n"));
			fileWriter.flush();
			lines++;
		}

		fileWriter.close();
		System.out.printf("lines: '%d'; in '%s' %n", lines, System.currentTimeMillis() - before);
		System.out.println("end.");
	}

	public static void showPostgreSQLVersion()
	{
		final Configuration configuration = new Configuration();
		configuration.configure();

		final SessionFactory sessionFactory = configuration.buildSessionFactory();
		final Session session = sessionFactory.openSession();
		final Transaction transaction = session.beginTransaction();
		final Query query = session.createSQLQuery("select version()");
		System.out.println(query.uniqueResult());
		transaction.commit();
		session.close();
		sessionFactory.close();
	}

	public static void iterateOverAddress(String target, String sqlString)
	{
		final Configuration configuration = new Configuration();
		configuration.configure();

		final SessionFactory sessionFactory = configuration.buildSessionFactory();
		final StatelessSession session = sessionFactory.openStatelessSession();
		//final Transaction transaction = session.beginTransaction();

		final int start = 0;
		final int pageSize = 5000;
		final File resultFile = new File(target);
		try (
			final FileWriter fileWriter = new FileWriter(resultFile, true);
		)
		{
			fileWriter.write("адрес");
			fileWriter.write(";");

			for (String shortname : shortnameList)
			{
				if (!shortnameList.get(shortnameList.size() - 1).equals(shortname))
				{
					fileWriter.write(";");
				}
			}

			fileWriter.write(newline);

			final Query streetQuery = session.createSQLQuery(sqlString);
			int pageIndex = 0;
			boolean go = true;
			while (go)
			{
				final int pageStartIndex = start + pageIndex * pageSize;
				int absoluteIndex = pageStartIndex;
				try
				{
					streetQuery.setFirstResult(pageStartIndex);
					streetQuery.setMaxResults(pageSize);

					final List list = streetQuery.list();
					go = !list.isEmpty();

					for (Object row : list)
					{
						final Map<String, String> addressObjectMap = new HashMap<>();
						final Object[] columns = (Object[]) row;
						final String namesString = (String) columns[0];
						final String[] names = namesString.split("#");

						final String shortsString = (String) columns[1];
						final String[] shorts = shortsString.split("#");
						final StringBuilder addressBuilder = new StringBuilder();
						for (int i = 0; i < names.length; i++)
						{
							try
							{
								final String name = names[i];
								final String shortname = shorts[i];
								addressBuilder
									.append(shortname)
									.append(" ")
									.append(name)
									.append(i != (names.length - 1) ? ", " : "");

								addressObjectMap.put(shortname, name);
							}
							catch (Exception e)
							{
								System.out.println(String.format("%d %n %s %n %s", i, namesString, shortsString));
								throw e;
							}
						}

						fileWriter.write(addressBuilder.toString());
						fileWriter.write(";");

						//System.out.print(addressBuilder.toString());
						//System.out.print(";");

						for (String shortname : shortnameList)
						{
							final String object = addressObjectMap.get(shortname);
							if (object != null)
							{
								fileWriter.write(object);
								//System.out.print(object);
							}

							if (!shortnameList.get(shortnameList.size() - 1).equals(shortname))
							{
								fileWriter.write(";");
							}
							//System.out.print(";");
						}
						fileWriter.write(newline);
						//System.out.print(newline);

						absoluteIndex++;
					}

					fileWriter.flush();
				}
				catch (Exception e)
				{
					System.out.printf("absolute index='%d', last page index='%d'%n", absoluteIndex, pageIndex);
					throw e;
				}

				pageIndex++;
			}
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}

		System.out.println("end");

		//transaction.commit();
		session.close();
		sessionFactory.close();
	}

	public static void saveSqlAsCSV(String target, String sqlString)
	{
		final Configuration configuration = new Configuration();
		configuration.configure();

		final SessionFactory sessionFactory = configuration.buildSessionFactory();
		final StatelessSession session = sessionFactory.openStatelessSession();
		//final Transaction transaction = session.beginTransaction();

		final int start = 0;
		final int pageSize = 50_000;
		final File resultFile = new File(target);
		try (
			final FileWriter fileWriter = new FileWriter(resultFile, true);
		)
		{
			final Query streetQuery = session.createSQLQuery(sqlString);
			int pageIndex = 0;
			boolean go = true;
			while (go)
			{
				final int pageStartIndex = start + pageIndex * pageSize;
				int absoluteIndex = pageStartIndex;
				try
				{
					streetQuery.setFirstResult(pageStartIndex);
					streetQuery.setMaxResults(pageSize);

					final List list = streetQuery.list();
					go = !list.isEmpty();

					for (Object row : list)
					{
						final Optional<Object> rowOptional = Arrays.stream((Object[]) row)
							.map(v -> v == null ? "" : v)
							.reduce((x, y) -> x.toString().concat(";").concat(y.toString()));

						if (rowOptional.isPresent())
						{
							fileWriter.write(rowOptional.get().toString());
							fileWriter.write(newline);
						}

						absoluteIndex++;
					}

					fileWriter.flush();
					System.out.println("flushed page: " + pageIndex + "  " + System.currentTimeMillis());
				}
				catch (Exception e)
				{
					System.out.printf("absolute index='%d', last page index='%d'%n", absoluteIndex, pageIndex);
					throw e;
				}

				pageIndex++;
			}
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}

		System.out.println("end");

		//transaction.commit();
		session.close();
		sessionFactory.close();
	}

	private static void tail(int from, String source, String target) throws IOException
	{
		final File sourceFile = new File(source);
		final File file = new File(target);
		try (
			final FileInputStream inputStream = new FileInputStream(sourceFile);
			final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			final FileWriter fileWriter = new FileWriter(file);
		)
		{
			for (int i = 0; i < from; i++)
			{
				bufferedReader.readLine();
			}

			String line;
			while ((line = bufferedReader.readLine()) != null)
			{
				fileWriter.write(line);
				fileWriter.write("\n");
			}
		}
	}

	private static void uglify(String source, String target) throws IOException
	{
		System.out.println("start");
		final File sourceFile = new File(source);
		final File targetFile = new File(target);
		try (
			final FileReader sourceReader = new FileReader(sourceFile);
			final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
			final FileWriter targetWriter = new FileWriter(targetFile);
			final BufferedWriter targetBufferedWriter = new BufferedWriter(targetWriter);
		)
		{
			String line;
			int index = 0;
			final int pageSize = 5000;
			while ((line = sourceBufferedReader.readLine()) != null)
			{
				targetBufferedWriter.write(uglify(line, 1));
				targetBufferedWriter.write(newline);

				targetBufferedWriter.write(uglify(line, 2));
				targetBufferedWriter.write(newline);

				targetBufferedWriter.write(uglify(line, 3));
				targetBufferedWriter.write(newline);

				targetBufferedWriter.write(uglify(line, 4));
				targetBufferedWriter.write(newline);

				targetBufferedWriter.write(uglify(line, 5));
				targetBufferedWriter.write(newline);

				targetBufferedWriter.write(uglify(line, 6));
				targetBufferedWriter.write(newline);

				if (index % pageSize == 0)
				{
					targetBufferedWriter.flush();
				}

				index++;
			}
		}
		System.out.println("end");
	}

	private static String uglifyString(String line)
	{
		if (line.length() < 4)
		{
			return line;
		}

		final int positins = line.length() - 3;
		final int position = new Random().nextInt(positins);
		return line.substring(0, position + 3).concat(line.substring(position + 4));
	}

	private static String uglify(String line, int errorCount)
	{
		final int indexOf = line.indexOf(';');
		final String left = line.substring(0, indexOf);
		final String right = line.substring(indexOf);
		final Random random = new Random();
		return left
			.replaceAll(getRandomChar(left, random, errorCount), "")
			.concat(right);
	}

	private static String getRandomChar(String line, Random random, int count)
	{
		char[] randomChars = new char[count + 2];
		randomChars[0] = '[';
		for (int i = 0; i < count; i++)
		{
			final String lineChars = line.replaceAll("[^а-яА-Я0-9]", "");
			randomChars[i + 1] = lineChars.charAt(1 + random.nextInt(lineChars.length() - 1));
		}

		randomChars[count + 1] = ']';
		return String.valueOf(randomChars);
	}

	private static String getRandomChar(Random random)
	{
		return String.valueOf(chars.charAt(random.nextInt(charsCount)));
	}

	private static void uglifyFullAddresses(String source, String target, int skip) throws IOException
	{
		System.out.println("start");
		final File sourceFile = new File(source);
		final File targetFile = new File(target);
		try (
			final FileReader sourceReader = new FileReader(sourceFile);
			final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
			final FileWriter targetWriter = new FileWriter(targetFile);
			final BufferedWriter targetBufferedWriter = new BufferedWriter(targetWriter);
		)
		{
			final String csvColumnLabels = shortnameList.stream()
				.reduce((l, r) -> l + ";" + r)
				.get();

			targetBufferedWriter.write("line;");
			targetBufferedWriter.write(csvColumnLabels);
			targetBufferedWriter.write(newline);

			for (int i = 0; i < skip; i++)
			{
				sourceBufferedReader.readLine();
			}

			String line;
			int index = 0;
			final int pageSize = 5000;
			while ((line = sourceBufferedReader.readLine()) != null)
			{
				final String[] columns = line.split(";");
				final String[] names = columns[0].split("#");
				final String[] shorts = columns[1].split("#");

				// names, shortnames, housenum, buildnum, flatnumber, postalcode
				final String housenum = columns[2];
				final String buildnum = columns[3];
				final String flatnumber = columns[4];

				final List<Segment> fullSegments = new ArrayList<>(names.length);
				final Map<String, List<Segment>> segmentMap = new HashMap<>();

				if (columns.length > 5)
				{
					final String postalcode = columns[5];
					insertSegment(fullSegments, segmentMap, new Segment(postalcodeLabel, postalcode));
				}

				for (int i = 0; i < names.length; i++)
				{
					final Segment segment = new Segment(shorts[i], names[i]);
					final String shortname = segment.getLabel();
					fullSegments.add(segment);

					final List<Segment> segments = segmentMap.get(shortname);
					if (segments == null)
					{
						final ArrayList<Segment> newSegmentList = new ArrayList<>();
						newSegmentList.add(segment);

						segmentMap.put(segment.getLabel(), newSegmentList);
					}
					else
					{
						segments.add(segment);
					}
				}

				insertSegment(fullSegments, segmentMap, new Segment(houseLabel, housenum).addShortname("дом").addShortname("д").addShortname("д."));
				insertSegment(fullSegments, segmentMap, new Segment(buildnumLabel, buildnum).addShortname("к").addShortname("корп").addShortname("корпус"));
				insertSegment(fullSegments, segmentMap, new Segment(flatLabel, flatnumber).addShortname("пом").addShortname("пом.").addShortname("помещение"));
				insertSegment(fullSegments, segmentMap, new Segment(roomLabel, randomRoom()).addShortname("ком").addShortname("ком.").addShortname("комната"));

				final String csvColumns = shortnameList.stream()
					.map(shortname -> {
						final List<Segment> segmentsList = segmentMap.get(shortname);
						if (segmentsList == null)
						{
							return "";
						}

						return segmentsList.get(0).getName();
					})
					.reduce((l, r) -> l + ";" + r)
					.get();

				// write 1 обычный случай
				writeLine(targetBufferedWriter, csvColumns, fullSegments, Segment::getName);
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> uglifyString(s.getName()));
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> s.getShortname() + " " + s.getName());
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> s.getShortname() + " " + uglifyString(s.getName()));
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> uglifyString(s.getShortname()) + " " + s.getName());
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> uglifyString(s.getShortname()) + " " + uglifyString(s.getName()));

				// write 2 обычный случай
				Collections.shuffle(fullSegments);
				writeLine(targetBufferedWriter, csvColumns, fullSegments, Segment::getName);
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> uglifyString(s.getName()));
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> s.getShortname() + " " + s.getName());
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> s.getShortname() + " " + uglifyString(s.getName()));
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> uglifyString(s.getShortname()) + " " + s.getName());
				writeLine(targetBufferedWriter, csvColumns, fullSegments, s -> uglifyString(s.getShortname()) + " " + uglifyString(s.getName()));

				if (index % pageSize == 0)
				{
					targetBufferedWriter.flush();
					if (index > 0)
					{
						break;
					}
				}

				index++;
			}
		}
		System.out.println("end");
	}

	private static void insertSegment(
		List<Segment> fullSegments,
		Map<String, List<Segment>> segmentMap,
		Segment segment
	)
	{
		if (!segment.getName().isEmpty())
		{
			fullSegments.add(segment);
			segmentMap.put(segment.getLabel(), Collections.singletonList(segment));
		}
	}

	private static void writeLine(
		BufferedWriter targetBufferedWriter,
		String csvColumns,
		List<Segment> fullSegments,
		Function<Segment, String> mapper
	) throws IOException
	{
		final String fullAddress = fullSegments.stream()
			.map(mapper)
			.reduce((x, y) -> x + ", " + y)
			.get();

		targetBufferedWriter.write(fullAddress);
		targetBufferedWriter.write(";");

		targetBufferedWriter.write(csvColumns);
		targetBufferedWriter.write(newline);
	}

	private static void uglifyFullMoscow(String source, String target, int skip) throws IOException
	{
		System.out.println("start");
		final File sourceFile = new File(source);
		final File targetFile = new File(target);
		try (
			final FileReader sourceReader = new FileReader(sourceFile);
			final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
			final FileWriter targetWriter = new FileWriter(targetFile);
			final BufferedWriter targetBufferedWriter = new BufferedWriter(targetWriter);
		)
		{
			final String csvColumnLabels = shortnameList.stream()
				.reduce((l, r) -> l + ";" + r)
				.get();

			targetBufferedWriter.write("line;");
			targetBufferedWriter.write(csvColumnLabels);
			targetBufferedWriter.write(newline);

			for (int i = 0; i < skip; i++)
			{
				sourceBufferedReader.readLine();
			}

			final Random random = new Random();

			String line;
			int index = 0;
			final int pageSize = 5000;
			while ((line = sourceBufferedReader.readLine()) != null)
			{
				final String[] columns = line.split(";");
				final String[] names = columns[0].split("#");
				final String[] shorts = columns[1].split("#");
				final String num = columns[2];

				final Segment moscowSegment = new Segment("г", "Москва");
				final List<Segment> fullSegments = new ArrayList<>(names.length);
				fullSegments.add(moscowSegment);

				final Map<String, List<Segment>> segmentMap = new HashMap<>();
				final List<Segment> cities = new ArrayList<>();
				cities.add(moscowSegment);

				segmentMap.put(moscowSegment.getLabel(), cities);

				for (int i = 0; i < names.length; i++)
				{
					final Segment segment = new Segment(shorts[i], names[i]);
					final String shortname = segment.getLabel();
					fullSegments.add(segment);

					final List<Segment> segments = segmentMap.get(shortname);
					if (segments == null)
					{
						final ArrayList<Segment> newSegmentList = new ArrayList<>();
						newSegmentList.add(segment);

						segmentMap.put(segment.getLabel(), newSegmentList);
					}
					else
					{
						segments.add(segment);
					}
				}

				final Segment houseSegment = new Segment("дом", num);
				fullSegments.add(houseSegment);

				final List<Segment> houses = new ArrayList<>();
				houses.add(houseSegment);
				segmentMap.put(houseSegment.getLabel(), houses);

				final String csvColumns = shortnameList.stream()
					.map(shortname -> {
						final List<Segment> segmentsList = segmentMap.get(shortname);
						if (segmentsList == null)
						{
							return "";
						}

						return segmentsList.get(0).getName();
					})
					.reduce((l, r) -> l + ";" + r)
					.get();

				// write 1 все по порядку, корректно, с типами слева
				{
					final String fullAddress = fullSegments.stream()
						.map(s -> s.getLabel() + " " + s.getName())
						.reduce((x, y) -> x + ", " + y)
						.get();

					targetBufferedWriter.write(fullAddress);
					targetBufferedWriter.write(";");
					targetBufferedWriter.write(csvColumns);
					targetBufferedWriter.write(newline);
				}

				// write 2 все по порядку, корректно, с типами справа
				{
					final String fullAddress = fullSegments.stream()
						.map(s -> s.getName() + " " + s.getLabel())
						.reduce((x, y) -> x + ", " + y)
						.get();

					targetBufferedWriter.write(fullAddress);
					targetBufferedWriter.write(";");
					targetBufferedWriter.write(csvColumns);
					targetBufferedWriter.write(newline);
				}

				// write 3 все по порядку, корректно, без типов
				{
					final String fullAddress = fullSegments.stream()
						.map(Segment::getName)
						.reduce((x, y) -> x + ", " + y)
						.get();

					targetBufferedWriter.write(fullAddress);
					targetBufferedWriter.write(";");
					targetBufferedWriter.write(csvColumns);
					targetBufferedWriter.write(newline);
				}

				// write 4 перемешано, с ошибкой в имени, типы слева
				{
					Collections.shuffle(fullSegments);
					final String fullAddress = fullSegments.stream()
						.map(s -> s.getLabel() + " " + uglifyString(s.getName()))
						.reduce((x, y) -> x + ", " + y)
						.get();

					targetBufferedWriter.write(fullAddress);
					targetBufferedWriter.write(";");
					targetBufferedWriter.write(csvColumns);
					targetBufferedWriter.write(newline);
				}

				// write 5 перемешано, с ошибкой в имени, типы справа
				{
					Collections.shuffle(fullSegments);
					final String fullAddress = fullSegments.stream()
						.map(s -> uglifyString(s.getName()) + " " + s.getLabel())
						.reduce((x, y) -> x + ", " + y)
						.get();

					targetBufferedWriter.write(fullAddress);
					targetBufferedWriter.write(";");
					targetBufferedWriter.write(csvColumns);
					targetBufferedWriter.write(newline);
				}

				// write 6 перемешано, с ошибкой имени, положение случайно
				for (int i = 0; i < 5; i++)
				{
					Collections.shuffle(fullSegments);
					final String fullAddress = fullSegments.stream()
						.map(s -> {
							final String left = s.getLabel();
							final String right = uglifyString(s.getName());
							final boolean hasType = random.nextBoolean();
							if (hasType)
							{
								final boolean isLeftSide = random.nextBoolean();
								return isLeftSide
									? left + " " + right
									: right + " " + left;
							}

							return right;
						})
						.reduce((x, y) -> x + ", " + y)
						.get();

					targetBufferedWriter.write(fullAddress);
					targetBufferedWriter.write(";");
					targetBufferedWriter.write(csvColumns);
					targetBufferedWriter.write(newline);
				}

				if (index % pageSize == 0)
				{
					targetBufferedWriter.flush();
//					if (index > 0)
//					{
//						break;
//					}
				}

				index++;
			}
		}
		System.out.println("end");
	}

	private static String stringify(List<Segment> fullSegments)
	{
		return fullSegments.stream()
			.map(s -> s.getLabel() + " " + uglifyString(s.getName()))
			.reduce((x, y) -> x + ", " + y)
			.get();
	}

	public static int minDistance(String left, String right)
	{
		final int leftLength = left.length();
		final int rightLength = right.length();
		final int[][] distances = new int[leftLength + 1][rightLength + 1];

		for (int i = 0; i <= leftLength; i++)
		{
			distances[i][0] = i;
		}

		for (int j = 0; j <= rightLength; j++)
		{
			distances[0][j] = j;
		}

		//iterate though, and check last char
		for (int i = 0; i < leftLength; i++)
		{
			final char leftChar = left.charAt(i);
			for (int j = 0; j < rightLength; j++)
			{
				final char rightChar = right.charAt(j);

				//if last two chars equal
				if (leftChar == rightChar)
				{
					//update dp value for +1 length
					distances[i + 1][j + 1] = distances[i][j];
				}
				else
				{
					final int replace = distances[i][j] + 1;
					final int insert = distances[i][j + 1] + 1;
					final int delete = distances[i + 1][j] + 1;
					distances[i + 1][j + 1] = Math.min(delete, Math.min(replace, insert));
				}
			}
		}

		return distances[leftLength][rightLength];
	}

	private static void split(String source, int lines) throws IOException
	{
		System.out.println("start");
		final File sourceFile = new File(source);
		final File targetFile1 = new File(source.concat("1"));
		final File targetFile2 = new File(source.concat("2"));
		try (
			final FileReader sourceReader = new FileReader(sourceFile);
			final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
			final FileWriter targetWriter1 = new FileWriter(targetFile1);
			final BufferedWriter targetBufferedWriter1 = new BufferedWriter(targetWriter1);
			final FileWriter targetWriter2 = new FileWriter(targetFile2);
			final BufferedWriter targetBufferedWriter2 = new BufferedWriter(targetWriter2);
		)
		{
			int i = 0;
			String line;
			while ((line = sourceBufferedReader.readLine()) != null && i < lines)
			{
				targetBufferedWriter1.write(line);
				targetBufferedWriter1.write(newline);
				i++;
			}

			while ((line = sourceBufferedReader.readLine()) != null)
			{
				targetBufferedWriter2.write(line);
				targetBufferedWriter2.write(newline);
				i++;
			}
		}
		System.out.println("end.");
	}

	private static void showNodeListNames(NodeList nodeList, int size)
	{
		if (nodeList.getLength() != size)
		{
			return;
		}

		for (int i = 0; i < nodeList.getLength(); i++)
		{
			System.out.print(nodeList.item(i).getNodeName());
			System.out.print(", ");
		}

		System.out.println();
	}

	private static void toCsv(String xsd, String xmlLog) throws IOException, SAXException, ParserConfigurationException
	{
		final TableDescriptor tableDescriptor = TableDescriptorHelper.getTableDescriptor(xsd);
		System.out.println(tableDescriptor);

		final CsvRowBuilder csvRowBuilder = tableDescriptor.getCsvRowBuilder();

		final Pattern pattern = Pattern.compile("[a-zA-Z]+=\"[^\"]+\"");
		final String target = xmlLog.concat(".csv");

		System.out.println(tableDescriptor.getCsvHeader());

		final File sourceFile = new File(xmlLog);
		final File targetFile = new File(target);
		try (
			final FileReader sourceReader = new FileReader(sourceFile);
			final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
			final FileWriter targetWriter = new FileWriter(targetFile);
			final BufferedWriter targetBufferedWriter = new BufferedWriter(targetWriter);
		)
		{
			targetBufferedWriter.write(tableDescriptor.getCsvHeader());
			targetBufferedWriter.write(newline);

			int lines = 0;
			String line;
			while ((line = sourceBufferedReader.readLine()) != null)
			{
				csvRowBuilder.clear();

				try
				{
					final Matcher matcher = pattern.matcher(line);
					while (matcher.find())
					{
						final String group = matcher.group();
						final String[] split = group.split("=");
						final String label = split[0];
						final String quotedValue = split[1];
						final String value = quotedValue.substring(1, quotedValue.length() - 1);

						csvRowBuilder.fillColumn(label, value);
					}

					if (!csvRowBuilder.isEmpty())
					{
						targetBufferedWriter.write(csvRowBuilder.toString());
						targetBufferedWriter.write(newline);
					}
				}
				catch (Exception e)
				{

				}

				lines++;
				if (lines % 500_000 == 0)
				{
					System.out.println("lines: " + lines);
					targetBufferedWriter.flush();
				}
			}
		}
		System.out.println("end.");
	}
	private static void sed(String source) throws IOException, SAXException, ParserConfigurationException
	{
		System.out.println("start.");
		final File sourceFile = new File("D:\\download\\fias_data\\AS_HOUSE_20210207_9246061c-8e24-4820-8da1-6c5043111662.XML.log.csv.aoguid.2.csv");
		final File targetFile = new File("D:\\download\\fias_data\\AS_HOUSE_20210207_9246061c-8e24-4820-8da1-6c5043111662.XML.log.csv.aoguid.3.csv");
		try (
			final FileReader sourceReader = new FileReader(sourceFile);
			final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
			final FileWriter targetWriter = new FileWriter(targetFile);
			final BufferedWriter targetBufferedWriter = new BufferedWriter(targetWriter);
		)
		{
			int i = 1;
			String line;
			final Set<String> aoguidSet = new HashSet<>();
			while ((line = sourceBufferedReader.readLine()) != null) {
				aoguidSet.add(line);
				i++;
			}

			System.out.println(i);

			final ArrayList<String> aoguidList = new ArrayList<>(aoguidSet);
			aoguidList.sort(String::compareTo);

			for (String guid : aoguidList) {
				targetBufferedWriter.write(guid);
				targetBufferedWriter.write(newline);
			}

			targetBufferedWriter.flush();
			System.out.println(i);
		}
		System.out.println("end.");
	}

	private static boolean isOK(String[] row)
	{
		return isOK(row[5]);
	}

	private static boolean isOK(String endDateString)
	{
		if (endDateString == null || endDateString.isEmpty())
		{
			return false;
		}

		try
		{
			final Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(endDateString);
			return endDate.after(Calendar.getInstance().getTime());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	private static void join(String target, List<String> sources) throws IOException
	{
		System.out.println("start");

		final File targetFile = new File(target);
		try (
				final FileWriter targetWriter = new FileWriter(targetFile);
				final BufferedWriter targetBufferedWriter = new BufferedWriter(targetWriter);
		)
		{
			String line;
			for (String source : sources)
			{
				final File sourceFile = new File(source);
				try(
						final FileReader sourceReader = new FileReader(sourceFile);
						final BufferedReader sourceBufferedReader = new BufferedReader(sourceReader);
				)
				{
					while ((line = sourceBufferedReader.readLine()) != null)
					{
						targetBufferedWriter.write(line);
						targetBufferedWriter.write(newline);
					}
				}
			}
		}
		System.out.println("end.");
	}

	public static void main(String[] args) throws Exception
	{
		//RoomHelper.generateInsertsFromXMLAddressLines(
		//	"room",
		//	"fias/AS_ROOM_20210520_ed213ebd-ccf9-4b11-92ac-79d8ae6c841d.XML.log"
		//);

		//saveSqlAsCSV("fc.csv", "select names, shortnames, housenum, buildnum, flatnumber, postalcode from fc");

		//sed(
		//	"fias/AS_ROOM_2.XML.log.sql",
		//	"fias/AS_ROOM_3.XML.log.sql"
		//);

		//uglifyFullAddresses(
		//	"fc.csv",
		//	"result4.csv",
		//	0
		//);

		//makeNewlines("fias/AS_HOUSE_20210207_9246061c-8e24-4820-8da1-6c5043111662.XML");
		//toCsv("fias/Schemas/AS_HOUSE_2_250_02_04_01_01.XSD", "fias/AS_HOUSE_20210207_9246061c-8e24-4820-8da1-6c5043111662.XML.log");

		//System.out.println(TableDescriptorHelper.getTableDescriptor("fias/Schemas/AS_ADDROBJ_2_250_01_04_01_01.XSD"));

		// 81369158
		// split("fias/AS_HOUSE_20210207_9246061c-8e24-4820- 8da1-6c5043111662.XML.log.csv", 41369158);

		saveSqlAsCSV("addresses.csv", "select postalcode, offnames, shortnames from addresses ");
	}
}

package org.example;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class TableDescriptorHelper
{
	public static TableDescriptor getTableDescriptor(String path) throws ParserConfigurationException, IOException, SAXException
	{
		final FileReader reader = new FileReader(path);
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();
		final Document document = builder.parse(new InputSource(reader));
		final Element schemaElement = document.getDocumentElement();
		final String tableName = schemaElement.getElementsByTagName("xs:element")
			.item(0)
			.getAttributes()
			.getNamedItem("name")
			.getNodeValue();

		final TableDescriptor descriptor = new TableDescriptor(tableName);
		final NodeList attributes = schemaElement.getElementsByTagName("xs:attribute");
		final List<ColumnDescriptor> columnNames = new ArrayList<>();
		for (int i = 0; i < attributes.getLength(); i++)
		{
			final Node node = attributes.item(i);
			columnNames.add(getColumnDescriptor(node));
		}

		columnNames.sort(Comparator.comparing(ColumnDescriptor::getName));
		descriptor.addAll(columnNames);
		return descriptor;
	}

	private static ColumnDescriptor getColumnDescriptor(Node node)
	{
		final String name = node.getAttributes().getNamedItem("name").getNodeValue();
		try
		{
			if (name.toLowerCase().contains("uid"))
			{
				//System.out.println("As UUID: " + name);
				return new UuidDescriptor(name);
			}

			final Node type = node.getAttributes().getNamedItem("type");
			if (type != null)
			{
				final String typeValue = type.getNodeValue();
				if ("xs:date".equals(typeValue))
				{
					return new TimestampDescriptor(name);
				}
				else
				{
					throw new IllegalStateException("Undefined type:" + typeValue);
				}
			}

			final NodeList childNodes = node.getChildNodes();
			for (int j = 0; j < childNodes.getLength(); j++)
			{
				final Node simpleTypeChild = childNodes.item(j);
				if (simpleTypeChild.getNodeName().equals("xs:simpleType"))
				{
					return new VarcharDescriptor(name, "varchar", getSize(simpleTypeChild));
				}
			}

			throw new IllegalStateException();
		}
		catch (Exception e)
		{
			throw new IllegalStateException(String.format("column: '%s'", name), e);
		}
	}

	private static int getSize(Node node)
	{
		final NodeList simpleTypeChildList = node.getChildNodes();
		for (int i = 0; i < simpleTypeChildList.getLength(); i++)
		{
			final Node restrictionNode = simpleTypeChildList.item(i);
			if ("xs:restriction".equals(restrictionNode.getNodeName()))
			{
				final NodeList childNodes = restrictionNode.getChildNodes();
				if (childNodes.getLength() == 0)
				{
					return 10;
				}

				for (int j = 0; j < childNodes.getLength(); j++)
				{
					final Node child = childNodes.item(j);
					if ("xs:length".equals(child.getNodeName()))
					{
						return Integer.parseInt(child.getAttributes().getNamedItem("value").getNodeValue());
					}

					if ("xs:totalDigits".equals(child.getNodeName()))
					{
						return Integer.parseInt(child.getAttributes().getNamedItem("value").getNodeValue());
					}

					if ("xs:maxLength".equals(child.getNodeName()))
					{
						return Integer.parseInt(child.getAttributes().getNamedItem("value").getNodeValue());
					}

					if ("xs:minLength".equals(child.getNodeName()))
					{
						return 255;
					}

					if ("xs:enumeration".equals(child.getNodeName()))
					{
						int maxEnumSize = 0;
						for (int k = 0; k < childNodes.getLength(); k++)
						{
							final Node enumItem = childNodes.item(k);
							if ("xs:enumeration".equals(enumItem.getNodeName()))
							{
								final int enumSize = enumItem
									.getAttributes()
									.getNamedItem("value")
									.getNodeValue()
									.length();

								maxEnumSize = Math.max(maxEnumSize, enumSize);
							}
						}

						return maxEnumSize;
					}
				}

				throw new IllegalStateException();
			}
		}

		return 255;
	}
}

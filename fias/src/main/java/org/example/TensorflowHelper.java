package org.example;

import org.tensorflow.DataType;
import org.tensorflow.Graph;
import org.tensorflow.Operation;
import org.tensorflow.Tensor;

public class TensorflowHelper {

	public static double f() {
		// z = 3*x + 2*y
		final Graph graph = new Graph();
		final Operation a = graph.opBuilder("Const", "a")
				.setAttr("dtype", DataType.fromClass(Double.class))
				.setAttr("value", Tensor.<Double>create(3.0, Double.class))
				.build();

		final Operation b = graph.opBuilder("Const", "b")
				.setAttr("dtype", DataType.fromClass(Double.class))
				.setAttr("value", Tensor.<Double>create(2.0, Double.class))
				.build();

		final Operation x = graph.opBuilder("Placeholder", "x")
				.setAttr("dtype", DataType.fromClass(Double.class))
				.build();

		final Operation y = graph.opBuilder("Placeholder", "y")
				.setAttr("dtype", DataType.fromClass(Double.class))
				.build();

		final Operation ax = graph.opBuilder("Mul", "ax")
				.addInput(a.output(0))
				.addInput(x.output(0))
				.build();

		final Operation by = graph.opBuilder("Mul", "by")
				.addInput(b.output(0))
				.addInput(y.output(0))
				.build();

		final Operation z = graph.opBuilder("Add", "z")
				.addInput(ax.output(0))
				.addInput(by.output(0))
				.build();

		final org.tensorflow.Session session = new  org.tensorflow.Session(graph);
		final Tensor<Double> tensor = session.runner()
				.fetch("z")
				.feed("x", Tensor.create(33.0, Double.class))
				.feed("y", Tensor.create(63.0, Double.class))
				.run()
				.get(0)
				.expect(Double.class);

		return tensor.doubleValue();
	}
}

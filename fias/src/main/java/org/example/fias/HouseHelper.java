package org.example.fias;

import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class HouseHelper
{
	private static final Unmarshaller unmarshaller = getUnmarshaller();
	private static final String DELIMITER = ",";
	private static final String ON_CONFLICT_DO_NOTHING = ") ON CONFLICT DO NOTHING;";

	private HouseHelper() {}

	private static Unmarshaller getUnmarshaller()
	{
		final JAXBContext context;
		try
		{
			context = JAXBContext.newInstance(House.class);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			return context.createUnmarshaller();
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static House unmarshal(String string) throws JAXBException
	{
		if (unmarshaller == null)
		{
			throw new IllegalStateException("unmarshaller is null");
		}
		return (House) unmarshaller.unmarshal(new InputSource(new StringReader(string)));
	}

	public static void generateInsertsFromXMLAddressLines(String tableName, String sourceFileName) throws Exception
	{
		System.out.println("start.");
		final String targetFileName = sourceFileName.concat(".sql");
		final File sourceFile = new File(sourceFileName);
		final FileInputStream inputStream = new FileInputStream(sourceFile);
		final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		final File file = new File(targetFileName);
		final FileWriter fileWriter = new FileWriter(file);

		int id = 0;
		String line;
		House house;
		while ((line = bufferedReader.readLine()) != null)
		{

			try
			{
				house = unmarshal(line);
				house.setId(id);
				final String insertStatement = toInsertStatement(tableName, house);
				fileWriter.write(insertStatement);
				fileWriter.write("\n");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			id++;
		}

		fileWriter.close();
		System.out.println("end.");
	}

	private static String toInsertStatement(String tableName, House house)
	{
		return new StringBuilder().append(String.format("INSERT INTO %s(id, AOGUID, BUILDNUM, CADNUM, COUNTER, DIVTYPE, ENDDATE, ESTSTATUS, HOUSEGUID, HOUSEID, HOUSENUM, IFNSFL, IFNSUL, NORMDOC, OKATO, OKTMO, POSTALCODE, REGIONCODE, STARTDATE, STATSTATUS, STRSTATUS, STRUCNUM, TERRIFNSFL, TERRIFNSUL, UPDATEDATE) VALUES (", tableName))
			.append(house.getId()).append(DELIMITER)
			.append(f(house.getAOGUID())).append(DELIMITER)
			.append(f(house.getBUILDNUM())).append(DELIMITER)
			.append(f(house.getCADNUM())).append(DELIMITER)
			.append(f(house.getCOUNTER())).append(DELIMITER)
			.append(f(house.getDIVTYPE())).append(DELIMITER)
			.append(toTimestamp(house.getENDDATE())).append(DELIMITER)
			.append(f(house.getESTSTATUS())).append(DELIMITER)
			.append(f(house.getHOUSEGUID())).append(DELIMITER)
			.append(f(house.getHOUSEID())).append(DELIMITER)
			.append(f(house.getHOUSENUM())).append(DELIMITER)
			.append(f(house.getIFNSFL())).append(DELIMITER)
			.append(f(house.getIFNSUL())).append(DELIMITER)
			.append(f(house.getNORMDOC())).append(DELIMITER)
			.append(f(house.getOKATO())).append(DELIMITER)
			.append(f(house.getOKTMO())).append(DELIMITER)
			.append(f(house.getPOSTALCODE())).append(DELIMITER)
			.append(f(house.getREGIONCODE())).append(DELIMITER)
			.append(toTimestamp(house.getSTARTDATE())).append(DELIMITER)
			.append(f(house.getSTATSTATUS())).append(DELIMITER)
			.append(f(house.getSTRSTATUS())).append(DELIMITER)
			.append(f(house.getSTRUCNUM())).append(DELIMITER)
			.append(f(house.getTERRIFNSFL())).append(DELIMITER)
			.append(f(house.getTERRIFNSUL())).append(DELIMITER)
			.append(toTimestamp(house.getUPDATEDATE()))
			.append(ON_CONFLICT_DO_NOTHING)
			.toString();
	}

	private static String toTimestamp(String string)
	{
		if (string == null)
		{
			return null;
		}

		return String.format("to_timestamp('%s', 'YYYY-MM-DD')", string);
	}

	private static String f(String string)
	{
		if (string == null)
		{
			return null;
		}

		if ("null".equalsIgnoreCase(string))
		{
			return null;
		}

		return "'" + string.replaceAll("'", "''") + "'";
	}
}


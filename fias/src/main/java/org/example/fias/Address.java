package org.example.fias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Object")
@Entity
@Table(name = "AddressObjects")
public class Address
{
	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "ACTSTATUS")
	protected String ACTSTATUS;

	@Column(name = "AOGUID")
	protected String AOGUID;

	@Column(name = "AOID")
	protected String AOID;

	@Column(name = "AOLEVEL")
	protected String AOLEVEL;

	@Column(name = "AREACODE")
	protected String AREACODE;

	@Column(name = "AUTOCODE")
	protected String AUTOCODE;

	@Column(name = "CENTSTATUS")
	protected String CENTSTATUS;

	@Column(name = "CITYCODE")
	protected String CITYCODE;

	@Column(name = "CODE")
	protected String CODE;

	@Column(name = "CTARCODE")
	protected String CTARCODE;

	@Column(name = "CURRSTATUS")
	protected String CURRSTATUS;

	@Column(name = "DIVTYPE")
	protected String DIVTYPE;

	@Column(name = "ENDDATE")
	protected String ENDDATE;

	@Column(name = "EXTRCODE")
	protected String EXTRCODE;

	@Column(name = "FORMALNAME")
	protected String FORMALNAME;

	@Column(name = "IFNSFL")
	protected String IFNSFL;

	@Column(name = "IFNSUL")
	protected String IFNSUL;

	@Column(name = "LIVESTATUS")
	protected String LIVESTATUS;

	@Column(name = "NEXTID")
	protected String NEXTID;

	@Column(name = "NORMDOC")
	protected String NORMDOC;

	@Column(name = "OFFNAME")
	protected String OFFNAME;

	@Column(name = "OKATO")
	protected String OKATO;

	@Column(name = "OKTMO")
	protected String OKTMO;

	@Column(name = "OPERSTATUS")
	protected String OPERSTATUS;

	@Column(name = "PARENTGUID")
	protected String PARENTGUID;

	@Column(name = "PLACECODE")
	protected String PLACECODE;

	@Column(name = "PLAINCODE")
	protected String PLAINCODE;

	@Column(name = "PLANCODE")
	protected String PLANCODE;

	@Column(name = "POSTALCODE")
	protected String POSTALCODE;

	@Column(name = "PREVID")
	protected String PREVID;

	@Column(name = "REGIONCODE")
	protected String REGIONCODE;

	@Column(name = "SEXTCODE")
	protected String SEXTCODE;

	@Column(name = "SHORTNAME")
	protected String SHORTNAME;

	@Column(name = "STARTDATE")
	protected String STARTDATE;

	@Column(name = "STREETCODE")
	protected String STREETCODE;

	@Column(name = "TERRIFNSFL")
	protected String TERRIFNSFL;

	@Column(name = "TERRIFNSUL")
	protected String TERRIFNSUL;

	@Column(name = "UPDATEDATE")
	protected String UPDATEDATE;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@XmlAttribute(name = "ACTSTATUS")
	public String getACTSTATUS()
	{
		return ACTSTATUS;
	}

	public void setACTSTATUS(String ACTSTATUS)
	{
		this.ACTSTATUS = ACTSTATUS;
	}

	@XmlAttribute(name = "AOGUID")
	public String getAOGUID()
	{
		return AOGUID;
	}

	public void setAOGUID(String AOGUID)
	{
		this.AOGUID = AOGUID;
	}

	@XmlAttribute(name = "AOID")
	public String getAOID()
	{
		return AOID;
	}

	public void setAOID(String AOID)
	{
		this.AOID = AOID;
	}

	@XmlAttribute(name = "AOLEVEL")
	public String getAOLEVEL()
	{
		return AOLEVEL;
	}

	public void setAOLEVEL(String AOLEVEL)
	{
		this.AOLEVEL = AOLEVEL;
	}

	@XmlAttribute(name = "AREACODE")
	public String getAREACODE()
	{
		return AREACODE;
	}

	public void setAREACODE(String AREACODE)
	{
		this.AREACODE = AREACODE;
	}

	@XmlAttribute(name = "AUTOCODE")
	public String getAUTOCODE()
	{
		return AUTOCODE;
	}

	public void setAUTOCODE(String AUTOCODE)
	{
		this.AUTOCODE = AUTOCODE;
	}

	@XmlAttribute(name = "CENTSTATUS")
	public String getCENTSTATUS()
	{
		return CENTSTATUS;
	}

	public void setCENTSTATUS(String CENTSTATUS)
	{
		this.CENTSTATUS = CENTSTATUS;
	}

	@XmlAttribute(name = "CITYCODE")
	public String getCITYCODE()
	{
		return CITYCODE;
	}

	public void setCITYCODE(String CITYCODE)
	{
		this.CITYCODE = CITYCODE;
	}

	@XmlAttribute(name = "CODE")
	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String CODE)
	{
		this.CODE = CODE;
	}

	@XmlAttribute(name = "CTARCODE")
	public String getCTARCODE()
	{
		return CTARCODE;
	}

	public void setCTARCODE(String CTARCODE)
	{
		this.CTARCODE = CTARCODE;
	}

	@XmlAttribute(name = "CURRSTATUS")
	public String getCURRSTATUS()
	{
		return CURRSTATUS;
	}

	public void setCURRSTATUS(String CURRSTATUS)
	{
		this.CURRSTATUS = CURRSTATUS;
	}

	@XmlAttribute(name = "DIVTYPE")
	public String getDIVTYPE()
	{
		return DIVTYPE;
	}

	public void setDIVTYPE(String DIVTYPE)
	{
		this.DIVTYPE = DIVTYPE;
	}

	@XmlAttribute(name = "ENDDATE")
	public String getENDDATE()
	{
		return ENDDATE;
	}

	public void setENDDATE(String ENDDATE)
	{
		this.ENDDATE = ENDDATE;
	}

	@XmlAttribute(name = "EXTRCODE")
	public String getEXTRCODE()
	{
		return EXTRCODE;
	}

	public void setEXTRCODE(String EXTRCODE)
	{
		this.EXTRCODE = EXTRCODE;
	}

	@XmlAttribute(name = "FORMALNAME")
	public String getFORMALNAME()
	{
		return FORMALNAME;
	}

	public void setFORMALNAME(String FORMALNAME)
	{
		this.FORMALNAME = FORMALNAME;
	}

	@XmlAttribute(name = "IFNSFL")
	public String getIFNSFL()
	{
		return IFNSFL;
	}

	public void setIFNSFL(String IFNSFL)
	{
		this.IFNSFL = IFNSFL;
	}

	@XmlAttribute(name = "IFNSUL")
	public String getIFNSUL()
	{
		return IFNSUL;
	}

	public void setIFNSUL(String IFNSUL)
	{
		this.IFNSUL = IFNSUL;
	}

	@XmlAttribute(name = "LIVESTATUS")
	public String getLIVESTATUS()
	{
		return LIVESTATUS;
	}

	public void setLIVESTATUS(String LIVESTATUS)
	{
		this.LIVESTATUS = LIVESTATUS;
	}

	@XmlAttribute(name = "NEXTID")
	public String getNEXTID()
	{
		return NEXTID;
	}

	public void setNEXTID(String NEXTID)
	{
		this.NEXTID = NEXTID;
	}

	@XmlAttribute(name = "NORMDOC")
	public String getNORMDOC()
	{
		return NORMDOC;
	}

	public void setNORMDOC(String NORMDOC)
	{
		this.NORMDOC = NORMDOC;
	}

	@XmlAttribute(name = "OFFNAME")
	public String getOFFNAME()
	{
		return OFFNAME;
	}

	public void setOFFNAME(String OFFNAME)
	{
		this.OFFNAME = OFFNAME;
	}

	@XmlAttribute(name = "OKATO")
	public String getOKATO()
	{
		return OKATO;
	}

	public void setOKATO(String OKATO)
	{
		this.OKATO = OKATO;
	}

	@XmlAttribute(name = "OKTMO")
	public String getOKTMO()
	{
		return OKTMO;
	}

	public void setOKTMO(String OKTMO)
	{
		this.OKTMO = OKTMO;
	}

	@XmlAttribute(name = "OPERSTATUS")
	public String getOPERSTATUS()
	{
		return OPERSTATUS;
	}

	public void setOPERSTATUS(String OPERSTATUS)
	{
		this.OPERSTATUS = OPERSTATUS;
	}

	@XmlAttribute(name = "PARENTGUID")
	public String getPARENTGUID()
	{
		return PARENTGUID;
	}

	public void setPARENTGUID(String PARENTGUID)
	{
		this.PARENTGUID = PARENTGUID;
	}

	@XmlAttribute(name = "PLACECODE")
	public String getPLACECODE()
	{
		return PLACECODE;
	}

	public void setPLACECODE(String PLACECODE)
	{
		this.PLACECODE = PLACECODE;
	}

	@XmlAttribute(name = "PLAINCODE")
	public String getPLAINCODE()
	{
		return PLAINCODE;
	}

	public void setPLAINCODE(String PLAINCODE)
	{
		this.PLAINCODE = PLAINCODE;
	}

	@XmlAttribute(name = "PLANCODE")
	public String getPLANCODE()
	{
		return PLANCODE;
	}

	public void setPLANCODE(String PLANCODE)
	{
		this.PLANCODE = PLANCODE;
	}

	@XmlAttribute(name = "POSTALCODE")
	public String getPOSTALCODE()
	{
		return POSTALCODE;
	}

	public void setPOSTALCODE(String POSTALCODE)
	{
		this.POSTALCODE = POSTALCODE;
	}

	@XmlAttribute(name = "PREVID")
	public String getPREVID()
	{
		return PREVID;
	}

	public void setPREVID(String PREVID)
	{
		this.PREVID = PREVID;
	}

	@XmlAttribute(name = "REGIONCODE")
	public String getREGIONCODE()
	{
		return REGIONCODE;
	}

	public void setREGIONCODE(String REGIONCODE)
	{
		this.REGIONCODE = REGIONCODE;
	}

	@XmlAttribute(name = "SEXTCODE")
	public String getSEXTCODE()
	{
		return SEXTCODE;
	}

	public void setSEXTCODE(String SEXTCODE)
	{
		this.SEXTCODE = SEXTCODE;
	}

	@XmlAttribute(name = "SHORTNAME")
	public String getSHORTNAME()
	{
		return SHORTNAME;
	}

	public void setSHORTNAME(String SHORTNAME)
	{
		this.SHORTNAME = SHORTNAME;
	}

	@XmlAttribute(name = "STARTDATE")
	public String getSTARTDATE()
	{
		return STARTDATE;
	}

	public void setSTARTDATE(String STARTDATE)
	{
		this.STARTDATE = STARTDATE;
	}

	@XmlAttribute(name = "STREETCODE")
	public String getSTREETCODE()
	{
		return STREETCODE;
	}

	public void setSTREETCODE(String STREETCODE)
	{
		this.STREETCODE = STREETCODE;
	}

	@XmlAttribute(name = "TERRIFNSFL")
	public String getTERRIFNSFL()
	{
		return TERRIFNSFL;
	}

	public void setTERRIFNSFL(String TERRIFNSFL)
	{
		this.TERRIFNSFL = TERRIFNSFL;
	}

	@XmlAttribute(name = "TERRIFNSUL")
	public String getTERRIFNSUL()
	{
		return TERRIFNSUL;
	}

	public void setTERRIFNSUL(String TERRIFNSUL)
	{
		this.TERRIFNSUL = TERRIFNSUL;
	}

	@XmlAttribute(name = "UPDATEDATE")
	public String getUPDATEDATE()
	{
		return UPDATEDATE;
	}

	public void setUPDATEDATE(String UPDATEDATE)
	{
		this.UPDATEDATE = UPDATEDATE;
	}
}

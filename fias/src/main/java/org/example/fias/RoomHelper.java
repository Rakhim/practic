package org.example.fias;

import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class RoomHelper
{
	private static final Unmarshaller unmarshaller = getUnmarshaller();
	private static final String DELIMITER = ",";
	private static final String ON_CONFLICT_DO_NOTHING = ") ON CONFLICT DO NOTHING;";

	private RoomHelper() {}

	private static Unmarshaller getUnmarshaller()
	{
		final JAXBContext context;
		try
		{
			context = JAXBContext.newInstance(Room.class);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			return context.createUnmarshaller();
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Room unmarshal(String string)
	{
		if (unmarshaller == null)
		{
			throw new IllegalStateException("unmarshaller is null");
		}

		try
		{
			return (Room) unmarshaller.unmarshal(new InputSource(new StringReader(string)));
		}
		catch (JAXBException e)
		{
			System.out.println("failed for: ");
			System.out.println(string);
			System.out.println();

			throw new IllegalArgumentException(e);
		}
	}

	public static void generateInsertsFromXMLAddressLines(String tableName, String sourceFileName) throws Exception
	{
		System.out.println("start.");
		final String targetFileName = sourceFileName.concat(".sql");
		final File sourceFile = new File(sourceFileName);
		final FileInputStream inputStream = new FileInputStream(sourceFile);
		final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		final File file = new File(targetFileName);
		final FileWriter fileWriter = new FileWriter(file);

		int id = 0;
		String line;
		Room room;
		while ((line = bufferedReader.readLine()) != null)
		{

			try
			{
				room = unmarshal(line);
				room.setId(id);
				final String insertStatement = toInsertStatement(tableName, room);
				fileWriter.write(insertStatement);
				fileWriter.write("\n");
			}
			catch (Exception e)
			{
				System.out.printf("failed(%s) for: %s%n%n%n", e.getMessage(), line);
			}

			id++;
		}

		fileWriter.close();
		System.out.println("end.");
	}

	private static String toInsertStatement(String tableName, Room room)
	{
		return new StringBuilder().append(String.format("INSERT INTO %s(id, CADNUM , ENDDATE , FLATNUMBER , FLATTYPE , HOUSEGUID , LIVESTATUS , NEXTID , NORMDOC , OPERSTATUS , POSTALCODE , PREVID , REGIONCODE , ROOMCADNUM , ROOMGUID , ROOMID , ROOMNUMBER , ROOMTYPE , STARTDATE , UPDATEDATE) VALUES (", tableName))
			.append(room.getId()).append(DELIMITER)
			.append(f(room.getCADNUM())).append(DELIMITER)
			.append(toTimestamp(room.getENDDATE())).append(DELIMITER)
			.append(f(room.getFLATNUMBER())).append(DELIMITER)
			.append(f(room.getFLATTYPE())).append(DELIMITER)
			.append(f(room.getHOUSEGUID())).append(DELIMITER)
			.append(f(room.getLIVESTATUS())).append(DELIMITER)
			.append(f(room.getNEXTID())).append(DELIMITER)
			.append(f(room.getNORMDOC())).append(DELIMITER)
			.append(f(room.getOPERSTATUS())).append(DELIMITER)
			.append(f(room.getPOSTALCODE())).append(DELIMITER)
			.append(f(room.getPREVID())).append(DELIMITER)
			.append(f(room.getREGIONCODE())).append(DELIMITER)
			.append(f(room.getROOMCADNUM())).append(DELIMITER)
			.append(f(room.getROOMGUID())).append(DELIMITER)
			.append(f(room.getROOMID())).append(DELIMITER)
			.append(f(room.getROOMNUMBER())).append(DELIMITER)
			.append(f(room.getROOMTYPE())).append(DELIMITER)
			.append(toTimestamp(room.getSTARTDATE())).append(DELIMITER)
			.append(toTimestamp(room.getUPDATEDATE()))
			.append(ON_CONFLICT_DO_NOTHING)
			.toString();
	}

	private static String toTimestamp(String string)
	{
		if (string == null)
		{
			return null;
		}

		return String.format("to_timestamp('%s', 'YYYY-MM-DD')", string);
	}

	private static String f(String string)
	{
		if (string == null)
		{
			return null;
		}

		if ("null".equalsIgnoreCase(string))
		{
			return null;
		}

		return "'" + string.replaceAll("'", "''") + "'";
	}
}


package org.example.fias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Room")
@Entity
@Table(name = "Room")
public class Room
{
	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "CADNUM")
	protected String CADNUM;

	@Column(name = "ENDDATE")
	protected String ENDDATE;

	@Column(name = "FLATNUMBER")
	protected String FLATNUMBER;

	@Column(name = "FLATTYPE")
	protected String FLATTYPE;

	@Column(name = "HOUSEGUID")
	protected String HOUSEGUID;

	@Column(name = "LIVESTATUS")
	protected String LIVESTATUS;

	@Column(name = "NEXTID")
	protected String NEXTID;

	@Column(name = "NORMDOC")
	protected String NORMDOC;

	@Column(name = "OPERSTATUS")
	protected String OPERSTATUS;

	@Column(name = "POSTALCODE")
	protected String POSTALCODE;

	@Column(name = "PREVID")
	protected String PREVID;

	@Column(name = "REGIONCODE")
	protected String REGIONCODE;

	@Column(name = "ROOMCADNUM")
	protected String ROOMCADNUM;

	@Column(name = "ROOMGUID")
	protected String ROOMGUID;

	@Column(name = "ROOMID")
	protected String ROOMID;

	@Column(name = "ROOMNUMBER")
	protected String ROOMNUMBER;

	@Column(name = "ROOMTYPE")
	protected String ROOMTYPE;

	@Column(name = "STARTDATE")
	protected String STARTDATE;

	@Column(name = "UPDATEDATE")
	protected String UPDATEDATE;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@XmlAttribute(name = "CADNUM")
	public String getCADNUM()
	{
		return CADNUM;
	}

	public void setCADNUM(String CADNUM)
	{
		this.CADNUM = CADNUM;
	}

	@XmlAttribute(name = "ENDDATE")
	public String getENDDATE()
	{
		return ENDDATE;
	}

	public void setENDDATE(String ENDDATE)
	{
		this.ENDDATE = ENDDATE;
	}

	@XmlAttribute(name = "FLATNUMBER")
	public String getFLATNUMBER()
	{
		return FLATNUMBER;
	}

	public void setFLATNUMBER(String FLATNUMBER)
	{
		this.FLATNUMBER = FLATNUMBER;
	}

	@XmlAttribute(name = "FLATTYPE")
	public String getFLATTYPE()
	{
		return FLATTYPE;
	}

	public void setFLATTYPE(String FLATTYPE)
	{
		this.FLATTYPE = FLATTYPE;
	}

	@XmlAttribute(name = "HOUSEGUID")
	public String getHOUSEGUID()
	{
		return HOUSEGUID;
	}

	public void setHOUSEGUID(String HOUSEGUID)
	{
		this.HOUSEGUID = HOUSEGUID;
	}

	@XmlAttribute(name = "LIVESTATUS")
	public String getLIVESTATUS()
	{
		return LIVESTATUS;
	}

	public void setLIVESTATUS(String LIVESTATUS)
	{
		this.LIVESTATUS = LIVESTATUS;
	}

	@XmlAttribute(name = "NEXTID")
	public String getNEXTID()
	{
		return NEXTID;
	}

	public void setNEXTID(String NEXTID)
	{
		this.NEXTID = NEXTID;
	}

	@XmlAttribute(name = "NORMDOC")
	public String getNORMDOC()
	{
		return NORMDOC;
	}

	public void setNORMDOC(String NORMDOC)
	{
		this.NORMDOC = NORMDOC;
	}

	@XmlAttribute(name = "OPERSTATUS")
	public String getOPERSTATUS()
	{
		return OPERSTATUS;
	}

	public void setOPERSTATUS(String OPERSTATUS)
	{
		this.OPERSTATUS = OPERSTATUS;
	}

	@XmlAttribute(name = "POSTALCODE")
	public String getPOSTALCODE()
	{
		return POSTALCODE;
	}

	public void setPOSTALCODE(String POSTALCODE)
	{
		this.POSTALCODE = POSTALCODE;
	}

	@XmlAttribute(name = "PREVID")
	public String getPREVID()
	{
		return PREVID;
	}

	public void setPREVID(String PREVID)
	{
		this.PREVID = PREVID;
	}

	@XmlAttribute(name = "REGIONCODE")
	public String getREGIONCODE()
	{
		return REGIONCODE;
	}

	public void setREGIONCODE(String REGIONCODE)
	{
		this.REGIONCODE = REGIONCODE;
	}

	@XmlAttribute(name = "ROOMCADNUM")
	public String getROOMCADNUM()
	{
		return ROOMCADNUM;
	}

	public void setROOMCADNUM(String ROOMCADNUM)
	{
		this.ROOMCADNUM = ROOMCADNUM;
	}

	@XmlAttribute(name = "ROOMGUID")
	public String getROOMGUID()
	{
		return ROOMGUID;
	}

	public void setROOMGUID(String ROOMGUID)
	{
		this.ROOMGUID = ROOMGUID;
	}

	@XmlAttribute(name = "ROOMID")
	public String getROOMID()
	{
		return ROOMID;
	}

	public void setROOMID(String ROOMID)
	{
		this.ROOMID = ROOMID;
	}

	@XmlAttribute(name = "ROOMNUMBER")
	public String getROOMNUMBER()
	{
		return ROOMNUMBER;
	}

	public void setROOMNUMBER(String ROOMNUMBER)
	{
		this.ROOMNUMBER = ROOMNUMBER;
	}

	@XmlAttribute(name = "ROOMTYPE")
	public String getROOMTYPE()
	{
		return ROOMTYPE;
	}

	public void setROOMTYPE(String ROOMTYPE)
	{
		this.ROOMTYPE = ROOMTYPE;
	}

	@XmlAttribute(name = "STARTDATE")
	public String getSTARTDATE()
	{
		return STARTDATE;
	}

	public void setSTARTDATE(String STARTDATE)
	{
		this.STARTDATE = STARTDATE;
	}

	@XmlAttribute(name = "UPDATEDATE")
	public String getUPDATEDATE()
	{
		return UPDATEDATE;
	}

	public void setUPDATEDATE(String UPDATEDATE)
	{
		this.UPDATEDATE = UPDATEDATE;
	}
}

package org.example.fias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "House")
@Entity
@Table(name = "House")
public class House
{
	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "AOGUID")
	protected String AOGUID;

	@Column(name = "BUILDNUM")
	protected String BUILDNUM;

	@Column(name = "CADNUM")
	protected String CADNUM;

	@Column(name = "COUNTER")
	protected String COUNTER;

	@Column(name = "DIVTYPE")
	protected String DIVTYPE;

	@Column(name = "ENDDATE")
	protected String ENDDATE;

	@Column(name = "ESTSTATUS")
	protected String ESTSTATUS;

	@Column(name = "HOUSEGUID")
	protected String HOUSEGUID;

	@Column(name = "HOUSEID")
	protected String HOUSEID;

	@Column(name = "HOUSENUM")
	protected String HOUSENUM;

	@Column(name = "IFNSFL")
	protected String IFNSFL;

	@Column(name = "IFNSUL")
	protected String IFNSUL;

	@Column(name = "NORMDOC")
	protected String NORMDOC;

	@Column(name = "OKATO")
	protected String OKATO;

	@Column(name = "OKTMO")
	protected String OKTMO;

	@Column(name = "POSTALCODE")
	protected String POSTALCODE;

	@Column(name = "REGIONCODE")
	protected String REGIONCODE;

	@Column(name = "STARTDATE")
	protected String STARTDATE;

	@Column(name = "STATSTATUS")
	protected String STATSTATUS;

	@Column(name = "STRSTATUS")
	protected String STRSTATUS;

	@Column(name = "STRUCNUM")
	protected String STRUCNUM;

	@Column(name = "TERRIFNSFL")
	protected String TERRIFNSFL;

	@Column(name = "TERRIFNSUL")
	protected String TERRIFNSUL;

	@Column(name = "UPDATEDATE")
	protected String UPDATEDATE;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	@XmlAttribute(name = "AOGUID")
	public String getAOGUID()
	{
		return AOGUID;
	}

	public void setAOGUID(String AOGUID)
	{
		this.AOGUID = AOGUID;
	}

	@XmlAttribute(name = "BUILDNUM")
	public String getBUILDNUM()
	{
		return BUILDNUM;
	}

	public void setBUILDNUM(String BUILDNUM)
	{
		this.BUILDNUM = BUILDNUM;
	}

	@XmlAttribute(name = "CADNUM")
	public String getCADNUM()
	{
		return CADNUM;
	}

	public void setCADNUM(String CADNUM)
	{
		this.CADNUM = CADNUM;
	}

	@XmlAttribute(name = "COUNTER")
	public String getCOUNTER()
	{
		return COUNTER;
	}

	public void setCOUNTER(String COUNTER)
	{
		this.COUNTER = COUNTER;
	}

	@XmlAttribute(name = "DIVTYPE")
	public String getDIVTYPE()
	{
		return DIVTYPE;
	}

	public void setDIVTYPE(String DIVTYPE)
	{
		this.DIVTYPE = DIVTYPE;
	}

	@XmlAttribute(name = "ENDDATE")
	public String getENDDATE()
	{
		return ENDDATE;
	}

	public void setENDDATE(String ENDDATE)
	{
		this.ENDDATE = ENDDATE;
	}

	@XmlAttribute(name = "ESTSTATUS")
	public String getESTSTATUS()
	{
		return ESTSTATUS;
	}

	public void setESTSTATUS(String ESTSTATUS)
	{
		this.ESTSTATUS = ESTSTATUS;
	}

	@XmlAttribute(name = "HOUSEGUID")
	public String getHOUSEGUID()
	{
		return HOUSEGUID;
	}

	public void setHOUSEGUID(String HOUSEGUID)
	{
		this.HOUSEGUID = HOUSEGUID;
	}

	@XmlAttribute(name = "HOUSEID")
	public String getHOUSEID()
	{
		return HOUSEID;
	}

	public void setHOUSEID(String HOUSEID)
	{
		this.HOUSEID = HOUSEID;
	}

	@XmlAttribute(name = "HOUSENUM")
	public String getHOUSENUM()
	{
		return HOUSENUM;
	}

	public void setHOUSENUM(String HOUSENUM)
	{
		this.HOUSENUM = HOUSENUM;
	}

	@XmlAttribute(name = "IFNSFL")
	public String getIFNSFL()
	{
		return IFNSFL;
	}

	public void setIFNSFL(String IFNSFL)
	{
		this.IFNSFL = IFNSFL;
	}

	@XmlAttribute(name = "IFNSUL")
	public String getIFNSUL()
	{
		return IFNSUL;
	}

	public void setIFNSUL(String IFNSUL)
	{
		this.IFNSUL = IFNSUL;
	}

	@XmlAttribute(name = "NORMDOC")
	public String getNORMDOC()
	{
		return NORMDOC;
	}

	public void setNORMDOC(String NORMDOC)
	{
		this.NORMDOC = NORMDOC;
	}

	@XmlAttribute(name = "OKATO")
	public String getOKATO()
	{
		return OKATO;
	}

	public void setOKATO(String OKATO)
	{
		this.OKATO = OKATO;
	}

	@XmlAttribute(name = "OKTMO")
	public String getOKTMO()
	{
		return OKTMO;
	}

	public void setOKTMO(String OKTMO)
	{
		this.OKTMO = OKTMO;
	}

	@XmlAttribute(name = "POSTALCODE")
	public String getPOSTALCODE()
	{
		return POSTALCODE;
	}

	public void setPOSTALCODE(String POSTALCODE)
	{
		this.POSTALCODE = POSTALCODE;
	}

	@XmlAttribute(name = "REGIONCODE")
	public String getREGIONCODE()
	{
		return REGIONCODE;
	}

	public void setREGIONCODE(String REGIONCODE)
	{
		this.REGIONCODE = REGIONCODE;
	}

	@XmlAttribute(name = "STARTDATE")
	public String getSTARTDATE()
	{
		return STARTDATE;
	}

	public void setSTARTDATE(String STARTDATE)
	{
		this.STARTDATE = STARTDATE;
	}

	@XmlAttribute(name = "STATSTATUS")
	public String getSTATSTATUS()
	{
		return STATSTATUS;
	}

	public void setSTATSTATUS(String STATSTATUS)
	{
		this.STATSTATUS = STATSTATUS;
	}

	@XmlAttribute(name = "STRSTATUS")
	public String getSTRSTATUS()
	{
		return STRSTATUS;
	}

	public void setSTRSTATUS(String STRSTATUS)
	{
		this.STRSTATUS = STRSTATUS;
	}

	@XmlAttribute(name = "STRUCNUM")
	public String getSTRUCNUM()
	{
		return STRUCNUM;
	}

	public void setSTRUCNUM(String STRUCNUM)
	{
		this.STRUCNUM = STRUCNUM;
	}

	@XmlAttribute(name = "TERRIFNSFL")
	public String getTERRIFNSFL()
	{
		return TERRIFNSFL;
	}

	public void setTERRIFNSFL(String TERRIFNSFL)
	{
		this.TERRIFNSFL = TERRIFNSFL;
	}

	@XmlAttribute(name = "TERRIFNSUL")
	public String getTERRIFNSUL()
	{
		return TERRIFNSUL;
	}

	public void setTERRIFNSUL(String TERRIFNSUL)
	{
		this.TERRIFNSUL = TERRIFNSUL;
	}

	@XmlAttribute(name = "UPDATEDATE")
	public String getUPDATEDATE()
	{
		return UPDATEDATE;
	}

	public void setUPDATEDATE(String UPDATEDATE)
	{
		this.UPDATEDATE = UPDATEDATE;
	}
}

package org.example.fias;

import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class AddressHelper
{
	private static final Unmarshaller unmarshaller = getUnmarshaller();
	private static final String DELIMITER = ",";
	private static final String ON_CONFLICT_DO_NOTHING = ") ON CONFLICT DO NOTHING;";

	private AddressHelper() {}

	private static Unmarshaller getUnmarshaller()
	{
		final JAXBContext context;
		try
		{
			context = JAXBContext.newInstance(Address.class);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			return context.createUnmarshaller();
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Address unmarshal(String string) throws JAXBException
	{
		if (unmarshaller == null)
		{
			throw new IllegalStateException("unmarshaller is null");
		}
		return (Address) unmarshaller.unmarshal(new InputSource(new StringReader(string)));
	}

	public static void generateInsertsFromXMLAddressLines(String tableName, String sourceFileName) throws Exception
	{
		System.out.println("start.");
		final String targetFileName = sourceFileName.concat(".sql");
		final File sourceFile = new File(sourceFileName);
		final FileInputStream inputStream = new FileInputStream(sourceFile);
		final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		final File file = new File(targetFileName);
		final FileWriter fileWriter = new FileWriter(file);

		int id = 0;
		String line;
		Address address;
		while ((line = bufferedReader.readLine()) != null)
		{

			try
			{
				address = unmarshal(line);
				address.setId(id);
				final String insertStatement = AddressHelper.toInsertStatement(tableName, address);
				fileWriter.write(insertStatement);
				fileWriter.write("\n");

				if (id % 500_000 == 0) {
					fileWriter.flush();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			id++;
		}

		fileWriter.close();
		System.out.println("end.");
	}

	private static String toInsertStatement(String tableName, Address address)
	{
		return new StringBuilder()
			.append(String.format("INSERT INTO %s(id, ACTSTATUS, AOGUID, AOID, AOLEVEL, AREACODE, AUTOCODE, CENTSTATUS, CITYCODE, CODE, CTARCODE, CURRSTATUS, DIVTYPE, ENDDATE, EXTRCODE, FORMALNAME, IFNSFL, IFNSUL, LIVESTATUS, NEXTID, NORMDOC, OFFNAME, OKATO, OKTMO, OPERSTATUS, PARENTGUID, PLACECODE, PLAINCODE, PLANCODE, POSTALCODE, PREVID, REGIONCODE, SEXTCODE, SHORTNAME, STARTDATE, STREETCODE, TERRIFNSFL, TERRIFNSUL, UPDATEDATE) VALUES (", tableName))
			.append(address.getId()).append(DELIMITER)
			.append(f(address.getACTSTATUS())).append(DELIMITER)
			.append(f(address.getAOGUID())).append(DELIMITER)
			.append(f(address.getAOID())).append(DELIMITER)
			.append(f(address.getAOLEVEL())).append(DELIMITER)
			.append(f(address.getAREACODE())).append(DELIMITER)
			.append(f(address.getAUTOCODE())).append(DELIMITER)
			.append(f(address.getCENTSTATUS())).append(DELIMITER)
			.append(f(address.getCITYCODE())).append(DELIMITER)
			.append(f(address.getCODE())).append(DELIMITER)
			.append(f(address.getCTARCODE())).append(DELIMITER)
			.append(f(address.getCURRSTATUS())).append(DELIMITER)
			.append(f(address.getDIVTYPE())).append(DELIMITER)
			.append(toTimestamp(address.getENDDATE())).append(DELIMITER)
			.append(f(address.getEXTRCODE())).append(DELIMITER)
			.append(f(address.getFORMALNAME())).append(DELIMITER)
			.append(f(address.getIFNSFL())).append(DELIMITER)
			.append(f(address.getIFNSUL())).append(DELIMITER)
			.append(f(address.getLIVESTATUS())).append(DELIMITER)
			.append(f(address.getNEXTID())).append(DELIMITER)
			.append(f(address.getNORMDOC())).append(DELIMITER)
			.append(f(address.getOFFNAME())).append(DELIMITER)
			.append(f(address.getOKATO())).append(DELIMITER)
			.append(f(address.getOKTMO())).append(DELIMITER)
			.append(f(address.getOPERSTATUS())).append(DELIMITER)
			.append(f(address.getPARENTGUID())).append(DELIMITER)
			.append(f(address.getPLACECODE())).append(DELIMITER)
			.append(f(address.getPLAINCODE())).append(DELIMITER)
			.append(f(address.getPLANCODE())).append(DELIMITER)
			.append(f(address.getPOSTALCODE())).append(DELIMITER)
			.append(f(address.getPREVID())).append(DELIMITER)
			.append(f(address.getREGIONCODE())).append(DELIMITER)
			.append(f(address.getSEXTCODE())).append(DELIMITER)
			.append(f(address.getSHORTNAME())).append(DELIMITER)
			.append(toTimestamp(address.getSTARTDATE())).append(DELIMITER)
			.append(f(address.getSTREETCODE())).append(DELIMITER)
			.append(f(address.getTERRIFNSFL())).append(DELIMITER)
			.append(f(address.getTERRIFNSUL())).append(DELIMITER)
			.append(toTimestamp(address.getUPDATEDATE()))
			.append(ON_CONFLICT_DO_NOTHING)
			.toString();
	}

	private static String toTimestamp(String string)
	{
		if (string == null)
		{
			return null;
		}

		return String.format("to_timestamp('%s', 'YYYY-MM-DD')", string);
	}

	private static String f(String string)
	{
		if (string == null)
		{
			return null;
		}

		if ("null".equalsIgnoreCase(string))
		{
			return null;
		}

		return "'" + string.replaceAll("'", "''") + "'";
	}
}

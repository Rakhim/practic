package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvRowBuilder
{
	private final List<String> labels = new ArrayList<>();
	private final Map<String, String> values = new HashMap<>();
	private final String delimiter = "\t";

	public CsvRowBuilder(List<String> labels)
	{
		this.labels.addAll(labels);
	}

	public void fillColumn(String label, String value)
	{
		if (labels.contains(label) && !value.isEmpty())
		{
			values.put(label, value);
		}
	}

	private String getColumnValue(String label)
	{
		final String value = values.get(label);
		if (value != null)
		{
			return value;
		}

		return "";
	}

	public void clear()
	{
		values.clear();
	}

	public boolean isEmpty()
	{
		return values.isEmpty();
	}

	@Override
	public String toString()
	{
		return labels.stream()
			.map(this::getColumnValue)
			.reduce((left, right) -> left + delimiter + right)
			.orElse("");
	}

}

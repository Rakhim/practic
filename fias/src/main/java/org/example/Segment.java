package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Segment
{
	private final List<String> shortnames = new ArrayList<>();
	private final String label;
	private final String name;

	public Segment(String label, String name)
	{
		if ("г.".equals(label))
		{
			this.label = "г";
		}
		else if ("ул.".equals(label))
		{
			this.label = "ул";
		}
		else
		{
			this.label = label;
		}

		this.name = name;
	}

	public String getLabel()
	{
		return label;
	}

	public String getName()
	{
		return name;
	}

	public String getShortname()
	{
		return shortnames.isEmpty()
			? ""
			: shortnames.get(new Random().nextInt(shortnames.size()));
	}

	public Segment addShortname(String shortname)
	{
		shortnames.add(shortname);
		return this;
	}
}

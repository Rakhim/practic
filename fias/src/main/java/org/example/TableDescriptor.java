package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TableDescriptor
{
	private static final String newline = String.format("%n");
	private static final String delimiter = "\t";
	private final String name;
	private final List<ColumnDescriptor> columnsDescriptors = new ArrayList<>();

	public TableDescriptor(String name)
	{
		this.name = name.toLowerCase();
	}

	public void add(ColumnDescriptor columnDescriptor)
	{
		columnsDescriptors.add(columnDescriptor);
	}

	public void addAll(List<ColumnDescriptor> columnDescriptors)
	{
		columnsDescriptors.addAll(columnDescriptors);
	}

	public String getCsvHeader()
	{
		return columnsDescriptors.stream()
			.map(ColumnDescriptor::getName)
			.reduce( (left, right) -> left + delimiter + right)
			.orElse("");
	}

	public CsvRowBuilder getCsvRowBuilder()
	{
		final List<String> labels = columnsDescriptors.stream()
			.map(ColumnDescriptor::getName)
			.collect(Collectors.toList());

		return new CsvRowBuilder(labels);
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder()
			.append(String.format("create table %s%n(%n", name));

		final String primaryKey = String.format("\tid integer not null constraint pk_%s unique", name.toLowerCase());
		final String columns = columnsDescriptors.stream()
			.map(Object::toString)
			.reduce(primaryKey, (left, right) -> String.format("%s,%n%s", left, right));

		builder
			.append(columns)
			.append(newline);

		return builder
			.append(");")
			.toString();
	}
}


/*
create table house
(
	id integer not null	constraint pk_house	unique,
	houseid varchar(50),
	houseguid varchar(50),
	aoguid uuid,
	housenum varchar(50),
	strstatus varchar(10),
	eststatus varchar(10),
	statstatus varchar(10),
	ifnsfl varchar(10),
	ifnsul varchar(10),
	terrifnsfl varchar(10),
	terrifnsul varchar(10),
	okato varchar(20),
	oktmo varchar(20),
	postalcode varchar(10),
	startdate timestamp,
	enddate timestamp,
	updatedate timestamp,
	counter varchar(10),
	divtype varchar(10),
	regioncode varchar(10),
	normdoc varchar(50)
);
*/
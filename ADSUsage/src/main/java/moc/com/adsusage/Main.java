package moc.com.adsusage;

import moc.com.adsusage.ui.Frame;

/**
 *
 * @author Raim
 */
public class Main {
  public static void main(String args[]) {    
    java.awt.EventQueue.invokeLater(() -> {
      new Frame().setVisible(true);
    });
  }
}

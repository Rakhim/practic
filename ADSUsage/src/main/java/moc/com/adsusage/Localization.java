/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.adsusage;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Raim
 */
public class Localization {
  private static final String DEFAULT_LANGUAGE = "ru";
  private final Vocabulary vocabulary;
  private String currentLanguage;
  
  public Localization() {
    this.vocabulary = new Vocabulary();
    this.currentLanguage = DEFAULT_LANGUAGE;
    vocabulary.addLanguage(DEFAULT_LANGUAGE);
    
    defaultLocalization();
  }

  private void defaultLocalization() {
    final String EN = "en";
    final String KZ = "kz";
    
    vocabulary.add(EN, "Язык", "Language");
    vocabulary.add(EN, "Открыть", "Open");
    vocabulary.add(EN, "Сохранить", "Save");
    vocabulary.add(EN, "Файл", "File");
    vocabulary.add(EN, "слово", "word");
    vocabulary.add(EN, "перевод", "translation");
    vocabulary.add(EN, "Добавить язык", "Add language");

    vocabulary.add(DEFAULT_LANGUAGE, "Язык", "Язык");
    vocabulary.add(DEFAULT_LANGUAGE, "Открыть", "Открыть");
    vocabulary.add(DEFAULT_LANGUAGE, "Сохранить", "Сохранить");
    vocabulary.add(DEFAULT_LANGUAGE, "Файл", "Файл");
    vocabulary.add(DEFAULT_LANGUAGE, "слово", "слово");
    vocabulary.add(DEFAULT_LANGUAGE, "перевод", "перевод");
    vocabulary.add(DEFAULT_LANGUAGE, "Добавить язык", "Добавить язык");

    vocabulary.add(KZ, "Язык",	"Тілі");
    vocabulary.add(KZ, "Добавить язык",	"Тілінді қосу");
    vocabulary.add(KZ, "Открыть",	"Ашу");
    vocabulary.add(KZ, "Сохранить",	"Сақтау");
    vocabulary.add(KZ, "перевод",	"аударма");
    vocabulary.add(KZ, "Файл",	"Файл");
    vocabulary.add(KZ, "слово",	"сөз");
  }
  
  public void load(String fileName) {
    vocabulary.load(fileName);
  }

  public void save(String file) {
    vocabulary.save(file);
  }

  public Map<String, String> getDictionary(String language) {
    final Map<String, String> dictionary = new HashMap<>();
    final Set<String> languages = languages();
    final Map<Integer, String> words = vocabulary.getDictionary(
      languages.contains(language) ? language : languages.iterator().next()
    );
    final Map<Integer, String> base = vocabulary.base();
    
    words.entrySet().stream().forEach((entry) -> {
      dictionary.put(base.get(entry.getKey()), entry.getValue());
    });

    return dictionary;
  }

  public String getTranslation(String word) {
    final String translation = getDictionary(currentLanguage).get(word);
    return translation == null ? word : translation;
  }
  
  public void setDictionary(String language, Map<Integer, String> dictionary) {
    vocabulary.setDictionary(language, dictionary);
  }

  public void add(String targetLanguage, String word, String translation) {
    vocabulary.add(targetLanguage, word, translation);
  }

  public void addLanguage(String language) {
    vocabulary.addLanguage(language);
  }

  public boolean contains(String word) {
    return vocabulary.contains(word);
  }

  public boolean containsLanguage(String language) {
    return vocabulary.containsLanguage(language);
  }

  public String getTranslation(String targetLanguage, String word) {
    return vocabulary.getTranslation(targetLanguage, word);
  }

  Map<Integer, String> base() {
    return vocabulary.base();
  }

  public Set<String> languages() {
    return vocabulary.languages();
  }

  public String getCurrentLanguage() {
    return currentLanguage;
  }

  public void setCurrentLanguage(String currentLanguage) {
    this.currentLanguage = currentLanguage;
  }

}

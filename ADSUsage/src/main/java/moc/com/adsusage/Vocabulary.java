/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.adsusage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raim
 */
public class Vocabulary {
  private static final String LANGUAGES = "LANGUAGES";
  private final Map<String, Map<Integer, String>> dictionaries;
  private final Map<Integer, String> base;
  
  public Vocabulary() {
    dictionaries = new HashMap<>();
    base = new HashMap<>();
  }
  
  private String[] loadBase(String fileName) {
    String[] languages = new String[0];
    try {
      Properties props = new Properties();
      props.load(new FileInputStream(new File(fileName)));
      languages = props.getProperty(LANGUAGES).split(":");
      Set<String> keys = props.stringPropertyNames();
      keys.stream().forEach((key) -> {
        if (!LANGUAGES.equals(key)) {
          base.put(Integer.valueOf(key), props.getProperty(key));
        }
      });
      
    } catch (Exception e) {
    }
    return languages;
  }
  
  private void load(String fileName, String language) {
    try {      
      Properties props = new Properties();
      props.load(new FileInputStream(new File(fileName + ":" + language)));

      Map<Integer, String> dictionary = new HashMap<>();
      Set<String> keys = props.stringPropertyNames();
      keys.stream().forEach((key) -> {
        dictionary.put(Integer.valueOf(key), props.getProperty(key));
      });
      dictionaries.put(language, dictionary);
    } catch (IOException ex) {
      Logger.getLogger(Vocabulary.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  public void load(String fileName) {
    String[] languages = loadBase(fileName);
    if (languages.length > 0) {
      dictionaries.clear();
      try {
        Properties props = new Properties();
        props.load(new FileInputStream(new File(fileName)));
        for (String language: languages) {
          load(fileName, language);
        }
      } catch (IOException ex) {
        Logger.getLogger(Vocabulary.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  private void saveBase(String fileName) {
    try (OutputStream out = new FileOutputStream(new File(fileName))) {
      Properties props = new Properties();
      base.forEach((key, value) -> {
        props.setProperty(String.valueOf(key), value);
      });
      
      String languagesKey = "";
      languagesKey = dictionaries.keySet().stream()
          .map((language) -> language + ":")
          .reduce(languagesKey, String::concat);
      
      props.setProperty(LANGUAGES, languagesKey);
      props.store(out, "");
      out.flush();
    } catch (Exception e) {
    }
  }
  
  private void save(String file, String language) {
    
    try (OutputStream out = new FileOutputStream(new File(file + ":" + language))) {
      Properties props = new Properties();
      Map<Integer, String> dictionary = dictionaries.get(language);
      dictionary.forEach((key, value) -> {
        props.setProperty(String.valueOf(key), value);
      });

      props.store(out, "");
      out.flush();
    } catch (Exception e) {
    }
  }
  
  public void save(String file) {
    saveBase(file);
    try {
      dictionaries.keySet().stream().forEach((language) -> {
          save(file, language);
      });
    } catch (Exception e) {
    }
  }
  
  public Map<Integer, String> getDictionary(String language) {
    return dictionaries.get(language);
  }
  
  public void setDictionary(String language, Map<Integer, String> dictionary) {
    dictionaries.put(language, dictionary);
  }
  
  private Integer getKeyByValue(Map<Integer, String> map,String word) {
    for (Entry<Integer, String> entry: map.entrySet()) {
      if (word.equals(entry.getValue())) {
        return entry.getKey();
      }
    }
    return -1;
  }
  
  public void add(String targetLanguage, String word, String translation) {
    Map<Integer, String> dictionary = dictionaries.get(targetLanguage);
    if (dictionary == null) {
      dictionary = new HashMap<>();
      dictionaries.put(targetLanguage, dictionary);
    }
    
    if (base.containsValue(word)) {
      dictionary.put(getKeyByValue(base, word), translation);
    } else {
      final Integer number = base.size() + 1;
      base.put(number, word);
      dictionary.put(number, translation);
    }
  }

  public void addLanguage(String targetLanguage) {
    if (targetLanguage != null && !targetLanguage.isEmpty()) {
      dictionaries.put(targetLanguage, new HashMap<>());
    }
  }
  
  public boolean contains(String word) {
    return base.containsValue((String)word);
  }
  
  public boolean containsLanguage(String language) {
    return languages().contains(language);
  }
  
  public String getTranslation(String targetLanguage, String word) {
    Map<Integer, String> dictionary = dictionaries.get(targetLanguage);
    if (base.containsValue((String)word)) {
      return dictionary.get(getKeyByValue(base, word));
    }
    return "";
  }
  
  Map<Integer, String> base() {
    return base;
  }
  
  public Set<String> languages() {
    return dictionaries.keySet();
  }
}
